import logging

import psycopg2

from config import DATABASE_URL
from util.exceptions import AuthenticationError
from util.password import hash_password


logger = logging.getLogger(__name__)


def change_password(phone_number, current_password, auth_func, new_password):
    try:
        auth_func(phone_number, current_password)
    except:
        raise AuthenticationError

    hashed_password = hash_password(new_password)

    with psycopg2.connect(DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'update customer '
            'set password = %s '
            'where phone_number = %s',
            (hashed_password, phone_number)
        )
        logger.debug(cur.query)

        conn.commit()
