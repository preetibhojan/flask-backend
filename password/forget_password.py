import logging
import random
from datetime import timedelta

import psycopg2

from config import DATABASE_URL
from mail import send_email_async
from util.exceptions import InvalidInput
from util.password import hash_password

logger = logging.getLogger(__name__)


def send_forgot_password_email(phone_number, email, now):
    with psycopg2.connect(DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select id '
            'from customer '
            'where phone_number = %s '
            'and email = %s',
            (phone_number, email)
        )
        logger.debug(cur.query)

        if cur.rowcount == 0:
            logger.critical('Forgot password issued by unknown customer')
            send_email_async(
                email,
                'Password Reset Request',
                'Combination of this Email ID and Phone Number does '
                'not match with our database. Please use the email '
                'address and phone number used during registration '
                'with Preeti Bhojan'
            )

            return

        customer_id = cur.fetchone()[0]

        # The chances of collision are very low since in reset password,
        # requests before 15 minutes are deleted. And if there is a
        # collision, something went wrong will be shown to user. When
        # they will try again, it will work.
        token = random.randint(100000, 999999)

        cur.execute(
            'insert into password_reset_requests(token, customer_id, created) '
            'values (%s, %s, %s)',
            (token, customer_id, now)
        )
        logger.debug(cur.query)

        send_email_async(
            email,
            'Password Reset Request',
            'Your password reset request was successfully recorded. '
            f'The token is: {token}. Enter this token in the text field '
            'on the app. The token will expire in 15 minutes. \n\n'
            'If you did not send the password reset '
            'request, log out of your mail account on all devices '
            'and delete this email immediately!!!'
        )

        return


def reset_password(token, new_password, now):
    if not token:
        return

    with psycopg2.connect(DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'delete from password_reset_requests '
            'where %s - created > %s',
            (now, timedelta(minutes=15))
        )
        logger.debug(cur.query)

        cur.execute(
            'delete from password_reset_requests '
            'where token = %s '
            'returning customer_id',
            (token,)
        )
        logger.debug(cur.query)

        token_exists = cur.rowcount > 0

        if not token_exists:
            logger.critical('Someone sent an unsolicited token')
            raise InvalidInput

        customer_id = cur.fetchone()[0]
        cur.execute(
            'update customer '
            'set password = %s '
            'where id = %s',
            (hash_password(new_password), customer_id)
        )
