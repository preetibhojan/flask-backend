"""
Functions to calculate cost of schedule and cart
"""

import logging
from datetime import date, timedelta

from util.constants import SUNDAY, SATURDAY

logger = logging.getLogger(__name__)


def next_saturday(now: date):
    """
    Returns the next saturday from now.
    :param now: current date time
    :return: date time
    """
    logger.debug('next saturday called with %s', now)

    if now.isoweekday() == SUNDAY:
        result = now + timedelta(days=6)
    elif now.isoweekday() == SATURDAY:
        result = now + timedelta(days=7)
    else:
        result = now + timedelta(days=6 - now.isoweekday())

    assert result.isoweekday() == SATURDAY

    logger.debug('next saturday is: %s', result)

    return result


def calculate_till_date(packages, date_to_book_from):
    """
    Calculates cost of the given schedule from date to book from to the
    till date it calculates (both inclusive).

    **Note:** Does not add tax. Assumes tax is already added.

    :param packages: packages to book (assumes tax is added)
    :param date_to_book_from: date to calculate schedule from
    :return: a tuple containing
    1. cost of schedule from date to book from to whatever till date
    it calculated and
    2. the till date
    """
    days_required_for = {}

    for package in packages:
        days_required_for[package.id] = len(
            list(filter(lambda delivery_info: delivery_info.day >= date_to_book_from.isoweekday(),
                        package.delivery_info
                        )
                 )
        )

    till = date_to_book_from

    if max(days_required_for.values()) < 3:
        for package in packages:
            days_required_for[package.id] += len(package.delivery_info)

        if date_to_book_from.isoweekday() == SUNDAY or date_to_book_from.isoweekday() == SATURDAY:
            till = next_saturday(till)
        else:
            till = next_saturday(till) + timedelta(days=7)
    else:
        till = next_saturday(till)

    return till
