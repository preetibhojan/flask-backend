import unittest
from datetime import datetime, timedelta

from calculate_cost.use_cases import next_saturday


class NextSaturdayTestCase(unittest.TestCase):
    # A sunday
    NOW = datetime(2020, 7, 26)

    def test_next_saturday_works_for_sunday(self):
        self.assertEqual(next_saturday(self.NOW), datetime(2020, 8, 1))

    def test_next_saturday_works_for_monday(self):
        self.assertEqual(next_saturday(self.NOW + timedelta(days=1)), datetime(2020, 8, 1))

    def test_next_saturday_works_for_tuesday(self):
        self.assertEqual(next_saturday(self.NOW + timedelta(days=2)), datetime(2020, 8, 1))

    def test_next_saturday_works_for_wednesday(self):
        self.assertEqual(next_saturday(self.NOW + timedelta(days=3)), datetime(2020, 8, 1))

    def test_next_saturday_works_for_thursday(self):
        self.assertEqual(next_saturday(self.NOW + timedelta(days=4)), datetime(2020, 8, 1))

    def test_next_saturday_works_for_friday(self):
        self.assertEqual(next_saturday(self.NOW + timedelta(days=5)), datetime(2020, 8, 1))

    def test_next_saturday_works_for_saturday(self):
        self.assertEqual(next_saturday(self.NOW + timedelta(days=6)), datetime(2020, 8, 8))


if __name__ == '__main__':
    unittest.main()
