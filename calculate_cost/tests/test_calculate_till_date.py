"""
Tests for calculate schedule cost and till date
"""

import unittest
from datetime import date

from calculate_cost.use_cases import calculate_till_date
from save_schedule.model import PackageToSchedule
from payment_info.models import DeliveryInfo


class CalculateCostAndTillDateTestCase(unittest.TestCase):
    packages = [
        PackageToSchedule(1, [
            DeliveryInfo(5, 0, 0),
            DeliveryInfo(4, 0, 0),
            DeliveryInfo(3, 0, 0),
            DeliveryInfo(2, 0, 0),
            DeliveryInfo(1, 0, 0),
        ]),
        PackageToSchedule(2, [
            DeliveryInfo(5, 0, 0),
            DeliveryInfo(4, 0, 0),
            DeliveryInfo(3, 0, 0),
            DeliveryInfo(2, 0, 0),
            DeliveryInfo(1, 0, 0),
        ])
    ]

    def test_calculate_till_date_works_when_meals_required_for_is_less_than_3_days(self):
        till = calculate_till_date(self.packages, date(2020, 7, 24))
        self.assertEqual(till, date(2020, 8, 1))

    def test_calculate_till_date_works_when_meals_required_for_is_more_than_3_days(self):
        till = calculate_till_date(self.packages, date(2020, 7, 20))
        self.assertEqual(till, date(2020, 7, 25))

    def test_calculate_works_for_sunday(self):
        till = calculate_till_date(self.packages, date(2020, 7, 26))
        self.assertEqual(till, date(2020, 8, 1))

    def test_calculate_works_for_saturday(self):
        till = calculate_till_date(self.packages, date(2020, 7, 25))
        self.assertEqual(till, date(2020, 8, 1))


if __name__ == '__main__':
    unittest.main()
