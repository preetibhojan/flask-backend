from uuid import uuid4

import psycopg2

import config
from payment_gateway.initiate_transaction.use_case import MANUAL_ENTRY
from util.exceptions import AuthenticationError, InvalidInput

DEBIT = 0
CREDIT = 1


def update_payment_detail(auth_func, phone_number, password, customer_id, amount, update_type, remark):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    if amount < 1:
        raise InvalidInput

    if update_type != DEBIT and update_type != CREDIT:
        raise InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select update_payment_detail from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise AuthenticationError

        cur.execute('select id from customer where id = %s', (customer_id,))

        if update_type == DEBIT:
            cur.execute(
                'update paymentdetail set temp_credit = temp_credit - %s '
                'where customer_id = %s',
                (amount, customer_id)
            )
        else:
            cur.execute(
                'insert into transactions (customer_id, invoice_id, created, status, amount, id, source, reservation_id, remark) '
                'values (%s, null, now(), \'successful\', %s, %s, %s, null, %s)',
                (customer_id, amount, str(uuid4()), MANUAL_ENTRY, remark)
            )
            cur.execute(
                'update paymentdetail set temp_credit = temp_credit + %s '
                'where customer_id = %s',
                (amount, customer_id)
            )

        conn.commit()
