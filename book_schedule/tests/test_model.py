import unittest
from datetime import datetime

from book_schedule.model import ScheduleToBook, ScheduledPackage
from deliverables.model import DeliverableStatus, Deliverable
from util.constants import HOME


class ScheduleToBookTestCase(unittest.TestCase):
    def test_to_deliverables(self):
        schedule = ScheduleToBook([
            ScheduledPackage(1, 1, HOME, 100, 1),
            ScheduledPackage(1, 2, HOME, 100, 1),
            ScheduledPackage(1, 3, HOME, 100, 1),

            ScheduledPackage(2, 3, HOME, 100, 1),
            ScheduledPackage(2, 4, HOME, 100, 1),
            ScheduledPackage(2, 5, HOME, 100, 1),
        ], False)

        tuesday = datetime(2020, 10, 13)
        wednesday = datetime(2020, 10, 14)
        thursday = datetime(2020, 10, 15)

        deliverables = schedule.to_deliverables(tuesday, thursday)

        expected_deliverables = [
            Deliverable(1, 1, HOME, 100, tuesday, DeliverableStatus.ORDERED),
            Deliverable(1, 1, HOME, 100, wednesday, DeliverableStatus.ORDERED),
            Deliverable(2, 1, HOME, 100, wednesday, DeliverableStatus.ORDERED),
            Deliverable(2, 1, HOME, 100, thursday, DeliverableStatus.ORDERED),
        ]

        self.assertListEqual(deliverables, expected_deliverables)


if __name__ == '__main__':
    unittest.main()
