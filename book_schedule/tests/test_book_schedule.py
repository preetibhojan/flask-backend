import unittest
from datetime import datetime, timedelta
from unittest.mock import Mock

from book_schedule.model import ScheduledPackage, ScheduleToBook
from book_schedule.use_case import BookSchedule
from util.constants import HOME
from util.exceptions import AuthenticationError, InvalidInput


class NewScheduleTestCase(unittest.TestCase):
    def test_schedule_book_raises_authentication_error_when_auth_func_throws(self):
        auth_func = Mock()
        auth_func.side_effect = Exception

        with self.assertRaises(AuthenticationError):
            BookSchedule(auth_func, None, None).schedule('', '', None, None)

    sunday = datetime(2020, 10, 11)

    packages = [
        ScheduledPackage(1, 1, HOME, 100, 1),
        ScheduledPackage(1, 2, HOME, 100, 1),
        ScheduledPackage(1, 3, HOME, 100, 1),

        ScheduledPackage(2, 1, HOME, 100, 1),
        ScheduledPackage(2, 2, HOME, 100, 1),
        ScheduledPackage(2, 3, HOME, 100, 1),
    ]

    schedule = ScheduleToBook(packages, 0)

    cost_of_scheduled_packages = sum(map(lambda s: s.price, packages)) * 2

    def test_book_schedule_raises_invalid_input_when_schedule_is_empty(self):
        auth_func = Mock()
        auth_func.return_value = None

        repository = Mock()
        deposit_to_maintain = 100
        repository.schedule_and_deposit_to_maintain.return_value = (ScheduleToBook([], False), deposit_to_maintain)

        use_case = BookSchedule(auth_func, None, repository)

        with self.assertRaises(InvalidInput):
            use_case.schedule('', '', self.sunday, False)

        repository.schedule_and_deposit_to_maintain.assert_called_once_with('')
        repository.close.asset_called_once()

    def test_success_when_sum_of_temp_credit_and_deleted_deliverables_is_more_than_cost_of_schedule(self):
        auth_func = Mock()
        auth_func.return_value = None

        calculate_till_date = Mock()
        till_date = self.sunday.date() + timedelta(days=10)
        calculate_till_date.return_value = till_date

        repository = Mock()
        deposit_to_maintain = 100
        repository.schedule_and_deposit_to_maintain.return_value = (self.schedule, deposit_to_maintain)
        
        current_deposit = 50
        repository.current_deposit.return_value = current_deposit

        cost_of_deleted_deliverables = 200
        repository.delete_deliverables.return_value = cost_of_deleted_deliverables

        total_cost = self.cost_of_scheduled_packages \
                     + deposit_to_maintain \
                     - cost_of_deleted_deliverables \
                     - current_deposit

        use_case = BookSchedule(auth_func, calculate_till_date, repository)

        delta = 150
        temp_credit = total_cost + delta
        repository.temp_credit.return_value = temp_credit

        self.assertTrue(use_case.schedule('', '', self.sunday, False))

        repository.schedule_and_deposit_to_maintain.assert_called_once_with('')
        repository.temp_credit.assert_called_once_with('')
        repository.set_temp_credit.assert_called_once_with('', delta)
        repository.close.asset_called_once()
        repository.current_deposit.assert_called_once_with('')
        repository.set_deposit.assert_called_once_with('', deposit_to_maintain)
        repository.commit.assert_called_once()
        repository.make_customer_regular.assert_called_once_with('')

        tomorrow = self.sunday.date() + timedelta(days=1)
        repository.insert_deliverables('', self.schedule.to_deliverables(tomorrow, till_date))
        repository.delete_deliverables.assert_called_once_with('', tomorrow)
        calculate_till_date.assert_called_once_with(self.packages, tomorrow)

    def test_success_when_sum_of_temp_credit_and_deleted_deliverables_is_equal_to_cost_of_schedule(self):
        auth_func = Mock()
        auth_func.return_value = None

        calculate_till_date = Mock()
        till_date = self.sunday.date() + timedelta(days=10)
        calculate_till_date.return_value = till_date

        repository = Mock()
        deposit_to_maintain = 100
        repository.schedule_and_deposit_to_maintain.return_value = (self.schedule, deposit_to_maintain)

        cost_of_deleted_deliverables = 200
        repository.delete_deliverables.return_value = cost_of_deleted_deliverables

        current_deposit = 150
        repository.current_deposit.return_value = current_deposit

        total_cost = self.cost_of_scheduled_packages \
                     + deposit_to_maintain \
                     - cost_of_deleted_deliverables \
                     - current_deposit

        use_case = BookSchedule(auth_func, calculate_till_date, repository)

        delta = 0
        temp_credit = total_cost + delta
        repository.temp_credit.return_value = temp_credit

        self.assertTrue(use_case.schedule('', '', self.sunday, False))

        repository.schedule_and_deposit_to_maintain.assert_called_once_with('')
        repository.temp_credit.assert_called_once_with('')
        repository.set_temp_credit.assert_called_once_with('', delta)
        repository.close.asset_called_once()
        repository.current_deposit.assert_called_once_with('')
        repository.set_deposit.assert_called_once_with('', deposit_to_maintain)
        repository.commit.assert_called_once()
        repository.make_customer_regular.assert_called_once_with('')

        tomorrow = self.sunday.date() + timedelta(days=1)
        deliverables = self.schedule.to_deliverables(tomorrow, till_date)
        repository.insert_deliverables('', deliverables)
        repository.delete_deliverables.assert_called_once_with('', tomorrow)
        calculate_till_date.assert_called_once_with(self.packages, tomorrow)

    def test_schedule_returns_false_when_temp_credit_is_less_than_total_cost(self):
        auth_func = Mock()
        auth_func.return_value = None

        calculate_till_date = Mock()
        till_date = self.sunday.date() + timedelta(days=10)
        calculate_till_date.return_value = till_date

        repository = Mock()
        deposit_to_maintain = 100
        repository.schedule_and_deposit_to_maintain.return_value = (self.schedule, deposit_to_maintain)

        cost_of_deleted_deliverables = 200
        repository.delete_deliverables.return_value = cost_of_deleted_deliverables

        current_deposit = 100
        repository.current_deposit.return_value = current_deposit

        total_cost = self.cost_of_scheduled_packages \
                     + deposit_to_maintain \
                     - cost_of_deleted_deliverables \
                     - current_deposit

        use_case = BookSchedule(auth_func, calculate_till_date, repository)

        delta = -10
        temp_credit = total_cost + delta
        repository.temp_credit.return_value = temp_credit

        self.assertFalse(use_case.schedule('', '', self.sunday, False))

        repository.schedule_and_deposit_to_maintain.assert_called_once_with('')
        repository.temp_credit.assert_called_once_with('')
        repository.close.asset_called_once()
        repository.current_deposit.assert_called_once_with('')
        # not checking if set deposit is being called because current deposit == deposit to maintain

        tomorrow = self.sunday.date() + timedelta(days=1)
        calculate_till_date.assert_called_once_with(self.packages, tomorrow)
        repository.delete_deliverables.assert_called_once_with('', tomorrow)

    def test_schedule_updates_cart_when_update_cart_is_true(self):
        auth_func = Mock()
        auth_func.return_value = None

        calculate_till_date = Mock()
        till_date = self.sunday.date() + timedelta(days=10)
        calculate_till_date.return_value = till_date

        repository = Mock()
        deposit_to_maintain = 100
        repository.schedule_and_deposit_to_maintain.return_value = (self.schedule, deposit_to_maintain)

        cost_of_deleted_deliverables = 200
        repository.delete_deliverables.return_value = cost_of_deleted_deliverables

        current_deposit = 100
        repository.current_deposit.return_value = current_deposit

        total_cost = self.cost_of_scheduled_packages \
                     + deposit_to_maintain \
                     - cost_of_deleted_deliverables \
                     - current_deposit

        use_case = BookSchedule(auth_func, calculate_till_date, repository)

        delta = 0
        temp_credit = total_cost + delta
        repository.temp_credit.return_value = temp_credit

        self.assertTrue(use_case.schedule('', '', self.sunday, True))

        repository.schedule_and_deposit_to_maintain.assert_called_once_with('')
        repository.temp_credit.assert_called_once_with('')
        repository.set_temp_credit.assert_called_once_with('', delta)
        repository.close.asset_called_once()
        repository.current_deposit.assert_called_once_with('')
        # not checking if set deposit is being called because current deposit == deposit to maintain
        repository.commit.assert_called_once()
        repository.make_customer_regular.assert_called_once_with('')

        tomorrow = self.sunday.date() + timedelta(days=1)
        deliverables = self.schedule.to_deliverables(tomorrow, till_date)
        repository.insert_deliverables('', deliverables)
        repository.update_cart.assert_called_once_with('', list(filter(lambda d: d.date == tomorrow, deliverables)))
        repository.delete_deliverables.assert_called_once_with('', tomorrow)

        calculate_till_date.assert_called_once_with(self.packages, tomorrow)
