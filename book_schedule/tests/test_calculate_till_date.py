import unittest
from datetime import timedelta, date

from book_schedule.model import ScheduledPackage
from book_schedule.use_case import calculate_scheduled_till_date
from util.constants import HOME


class CalculateTillDateTestCase(unittest.TestCase):
    def test_calculate_till_date_returns_next_sunday_when_more_than_3_packages_can_be_ordered_in_one_week(self):
        packages = [
            ScheduledPackage(1, 1, HOME, 100, 1),
            ScheduledPackage(1, 2, HOME, 100, 1),
            ScheduledPackage(1, 3, HOME, 100, 1),
        ]

        monday = date(2020, 10, 12)
        next_sunday = monday + timedelta(days=6)
        self.assertEqual(calculate_scheduled_till_date(packages, monday), next_sunday)

    def test_calculate_till_date_returns_next_to_next_sunday_when_3_packages_cant_be_ordered_for_at_least_1_meal(self):
        packages = [
            ScheduledPackage(1, 1, HOME, 100, 1),
            ScheduledPackage(1, 2, HOME, 100, 1),
            ScheduledPackage(1, 3, HOME, 100, 1),

            ScheduledPackage(2, 1, HOME, 100, 1),
            ScheduledPackage(2, 2, HOME, 100, 1),
            ScheduledPackage(2, 3, HOME, 100, 1),
        ]

        tuesday = date(2020, 10, 13)
        next_to_next_sunday = tuesday + timedelta(days=12)
        self.assertEqual(calculate_scheduled_till_date(packages, tuesday), next_to_next_sunday)
