import unittest
from datetime import date, timedelta

from book_schedule.use_case import _next_sunday


class NextSundayTestCase(unittest.TestCase):
    sunday = date(2020, 10, 18)

    def test_next_sunday_works_for_monday(self):
        monday = date(2020, 10, 12)
        self.assertEqual(_next_sunday(monday), self.sunday)

    def test_next_sunday_works_for_tuesday(self):
        tuesday = date(2020, 10, 13)
        self.assertEqual(_next_sunday(tuesday), self.sunday)

    def test_next_sunday_works_for_wednesday(self):
        wednesday = date(2020, 10, 14)
        self.assertEqual(_next_sunday(wednesday), self.sunday)

    def test_next_sunday_works_for_thursday(self):
        thursday = date(2020, 10, 15)
        self.assertEqual(_next_sunday(thursday), self.sunday)

    def test_next_sunday_works_for_friday(self):
        friday = date(2020, 10, 16)
        self.assertEqual(_next_sunday(friday), self.sunday)

    def test_next_sunday_works_for_saturday(self):
        saturday = date(2020, 10, 17)
        self.assertEqual(_next_sunday(saturday), self.sunday)

    def test_next_sunday_works_for_sunday(self):
        self.assertEqual(_next_sunday(self.sunday), self.sunday + timedelta(days=7))


if __name__ == '__main__':
    unittest.main()
