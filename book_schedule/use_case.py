import logging
from datetime import timedelta

from util.constants import SUNDAY
from util.exceptions import AuthenticationError, InvalidInput


logger = logging.getLogger(__name__)


class BookSchedule:
    def __init__(self, auth_func, calculate_till_date, repository):
        self.auth_func = auth_func
        self.calculate_till_date = calculate_till_date
        self.repository = repository

    def schedule(self, phone_number, password, now, update_cart):
        try:
            self.auth_func(phone_number, password)
        except Exception:
            raise AuthenticationError

        tomorrow = now.date() + timedelta(days=1)

        try:
            schedule, deposit_to_maintain = self.repository.schedule_and_deposit_to_maintain(phone_number)
            logger.debug(f'deposit to maintain: {deposit_to_maintain}')

            if len(schedule.packages) == 0:
                raise InvalidInput

            current_deposit = self.repository.current_deposit(phone_number)
            logger.debug(f'current deposit: {current_deposit}')

            cost_of_deleted_deliverables = self.repository.delete_deliverables(phone_number, tomorrow)
            logger.debug(f'cost of deleted deliverables: {cost_of_deleted_deliverables}')

            till_date = self.calculate_till_date(schedule.packages, tomorrow)
            logger.debug(f'till date: {till_date}')

            deliverables = schedule.to_deliverables(tomorrow, till_date)
            logger.debug(f'deliverables: \n{deliverables}')

            cost_of_schedule = sum(map(lambda d: d.price, deliverables))
            logger.debug(f'cost of schedule: {cost_of_schedule}')

            total_cost = cost_of_schedule + deposit_to_maintain - cost_of_deleted_deliverables - current_deposit
            logger.debug(f'total cost: {total_cost}')

            temp_credit = self.repository.temp_credit(phone_number)
            logger.debug(f'temp credit: {temp_credit}')

            if temp_credit >= total_cost:
                self.repository.set_temp_credit(phone_number, temp_credit - total_cost)
                self.repository.insert_deliverables(phone_number, deliverables)
                self.repository.set_deposit(phone_number, deposit_to_maintain)
                self.repository.make_customer_regular(phone_number)

                if update_cart:
                    deliverables_for_tomorrow = list(filter(lambda d: d.date == tomorrow, deliverables))
                    self.repository.update_cart(phone_number, deliverables_for_tomorrow)

                self.repository.commit()
                return True

            return False
        finally:
            self.repository.close()


def _next_sunday(now):
    if now.isoweekday() == SUNDAY:
        return now + timedelta(days=7)

    return now + timedelta(days=SUNDAY - now.isoweekday())


def calculate_scheduled_till_date(packages, tomorrow):
    days_required_for = {}

    for package in packages:
        days_required_for[package.package_id] = len(
            list(
                filter(
                    lambda p: p.package_id == package.package_id
                              and p.day >= tomorrow.isoweekday(), packages
                )
            )
        )

    now = tomorrow - timedelta(days=1)
    next_sunday = _next_sunday(now)

    if max(days_required_for.values()) >= 3:
        return next_sunday

    return next_sunday + timedelta(days=7)
