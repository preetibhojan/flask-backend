import logging

import psycopg2
from psycopg2.extras import execute_values

import config
import util.database
from book_schedule.model import ScheduledPackage, ScheduleToBook
from util.constants import HOME, OFFICE

logger = logging.getLogger(__name__)


class BookScheduleRepository:
    def __init__(self, _config):
        self._conn = psycopg2.connect(config.DATABASE_URL)
        self.config = _config

    def schedule_and_deposit_to_maintain(self, phone_number):
        with self._conn.cursor() as cur:
            customer_id = util.database.get_customer_id(cur, phone_number)

            home_delivery_charge, office_delivery_charge = util.database.delivery_charge_for_v2(cur, phone_number)

            cur.execute(
                'select package_id, quantity, day, delivery_location, price, m.id, add_on '
                'from temp_tiffininformation '
                'inner join packages p '
                'on temp_tiffininformation.package_id = p.id '
                'inner join meals m on m.id = p.meal_id '
                'where customer_id = %s',
                (customer_id,)
            )
            logger.debug(cur.query)

            scheduled_packages = []

            meal_ids = set()

            gst_at_home = self.config['gst'][str(HOME)]
            gst_at_office = self.config['gst'][str(OFFICE)]
            logger.debug(f'gst at home: {gst_at_home}')
            logger.debug(f'gst at office: {gst_at_office}')

            for package_id, quantity, day, delivery_location, price, meal_id, add_on in cur.fetchall():
                meal_ids.add(meal_id)

                # Add ons dont have GST or delivery charge
                if add_on:
                    price_with_gst = price
                    logger.debug(f'package id: {package_id} is addon with price: {price_with_gst}')
                else:
                    if delivery_location == HOME:
                        logger.debug(f'regular package: {package_id}, price: {price}, '
                                     f'home delivery charge: {home_delivery_charge[meal_id]}')
                        price_with_delivery_charge = price + home_delivery_charge[meal_id]
                        price_with_gst = price_with_delivery_charge + price_with_delivery_charge * gst_at_home / 100
                    else:
                        logger.debug(f'regular package: {package_id}, price: {price}, '
                                     f'office delivery charge: {office_delivery_charge[meal_id]}')
                        price_with_delivery_charge = price + office_delivery_charge[meal_id]
                        price_with_gst = price_with_delivery_charge + price_with_delivery_charge * gst_at_office / 100

                    logger.debug(f'price with gst" {price_with_gst}')

                scheduled_packages.append(
                    ScheduledPackage(
                        package_id, day, delivery_location, price_with_gst, quantity
                    )
                )

            deposit_to_maintain = 0

            if len(meal_ids) != 0:
                cur.execute(
                    'select sum(deposit) '
                    'from meals '
                    'where id in %s',
                    (tuple(meal_ids),)
                )
                deposit_to_maintain = cur.fetchone()[0]

            door_step_delivery = False
            cur.execute('select door_step_delivery from homeaddress where customer_id = %s', (customer_id,))
            if cur.rowcount > 0:
                door_step_delivery = cur.fetchone()[0]

            return ScheduleToBook(scheduled_packages, door_step_delivery), deposit_to_maintain

    def current_deposit(self, phone_number):
        with self._conn.cursor() as cur:
            customer_id = util.database.get_customer_id(cur, phone_number)

            cur.execute('select deposit '
                        'from paymentdetail '
                        'where customer_id = %s '
                        'for update',
                        (customer_id,)
                        )
            logger.debug(cur.query)

            return float(cur.fetchone()[0])

    def set_deposit(self, phone_number, deposit):
        with self._conn.cursor() as cur:
            customer_id = util.database.get_customer_id(cur, phone_number)
            logger.debug(f'setting deposit for {customer_id} to {deposit}')
            cur.execute(
                'update paymentdetail '
                'set deposit = %s '
                'where customer_id = %s ',
                (deposit, customer_id)
            )

    def delete_deliverables(self, phone_number, tomorrow):
        with self._conn.cursor() as cur:
            customer_id = util.database.get_customer_id(cur, phone_number)

            cur.execute(
                'delete from deliverables '
                'where customer_id = %s '
                'and date >= %s '
                'and status = \'ordered\' '
                'returning price',
                (customer_id, tomorrow)
            )
            logger.debug(cur.query)

            return float(sum(map(lambda x: x[0], cur.fetchall())))

    def insert_deliverables(self, phone_number, deliverables):
        with self._conn.cursor() as cur:
            customer_id = util.database.get_customer_id(cur, phone_number)

            deliverables_with_customer_id = list(
                map(
                    lambda d: (
                        customer_id,
                        d.package_id,
                        d.quantity,
                        d.delivery_location,
                        d.date,
                        d.price,
                        d.door_step_delivery,
                    ),
                    deliverables
                )
            )

            execute_values(
                cur,
                'insert into deliverables (customer_id, package_id, '
                'quantity, delivery_location, date, price, door_step_delivery) '
                'values %s',
                deliverables_with_customer_id
            )
            logger.debug(cur.query)

            cur.execute('delete from tiffininformation where customer_id = %s', (customer_id,))
            logger.debug(cur.query)
            cur.execute('insert into tiffininformation select * from temp_tiffininformation where customer_id = %s', (customer_id,))
            logger.debug(cur.query)

            cur.execute(
                'delete from regular_customers_to_send_notification '
                'where customer_id = %s', (customer_id,)
            )

            cur.execute('delete from temp_tiffininformation where customer_id = %s', (customer_id,))
            logger.debug(cur.query)

    def make_customer_regular(self, phone_number):
        with self._conn.cursor() as cur:
            customer_id = util.database.get_customer_id(cur, phone_number)
            logger.debug(f'making {customer_id} regular')
            cur.execute(
                'update customer '
                'set isregularcustomer = true '
                'where id = %s',
                (customer_id,)
            )

    def temp_credit(self, phone_number):
        return util.database.temp_credit(self._conn, phone_number)

    def set_temp_credit(self, phone_number, credit):
        util.database.set_credit(self._conn, phone_number, credit)

    def update_cart(self, phone_number, deliverables):
        util.database.add_to_cart(self._conn, phone_number, deliverables)

    def commit(self):
        self._conn.commit()

    def close(self):
        self._conn.close()
