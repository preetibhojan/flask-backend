from datetime import timedelta

from deliverables.model import DeliverableStatus, Deliverable
from util.constants import HOME
from util.datetime_util import date_range


class ScheduledPackage:
    def __init__(self, package_id, day, delivery_location, price, quantity):
        self.package_id = package_id
        self.day = day
        self.delivery_location = delivery_location
        self.price = price
        self.quantity = quantity

    def __repr__(self):
        return f'id: {self.package_id}, day: {self.day}, ' \
               f'delivery_location: {self.delivery_location}, ' \
               f'price: {self.price}'

    def __str__(self):
        return self.__repr__()

    def __format__(self, format_spec):
        return self.__str__()


class ScheduleToBook:
    def __init__(self, packages, door_step_delivery):
        self.packages = packages
        self.door_step_delivery = door_step_delivery

    def to_deliverables(self, from_date, till_date):
        deliverables = []

        for date in date_range(from_date, till_date + timedelta(days=1)):
            for package in self.packages:
                if package.day == date.isoweekday():
                    deliverables.append(
                        Deliverable(
                            package.package_id,
                            package.quantity,
                            package.delivery_location,
                            package.price * package.quantity,
                            date,
                            DeliverableStatus.ORDERED,
                            door_step_delivery=package.delivery_location == HOME and self.door_step_delivery,
                        )
                    )

        return deliverables
