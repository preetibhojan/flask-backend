import logging
from datetime import timedelta, date

import firebase_admin
import psycopg2
from firebase_admin.messaging import Notification, Message, send_all, send_multicast, MulticastMessage

import config

logging.basicConfig(level=config.log_level, format='%(levelname)s %(name)s %(asctime)s %(message)s')
logger = logging.getLogger(__name__)

firebase_admin.initialize_app()


def create_regular_customers_to_send_notification():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('truncate table regular_customers_to_send_notification')
        logger.debug(cur.query)

        cur.execute(
            'insert into regular_customers_to_send_notification '
            'select id from customer '
            'where isregularcustomer is true and exceptional is false'
        )
        logger.debug(cur.query)

        cur.execute(
            'update customer set isregularcustomer = false '
            'where exceptional is false'
        )
        logger.debug(cur.query)

        cur.execute('truncate table tiffininformation')
        logger.debug(cur.query)


def send_schedule_reminder_notifications():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select first_name, fcm_token from customer where id in ('
            'select customer_id from regular_customers_to_send_notification'
            ') '
            'and fcm_token is not null'
        )
        logger.debug(cur.query)

        messages = []
        for first_name, token in cur.fetchall():
            messages.append(
                Message(
                    notification=Notification(
                        title='Schedule Reminder',
                        body=f'Dear {first_name}, Please schedule your meal for the next week',
                    ),
                    token=token
                )
            )

        response = send_all(messages)
        logger.info(f'messages to be sent: {len(messages)}, messages sent: {response.success_count}')


def reminder_to_book(meal_id, date):
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('select active from meals where id = %s', (meal_id,))
        print(cur.query)

        active = cur.fetchone()[0]

        if not active:
            print('meal not active')
            return
        cur.execute('select day from cannot_order_on where meal_id = %s', (meal_id,))
        print(cur.query)

        cannot_order_on = list(map(lambda r: r[0], cur.fetchall()))

        if date.isoweekday() in cannot_order_on:
            print('day is in cannot order on')
            return

        cur.execute(
            'select * from menu '
            'where meal_id = %s '
            'and date = %s',
            (meal_id, date)
        )
        print(cur.query)

        if cur.rowcount == 0:
            print('menu is not set')
            return

        cur.execute(
            'select * from not_serving '
            'where from_date >= %s '
            'and till_date <= %s',
            (date, date)
        )
        print(cur.query)

        if cur.rowcount > 0:
            print('not serving includes today\'s date')
            return

        cur.execute(
            'select fcm_token from customer '
            'where id in (select distinct customer_id from deliverables d '
            'inner join packages p on p.id = d.package_id '
            'where date >= %s and p.meal_id = %s'
            ') '
            'and id not in (select distinct customer_id from deliverables d '
            'inner join packages p on p.id = d.package_id '
            'where date = %s and p.meal_id = %s'
            ') '
            'and fcm_token is not null '
            'and exceptional is false '
            'union distinct '
            'select fcm_token '
            'from customer '
            'inner join officeaddress o '
            'on customer.id = o.customer_id '
            'and o.company = \'Springer Nature\' '
            'and customer.fcm_token is not null',
            (date - timedelta(days=15), meal_id, date, meal_id),
        )
        print(cur.query)

        rows = cur.fetchall()
        tokens = list(map(lambda r: r[0], rows))

        if len(tokens) == 0:
            print('no customer found to send notifications')
            return

        cur.execute(
            'select time_to_delete_packages from schedule '
            'where meal_id = %s',
            (meal_id,)
        )
        print(cur.query)
        row = cur.fetchone()
        time = row[0]

        cur.execute('select name from meals where id = %s', (meal_id,))
        print(cur.query)
        row = cur.fetchone()
        meal = row[0]

        message = MulticastMessage(
            notification=Notification(
                title='Gentle Reminder',
                body=f'Please book your {meal} by {time.strftime("%I:%M %p")}',
            ),
            tokens=tokens,
        )

        send_multicast(message)


if __name__ == '__main__':
    reminder_to_book(40, date(2022, 7, 10))
