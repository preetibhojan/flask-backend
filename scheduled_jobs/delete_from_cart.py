import logging
from datetime import datetime, date

import psycopg2

import config

logger = logging.getLogger(__name__)


def delete_all_quantity(meal_id, cuisine_id):
    now = date.today()
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'delete from extra_quantity '
            'where date = %s '
            'and package_id in ('
            'select id from packages where meal_id = %s and cuisine_id = %s'
            ') ',
            (now, meal_id, cuisine_id)
        )

        logger.debug(cur.query)


def delete_package_quantity(meal_id, cuisine_id):
    now = date.today()
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'delete from package_quantity '
            'where date = %s '
            'and package_id in ('
            'select id from packages where meal_id = %s and cuisine_id = %s'
            ')',
            (now, meal_id, cuisine_id)
        )

        logger.debug(cur.query)


def set_delivered(meal_id, cuisine_id):
    now = datetime.now()

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select id from packages '
            'where meal_id = %s and cuisine_id = %s',
            (meal_id, cuisine_id)
        )

        logger.debug(cur.query)

        package_ids = tuple(map(lambda x: x[0], cur.fetchall()))

        cur.execute('update deliverables '
                    'set status = \'delivered\' '
                    'where status = \'ordered\' '
                    'and package_id in %s '
                    'and date = %s '
                    'returning customer_id, package_id',
                    (package_ids, now.date())
                    )

        logger.debug(cur.query)


if __name__ == '__main__':
    logging.basicConfig(level='DEBUG', format='%(levelname)s %(name)s %(asctime)s %(message)s')
    set_delivered(2, 1)
