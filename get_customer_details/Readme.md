# Get customer details

Takes phone_number and password for authentication and returns first
name, last name, phone number and email of the customer. 

# Sample input
```json
{
    "phone_number": "1234567890",
	"password": "abcdefghi"
}
```

# Sample output
```json
{
    "cuisine_id": 1,
    "email": "abc@def.com",
    "first_name": "abc",
    "last_name": "def",
    "phone_number": "124567890"
}
```

# Error codes
It returns a 400 Bad request in case of an error with and error code.

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication error            |

## Invalid input
1. Request is not json
2. A required parameter is missing or null

## Authentication Error
Phone number password combo does not match or customer does not exist
