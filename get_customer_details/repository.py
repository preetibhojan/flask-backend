import logging

import psycopg2

import config
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


def get_customer_details(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication failed')
        raise AuthenticationError

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('select first_name, last_name, email, cuisine_id from customer where phone_number = %(pho_no)s',
                    {
                        'pho_no': phone_number
                    })

        logger.debug(cur.query)

        row = cur.fetchone()

        return {
            'phone_number': phone_number,
            'first_name': row[0],
            'last_name': row[1],
            'email': row[2],
            'cuisine_id': row[3]
        }

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()
