import logging
from datetime import datetime, timedelta, date, time

import psycopg2
from apscheduler.schedulers.blocking import BlockingScheduler

import config
from scheduled_jobs.delete_from_cart import set_delivered, delete_package_quantity
from scheduled_jobs.reminders import create_regular_customers_to_send_notification, \
    send_schedule_reminder_notifications, reminder_to_book

logging.basicConfig(level=config.log_level, format='%(levelname)s %(name)s %(asctime)s %(message)s')

logger = logging.getLogger(__name__)

scheduler = BlockingScheduler()


def schedule_cart_and_quantity_jobs(now):
    schedule = get_schedule()

    for s in schedule:
        fifteen_minutes_before = datetime.combine(now, s.delivery_time) - timedelta(minutes=15)
        scheduler.add_job(
            lambda meal_id=s.meal_id, cuisine_id=s.cuisine_id: set_delivered(meal_id, cuisine_id),
            'cron',
            hour=fifteen_minutes_before.hour,
            minute=fifteen_minutes_before.minute,
            name=f'set_delivered_and_delete_from_cart({s.meal_id}, {s.cuisine_id}) {fifteen_minutes_before}'
        )

        delivery_time = datetime.combine(now, s.delivery_time)

        # if we dont write s=s, s will always refer to the last meal
        # because of reference semantics
        def set_delivered_and_delete_packages(s=s):
            print(f'setting delivered and deleting regular quantity for {s.meal_id} and {s.cuisine_id}')
            set_delivered(s.meal_id, s.cuisine_id)
            delete_package_quantity(s.meal_id, s.cuisine_id)

        scheduler.add_job(
            set_delivered_and_delete_packages,
            'cron',
            hour=delivery_time.hour,
            minute=delivery_time.minute,
            name=f'set_delivered_and_delete_from_cart({s.meal_id}, {s.cuisine_id}) {delivery_time}'
        )

        def delete_all_quantity(meal_id=s.meal_id, cuisine_id=s.cuisine_id):
            print(f'setting delivered and deleting all quantity for {meal_id} and {cuisine_id}')
            delete_package_quantity(meal_id, cuisine_id)
            now = date.today()
            with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
                cur.execute(
                    'delete from extra_quantity '
                    'where date = %s '
                    'and package_id in ('
                    'select id from packages where meal_id = %s and cuisine_id = %s'
                    ') ',
                    (now, meal_id, cuisine_id)
                )
                logger.debug(cur.query)

        scheduler.add_job(
            delete_all_quantity,
            'cron',
            hour=s.time_to_delete_packages.hour,
            minute=s.time_to_delete_packages.minute,
            name=f'delete_all_quantity({s.meal_id}, {s.cuisine_id}) {s.time_to_delete_packages}'
        )


class Schedule:
    def __init__(self, meal_id, cuisine_id, time_to_delete_packages, delivery_time):
        self.meal_id = meal_id
        self.cuisine_id = cuisine_id
        self.time_to_delete_packages = time_to_delete_packages
        self.delivery_time = delivery_time


def get_schedule():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select meal_id, cuisine_id, time_to_delete_packages, m.deliverytime '
            'from schedule '
            'inner join meals m on schedule.meal_id = m.id'
        )

        logger.debug(cur.query)

        return [Schedule(s[0], s[1], s[2], s[3]) for s in cur.fetchall()]


def schedule_notification_reminder_jobs():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('select time from schedule_notification_remainder')
        times = list(map(lambda r: r[0], cur.fetchall()))

        scheduler.add_job(
            create_regular_customers_to_send_notification,
            'cron',
            day_of_week='sun',
            hour=0,
            minute=0,
            second=0,
        )

        for time in times:
            scheduler.add_job(
                send_schedule_reminder_notifications,
                'cron',
                day_of_week='sun',
                hour=time.hour,
                minute=time.minute,
                second=time.second
            )


def schedule_reminders():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select meal_id, time_to_delete_packages from schedule '
            'where meal_id in (select id from meals where active)'
        )
        rows = cur.fetchall()

        for meal_id, time_to_delete in rows:
            for delta in [time(minute=45), time(minute=30), time(minute=15)]:
                reminder = datetime.combine(date.min, time_to_delete) - datetime.combine(date.min, delta)
                hour = reminder.seconds // (60 * 60)
                minutes = (reminder.seconds // 60) % 60

                scheduler.add_job(
                    lambda m=meal_id: reminder_to_book(m, date.today()),
                    'cron',
                    hour=hour,
                    minute=minutes,
                    name=f'Reminder to book meal: {meal_id} at {hour}:{minutes}'
                )


if __name__ == '__main__':
    schedule_cart_and_quantity_jobs(datetime.now())
    schedule_notification_reminder_jobs()
    schedule_reminders()
    scheduler.print_jobs()
    scheduler.start()
