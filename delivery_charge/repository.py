import psycopg2

import config
import util.database
from util.exceptions import AuthenticationError


def delivery_charge_use_case_v2(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        return util.database.delivery_charge_for_v2(cur, phone_number)
