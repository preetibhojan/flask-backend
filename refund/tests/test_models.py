import unittest

from refund.model import NetBankingDetails, AccountType


class ModelTestCase(unittest.TestCase):
    def test_is_valid_returns_false_when_account_holder_name_contains_non_alphabetic_characters(self):
        net_banking_details = NetBankingDetails('abc @', '12354678900', 'icici bank', '00987654321',
                                                AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_account_holder_name_starts_with_a_space(self):
        net_banking_details = NetBankingDetails(' abc', '12354678900', 'icici bank', '00987654321',
                                                AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_account_holder_name_contains_numbers(self):
        net_banking_details = NetBankingDetails('abc 12', '12354678900', 'icici bank', '00987654321',
                                                AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_account_number_contains_special_characters(self):
        net_banking_details = NetBankingDetails('abc', '123546@78900', 'icici bank', '00987654321', AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_account_number_contains_spaces(self):
        net_banking_details = NetBankingDetails('abc', '12354678 00', 'icici bank', '00987654321', AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_account_number_is_less_than_11_characters(self):
        net_banking_details = NetBankingDetails('abc', '1235467800', 'icici bank', '00987654321', AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_bank_contains_special_characters(self):
        net_banking_details = NetBankingDetails('abc', '12354678900', 'icici bank@', '00987654321', AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_ifsc_code_contains_spaces(self):
        net_banking_details = NetBankingDetails('abc', '12354678900', 'icici bank', '009 7654321', AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_ifsc_code_contains_special_characters(self):
        net_banking_details = NetBankingDetails('abc', '12354678900', 'icici bank', '009@7654321', AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

    def test_is_valid_returns_false_when_ifsc_code_is_not_11_characters_long(self):
        net_banking_details = NetBankingDetails('abc', '1235467890', 'icici bank', '009887654321', AccountType.Savings)

        self.assertFalse(net_banking_details.is_valid())

        net_banking_details.ifsc_code = '0097654321'

        self.assertFalse(net_banking_details.is_valid())
