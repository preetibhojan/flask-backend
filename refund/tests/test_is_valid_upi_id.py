import unittest

from refund.repository import _is_upi_id_valid


class IsValidUpiIDTestCase(unittest.TestCase):
    def test_is_upi_id_valid_returns_false_when_upi_id_returns_false_when_input_contains_spaces(self):
        self.assertFalse(_is_upi_id_valid(' abd@def'))
        self.assertFalse(_is_upi_id_valid('abd@def '))
        self.assertFalse(_is_upi_id_valid('abd @def'))
        self.assertFalse(_is_upi_id_valid('abd@ def'))

    def test_is_upi_id_valid_returns_true_for_phone_number_like_upi_id(self):
        self.assertTrue(_is_upi_id_valid('1234567890@abc'))

    def test_is_upi_id_valid_returns_true_for_email_like_upi_id(self):
        self.assertTrue(_is_upi_id_valid('abc.def@abc'))

    def test_is_upi_id_valid_returns_true_when_upi_id_contains_dash(self):
        self.assertTrue(_is_upi_id_valid('abc.def-ghi@abc'))


if __name__ == '__main__':
    unittest.main()
