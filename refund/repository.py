import logging
import re
import uuid

import psycopg2

import config
from refund.model import PaymentMode, AccountType
from util.database import get_customer_id
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger(__name__)


def _is_upi_id_valid(upi_id):
    # Escaping . is not redundant. We want to match the dot.
    # But dot in regex means a single character (other than newline)
    # noinspection RegExpRedundantEscape
    return re.search(r'^[a-zA-Z0-9\.\-]{2,256}@[a-zA-Z]{2,64}$', upi_id)


def refund(phone_number, password, auth_func, now, payment_mode, upi_id, net_banking_details):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)

        cur.execute(
            'select temp_credit '
            'from paymentdetail '
            'where paymentdetail.customer_id = %s '
            'for update',
            (customer_id,)
        )
        logger.debug(cur.query)

        credit = cur.fetchone()[0]

        if credit == 0:
            return

        _validate_payment_details(payment_mode, upi_id, net_banking_details)

        cur.execute(
            'update paymentdetail '
            'set temp_credit = 0 '
            'where customer_id = %s',
            (customer_id,)
        )
        logger.debug(cur.query)

        _insert_refund_record(cur, credit, customer_id, net_banking_details, now, upi_id)

        conn.commit()


def _insert_refund_record(cur, credit, customer_id, net_banking_details, now, upi_id):
    cur.execute(
        'insert into refund(id, customer_id, date, amount_requested, upi_id, '
        'acc_holder_name, acc_number, bank, ifsc_code, acc_type) '
        'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
        (
            str(uuid.uuid4()),
            customer_id,
            now.date(),
            credit,
            upi_id,
            net_banking_details.account_holder_name,
            net_banking_details.account_number,
            net_banking_details.bank,
            net_banking_details.ifsc_code,
            'savings' if net_banking_details.account_type == AccountType.Savings else 'current'
        )
    )
    logger.debug(cur.query)


def _validate_payment_details(payment_mode, upi_id, net_banking_details):
    if payment_mode is None:
        logger.info('Payment mode is null')
        raise InvalidInput
    if payment_mode == PaymentMode.UPI and not _is_upi_id_valid(upi_id):
        logger.info('Payment mode is upi id not valid')
        raise InvalidInput
    if payment_mode == PaymentMode.NetBanking and not net_banking_details.is_valid():
        logger.info('net banking details is not valid')
        raise InvalidInput


def discontinue_service(phone_number, password, auth_func, now, payment_mode, upi_id, net_banking_details):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)

        cur.execute(
            'select temp_credit + deposit '
            'from paymentdetail '
            'where paymentdetail.customer_id = %s '
            'for update',
            (customer_id,)
        )
        logger.debug(cur.query)

        credit = cur.fetchone()[0]

        cur.execute(
            'update customer '
            'set isregularcustomer = false '
            'where id = %s',
            (customer_id,)
        )

        if credit != 0:
            _validate_payment_details(payment_mode, upi_id, net_banking_details)

            cur.execute(
                'update paymentdetail '
                'set temp_credit = 0, deposit = 0 '
                'where customer_id = %s',
                (customer_id,)
            )
            logger.debug(cur.query)

            _insert_refund_record(cur, credit, customer_id, net_banking_details, now, upi_id)

        conn.commit()
