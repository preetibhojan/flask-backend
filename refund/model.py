import re
from enum import IntEnum

from util.exceptions import InvalidInput
from util.string_util import is_alphanumeric_underscore_or_space


class AccountType(IntEnum):
    Savings = 0
    Current = 1

    @staticmethod
    def from_int(value):
        if value == 0:
            return AccountType.Savings
        if value == 1:
            return AccountType.Current

        raise InvalidInput


class PaymentMode(IntEnum):
    UPI = 0
    NetBanking = 1

    @staticmethod
    def from_int(value):
        if value == 0:
            return PaymentMode.UPI
        if value == 1:
            return PaymentMode.NetBanking

        if value is None:
            return None

        raise InvalidInput


class NetBankingDetails:
    def __init__(self, account_holder_name, account_number, bank, ifsc_code, account_type):
        self.account_holder_name = account_holder_name
        self.account_number = account_number
        self.bank = bank
        self.ifsc_code = ifsc_code
        self.account_type = account_type

    def is_valid(self):
        return self._is_account_holder_name_valid() \
               and self._is_account_number_valid() \
               and self._is_bank_name_valid() \
               and self._is_ifsc_code_valid()

    def _is_account_holder_name_valid(self):
        return self.account_holder_name is not None \
                and re.search(r'^[a-zA-Z][a-zA-Z ]+$', self.account_holder_name) is not None

    def _is_account_number_valid(self):
        return self.account_number is not None \
                and re.search(r'^[a-zA-Z0-9]{11,20}$', self.account_number) is not None

    def _is_bank_name_valid(self):
        return self.bank is not None \
               and is_alphanumeric_underscore_or_space(self.bank)

    def _is_ifsc_code_valid(self):
        return self.ifsc_code is not None \
               and re.search(r'[a-zA-Z0-9]{11}', self.ifsc_code) is not None

    @staticmethod
    def from_json(json):
        if json is None:
            return NetBankingDetails(None, None, None, None, None)

        return NetBankingDetails(
            json['acc_holder_name'],
            json['acc_number'],
            json['bank'],
            json['ifsc_code'],
            AccountType.from_int(json['acc_type'])
        )
