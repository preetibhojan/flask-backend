import logging

import psycopg2

import config
from util.database import db_connection
from util.exceptions import UnauthorizedAccess, AuthenticationError

logger = logging.getLogger(__name__)


def summary_use_case(phone_number, password, auth_func, meal_id, date):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select home from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise UnauthorizedAccess

        cur.execute(
            'create temporary table d on commit drop as '
            'select * from report_packages(%s, %s);',
            (date, meal_id)
        )

        logger.debug(cur.query)

        cur.execute(
            'select package_id, count, p.name, pg.name, cuisine_id, add_on from d '
            'inner join packages p on p.id = package_id '
            'inner join package_group pg on pg.id = p.group_id'
        )

        packages = []
        for package_id, count, name, group, cuisine_id, add_on in cur.fetchall():
            packages.append({
                'package_id': package_id,
                'count': count,
                'name': name,
                'group': group,
                'cuisine_id': cuisine_id,
                'add_on': add_on,
            })

        cur.execute('select roti, sum(d.count) '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'group by roti '
                    'order by roti')

        logger.debug(cur.query)

        rotis = []
        for roti, count in cur.fetchall():
            # Some packages do not have roti
            if roti is not None:
                rotis.append({
                    'name': f'{int(roti)} Roti',
                    'count': int(count),
                    'value': int(roti)
                })

        cur.execute('select name, sum(sabji1_quantity * d.count), sabji1 '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.sabji1 '
                    'group by name, sabji1 '
                    'order by sabji1')

        logger.debug(cur.query)

        sabji1 = []
        for name, count, _ in cur.fetchall():
            sabji1.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(sabji2_quantity * d.count), sabji2 '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.sabji2 '
                    'group by name, sabji2 '
                    'order by sabji2')

        logger.debug(cur.query)

        sabji2 = []
        for name, count, _ in cur.fetchall():
            sabji2.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(sabji_p_quantity * d.count), sabji_p '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.sabji_p '
                    'group by name, sabji_p '
                    'order by sabji_p')

        logger.debug(cur.query)

        sabji_p = []
        for name, count, _ in cur.fetchall():
            sabji_p.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(sabji_s_quantity * d.count), sabji_s '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.sabji_s '
                    'group by name, sabji_s '
                    'order by sabji_s')

        logger.debug(cur.query)

        sabji_s = []
        for name, count, _ in cur.fetchall():
            sabji_s.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(daal_quantity * d.count), daal '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.daal '
                    'group by name, daal '
                    'order by daal')

        logger.debug(cur.query)

        daal = []
        for name, count, _ in cur.fetchall():
            daal.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(daal_s_quantity * d.count), daal_s '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.daal_s '
                    'group by name, daal_s '
                    'order by daal_s')

        logger.debug(cur.query)

        daal_s = []
        for name, count, _ in cur.fetchall():
            daal_s.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(rice_quantity * d.count), rice '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.rice '
                    'group by name, rice '
                    'order by rice')

        logger.debug(cur.query)

        rice = []
        for name, count, _ in cur.fetchall():
            rice.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(pulav_quantity * d.count), pulav '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.pulav '
                    'group by name, pulav '
                    'order by pulav')

        logger.debug(cur.query)

        pulav = []
        for name, count, _ in cur.fetchall():
            pulav.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(pulav_s_quantity * d.count), pulav_s '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.pulav_s '
                    'group by name, pulav_s '
                    'order by pulav_s')

        logger.debug(cur.query)

        pulav_s = []
        for name, count, _ in cur.fetchall():
            pulav_s.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(sweet_quantity * d.count), sweet '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.sweet '
                    'group by name, sweet '
                    'order by sweet')

        logger.debug(cur.query)

        sweet = []
        for name, count, _ in cur.fetchall():
            sweet.append({
                'name': name,
                'count': int(count)
            })

        cur.execute('select name, sum(farsan_quantity * d.count), farsan '
                    'from container_detail '
                    'inner join d '
                    'on container_detail.package_id = d.package_id '
                    'inner join containers on containers.id = container_detail.farsan '
                    'group by name, farsan '
                    'order by farsan')

        logger.debug(cur.query)

        farsan = []
        for name, count, _ in cur.fetchall():
            farsan.append({
                'name': name,
                'count': int(count)
            })

        return rotis, sabji1, sabji2, sabji_s, sabji_p, daal, daal_s, rice, pulav, pulav_s, sweet, farsan, packages


def summary_use_case2(phone_number, password, auth_func, meal_id, date):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with db_connection() as conn, conn.cursor() as cur:
        cur.execute(
            'select home from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise UnauthorizedAccess

        cur.execute(
            'select package_id, count, p.name, pg.name, cuisine_id, add_on '
            'from report_packages(%s, %s) '
            'inner join packages p on p.id = package_id '
            'inner join package_group pg on pg.id = p.group_id',
            (date, meal_id)
        )

        logger.debug(cur.query)

        packages = {}
        for package_id, count, name, group, cuisine_id, add_on in cur.fetchall():
            packages[package_id] = {
                'package_id': package_id,
                'count': count,
                'name': name,
                'group': group,
                'cuisine_id': cuisine_id,
                'add_on': add_on,
                'content': [],
            }

        if len(packages) != 0:
            cur.execute(
                'select package_id, name, category from container_detail2 '
                'inner join containers c '
                'on container_detail2.container_id = c.id '
                'and package_id in %s '
                'order by c.id ',
                (tuple(packages.keys()),)
            )

            logger.debug(cur.query)

            for package_id, name, category in cur.fetchall():
                packages[package_id]['content'].append({
                    'name': name,
                    'category': category
                })

        cur.execute(
            'select sum(d.quantity), c.quantity, name, category, c.id '
            'from container_detail2 cd '
            'inner join containers c '
            'on c.id in (select id from containers where quantity != 0) '
            'and c.id = cd.container_id '
            'inner join deliverables d on cd.package_id = d.package_id '
            'and date = %s '
            'and (d.status = \'ordered\' or d.status = \'delivered\') '
            'and d.package_id in (select id from packages where meal_id = %s) '
            'inner join customer cc on d.customer_id = cc.id '
            'group by (c.quantity, name, category, c.id) ',
            (date, meal_id)
        )

        quantities = []
        for quantity, count, name, category, container_id in cur.fetchall():
            quantities.append({
                'category': category,
                'name': name,
                'quantity': quantity,
                'count': count,
                'container_id': container_id,
            })

        return {
            'packages': list(packages.values()),
            'quantities': quantities
        }
