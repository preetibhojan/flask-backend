import logging

import psycopg2

import config
from util.constants import HOME
from util.exceptions import AuthenticationError, UnauthorizedAccess

logger = logging.getLogger(__name__)


def customer_wise_packages_report(phone_number, password, auth_func, meal_id, date):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select home from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise UnauthorizedAccess

        cur.execute(
            'select c.id, phone_number, first_name, last_name, package_id, '
            'p.name, p.description, p.cuisine_id, p.meal_id, d.price, quantity, delivery_location, '
            't.name, ha.flat_number, ha.building, oa.office_number, oa.floor, d.door_step_delivery, '
            'p.display_name, p.add_on '
            'from deliverables d '
            'inner join customer c '
            'on c.id = d.customer_id '
            'inner join packages p '
            'on p.id = d.package_id '
            'left outer join homeaddress ha '
            'on ha.customer_id = c.id '
            'left outer join officeaddress oa '
            'on oa.customer_id = c.id '
            'inner join tower t '
            'on (d.delivery_location = 0 and t.id = ha.tower_id) '
            'or (d.delivery_location = 1 and t.id = oa.tower_id) '
            # Purposely not using active localities only so admin can book
            # for inactive localities as well
            'inner join localities l '
            'on l.id = t.locality_id '
            # Purposely not using active areas only so admin can book
            # for inactive areas as well
            'inner join areas a '
            'on l.area_id = a.id '
            'where (status = \'ordered\' or status = \'delivered\') '
            'and p.meal_id = %s '
            'and date = %s '
            'order by (a.priority, l.priority, t.priority, first_name, last_name, p.name)', (
                meal_id,
                date
            )
        )

        logger.debug(cur.query)

        result = []

        for cust_id, phone_number, first_name, last_name, package_id,\
            package, description, cuisine_id, meal_id, price, quantity, delivery_location, \
            tower, flat_number, building, office_number, floor, door_step_delivery, display_name, add_on in cur.fetchall():
            result.append({
                'customer_id': cust_id,
                'phone_number': phone_number,
                'first_name': first_name,
                'last_name': last_name,
                'package': package,
                'description': description,
                'cuisine_id': cuisine_id,
                'meal_id': meal_id,
                'price': int(price),
                'quantity': quantity,
                'package_id': package_id,
                'tower': tower,
                'grouping': f'{building} {flat_number}' if delivery_location == HOME
                else f'{floor} {office_number}',
                'door_step_delivery': door_step_delivery,
                'display_name': display_name,
                'add_on': add_on,
            })

        return result


def customer_wise_packages_report_for_delivery_executive(phone_number, password, auth_func, meal_id, date):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('select id from staff where phone_number = %s', (phone_number,))
        staff_id = cur.fetchone()[0]

        cur.execute(
            'select home from staff_permissions '
            'where staff_id = %s',
            (staff_id,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise UnauthorizedAccess

        cur.execute(
            'select tower_id from notification_permission where staff_id = %s',
            (staff_id,)
        )

        tower_ids = tuple(map(lambda r: r[0], cur.fetchall()))

        if len(tower_ids) == 0:
            return []

        cur.execute(
            'select c.id, phone_number, first_name, last_name, package_id, '
            'p.name, p.description, p.cuisine_id, p.meal_id, d.price, quantity, delivery_location, '
            't.name, ha.flat_number, ha.building, oa.office_number, oa.floor, d.door_step_delivery, '
            'p.display_name, p.add_on '
            'from deliverables d '
            'inner join customer c '
            'on c.id = d.customer_id '
            'inner join packages p '
            'on p.id = d.package_id '
            'left outer join homeaddress ha '
            'on ha.customer_id = c.id '
            'and ha.tower_id in %s '
            'left outer join officeaddress oa '            
            'on oa.customer_id = c.id '
            'and oa.tower_id in %s '
            'inner join tower t '
            'on (d.delivery_location = 0 and t.id = ha.tower_id) '
            'or (d.delivery_location = 1 and t.id = oa.tower_id) '
            # Purposely not using active localities only so admin can book
            # for inactive localities as well
            'inner join localities l '
            'on l.id = t.locality_id '
            # Purposely not using active areas only so admin can book
            # for inactive areas as well
            'inner join areas a '
            'on l.area_id = a.id '
            'where (status = \'ordered\' or status = \'delivered\') '
            'and p.meal_id = %s '
            'and date = %s '
            'order by (a.priority, l.priority, t.priority, first_name, last_name, p.name)',
            (
                tower_ids,
                tower_ids,
                meal_id,
                date
            )
        )

        logger.debug(cur.query)

        result = []

        for cust_id, phone_number, first_name, last_name, package_id,\
            package, description, cuisine_id, meal_id, price, quantity, delivery_location, \
            tower, flat_number, building, office_number, floor, door_step_delivery, display_name, add_on in cur.fetchall():
            result.append({
                'customer_id': cust_id,
                'phone_number': phone_number,
                'first_name': first_name,
                'last_name': last_name,
                'package': package,
                'description': description,
                'cuisine_id': cuisine_id,
                'meal_id': meal_id,
                'price': int(price),
                'quantity': quantity,
                'package_id': package_id,
                'tower': tower,
                'grouping': f'{building} {flat_number}' if delivery_location == HOME
                else f'{floor} {office_number}',
                'door_step_delivery': door_step_delivery,
                'display_name': display_name,
                'add_on': add_on,
            })

        return result
