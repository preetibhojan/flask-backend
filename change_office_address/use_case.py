import logging

import psycopg2

import config
from change_office_address.exceptions import AreaIDDoesNotExist
from util.database import get_customer_id
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger(__name__)


def change_office_address_use_case(phone_number, password, office_address, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication failed')
        raise AuthenticationError

    if not office_address.is_valid():
        logger.info('Invalid office address')
        raise InvalidInput

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute('select id from tower where id = %(id)s '
                    'and home is false',
                    {
                        'id': office_address.tower_id
                    })

        logger.debug(cur.query)

        if cur.rowcount == 0:
            raise AreaIDDoesNotExist

        cur.execute(
            'insert into officeaddress(customer_id, office_number, '
            'floor, company, tower_id) '
            'values (%(cust_id)s, %(office_no)s, %(floor)s, %(company)s,'
            ' %(tower_id)s) '
            'on conflict (customer_id) do update set '
            'office_number = %(office_no)s, '
            'floor = %(floor)s, '
            'company = %(company)s, '
            'tower_id = %(tower_id)s',
            {
                'cust_id': customer_id,
                'office_no': office_address.office_number,
                'floor': office_address.floor,
                'tower_id': office_address.tower_id,
                'company': office_address.company
            }
        )

        logger.debug(cur.query)

        conn.commit()

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()
