# Change home address

Change home address for customer. Takes in phone number and password
from for authentication. Other parameters are same as registration
in home_address. Refer to registration for explanation

# Sample input
```json
{
    "phone_number": "1234567890",
	"password": "abcdefghi",
    "home_address": 
    {
        "flat_number": "104",
        "building": "def",
        "society": "ghi",
        "locality_id": 1
    }
}
```

# Error codes
It returns a 400 Bad request in case of an error with and error code.

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication error            |
|  2         | Area ID does not exist          |

## Invalid input
1. Request is not json
2. A required parameter is missing or null
3. One of society, building or flat number consists of something other
than alphabets, numbers, _ or space

## Authentication Error
Phone number password combo does not match or customer does not exist

## Area id does not exist
Self explanatory
