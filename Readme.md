# Flask backend
Flask backend for Preeti Bhojan

Preeti Bhojan is an app to order food (for breakfast, lunch, dinner, 
etc.) that will be delivered at specific times during the day 
specifically geared to meet my dad's needs.

**Note:** 

1. Performance is not a priority. We don't have like a million 
customers to serve 

2. You probably also want to check out the mobile app

3. This is not a fully REST API as none of the end points follow
HATEOS.

# How to use?

To use the service, just make a Rest API call in your favourite 
language. 

Services served:

1. Authenticate - (POST) Used to check if the phone number, password
combination is correct. URL: `localhost:5000/api/v1/authenticate`.

2. Cuisines - (GET) The cuisines currently served. 
URL: `localhost:5000/api/v1/cuisines`. For more information, about
the output, etc. check out the readme.md in cuisines directory

3. Delivery locations - (GET) The delivery locations the food is served. 
Currently only Pune city is served in India. So, the bifurcation is
the area and the locality where an area can have many localities.
For more information, about the output, etc. check out the readme.md 
in deliver_locations directory. 
URL: `localhost:5000/api/v1/delivery_locations`

4. Meals - (GET) The meals currently served. They will probably almost
always be breakfast, lunch, snacks and dinner. For more information, 
about the output, etc. check out the readme.md in meals directory.
URL: `localhost:5000/api/v1/meals`

5. Meals I buy - (POST) Gives you the list of ids of meals you buy.
Required your phone number and password for authentication.
URL: `localhost:5000/api/v1/mealsIBuy`

6. Menu - (GET) It returns today's menu. 
URL: `localhost:5000/api/v1/meals`

7. Packages - (GET) Package is the different combinations of food items or
same food items in different quantity. For more information, about
the output, etc. check out the readme.md in packages directory.
URL: `localhost:5000/api/v1/packages`

8. Register - (POST) Register a customer. There are two types of customers -
Need based customers and Regular customers. Need based customers are 
customers who order food sometimes. Regular customers need food regularly.
For more information, about the output, etc. check out the readme.md 
in register directory.
URL: `localhost:5000/api/v1/packages`


# Project structure
Each folder is an independent module (or package if you will but I didnt
want to confuse with the folder packages which is something different).
Each folder also has a Readme.md about it along with a sample output.
*_util folders are utility packages used by other packages.  


# Notes
1. Everywhere input is expected to be trimmed except where spaces 
may be allowed in input.

2. The plan is to only update database at 12 in the night, so 
(at least right now) feel free to cache data until next day

# Static
This folder contains static assets (like images)