"""
This module contains routes to access the API endpoints
"""
import logging
import os
from datetime import date, datetime
from enum import Enum
from typing import Any, Callable

from flask import Flask, jsonify, json, request, render_template
from flask_cors import CORS
from psycopg2._psycopg import adapt, AsIs
from psycopg2.extensions import register_adapter

import authenticate.exceptions
import authenticate.repository
import book_spot.exceptions
import change_cuisine.exceptions
import change_email.exceptions
import change_home_address.exceptions
import change_office_address.exceptions
import meals.repository
import register.exceptions
import save_schedule.exceptions
import util.exceptions
from admin import admin
from am_i_regular_customer.use_case import am_i_regular_customer_use_case, is_regular_customer
from authenticate.use_case import authenticate_use_case
from book_schedule.repository import BookScheduleRepository
from book_schedule.use_case import BookSchedule, calculate_scheduled_till_date
from book_spot.repository import BookingRepository
from book_spot.use_case import book_spot2_use_case, manipulate_price2
from cancel2.repository import cancel2_use_case
from cancel_deliverables_for_date.use_cases import cancel_deliverables_for_date_use_case
from cart2.repository import delete_from_cart, set_delivery_location, cartV2, clear_cart2, set_in_cart
from change_cuisine.repository import change_cuisine_use_case
from change_email.repository import change_email_use_case
from change_home_address.use_case import change_home_address_use_case
from change_office_address.use_case import change_office_address_use_case
from config import log_level, IS_STAGING
from convert.model import PackageConversion
from cuisines.repository import get_cuisines
from deliverables.model import Deliverable
from deliverables.repository import get_all_deliverables, deliverables_from_today, \
    cancelled_deliverables_from_tomorrow
from delivery_charge.repository import delivery_charge_use_case_v2
from delivery_locations.models import Area, Locality, Tower
from delivery_locations.repository import delivery_locations
from door_step_delivery import door_step_delivery
from front_end_config.repository import front_end_config, admin_config
from get_credit.repository import get_credit_use_case
from get_customer_details.repository import get_customer_details
from get_home_address.repository import get_home_address_use_case
from get_office_address.repository import get_office_address_use_case
from mail import send_email_async
from meals.repository import get_meals, get_all_meals
from menu.repository import get_menu
from not_serving import not_serving_repository
from packages.repository import Package, get_packages_for_menu, get_all_packages, get_packages_v2, get_groups, \
    get_packages_for_menu2
from password.change_password import change_password
from password.forget_password import send_forgot_password_email, reset_password
from payment_gateway_routes import payment_gateway
from payment_info.models import DeliveryInfo, PackagePaymentInfo
from payment_info.repository import payment_info_use_case, booked_payment_info
from refund.model import PaymentMode, NetBankingDetails
from refund.repository import refund, discontinue_service
from register.models import Customer, HomeAddress, OfficeAddress
from register.repository import CustomerRepository
from register.use_cases import register_customer
from reserve.model import ReservationDetails
from reserve.repository import ReserveRepository
from reserve.use_cases import reserve_use_case, cancel_reservation_use_case
from save_schedule.model import PackageToSchedule
from save_schedule.repository import SaveScheduleRepository
from save_schedule.use_case import save_schedule_use_case
from societies_and_offices import get_societies, Society, get_offices, Office
from staff_permissions.repository import permissions_for
from transactions.history import transaction_history
from transactions.models import TransactionDetails
from transactions.repositories import TransactionRepository
from transactions.transactionID import generate_transaction_id
from util.database import booking_started_for_tomorrow
from util.flask import respond_with_error, password_for, auth_func, ErrorCode, authenticated
from utility_information.use_case import utility_information_use_case
from website import website
from whatsapp import whatsapp


def adapt_tuple(tuple):
     quoted = [adapt(x).getquoted().decode('utf-8') for x in tuple]
     return AsIs('(' + ','.join(quoted) + ')')


register_adapter(tuple, adapt_tuple)


class CustomEncoder(json.JSONEncoder):
    """
    CustomEncoder is a custom encoder which serializes my user defined types.
    To add another type, define a serialize function for the type and add it
    to _myClasses list.
    """

    _myClasses = [Package, Area, Locality, PackagePaymentInfo, DeliveryInfo,
                  Deliverable, TransactionDetails, Tower, PackageConversion,
                  Society, Office]

    def default(self, o):
        # For some odd reason flask was returning a date as a date time. So I had to add this
        if isinstance(o, date):
            return o.isoformat()
        for class_ in self._myClasses:
            if isinstance(o, class_):
                return o.serialize()

        return super(CustomEncoder, self).default(o)


logging.basicConfig(level=log_level, format='%(levelname)s %(name)s %(asctime)s %(message)s')

logger = logging.getLogger(__name__)

prefix = '/api'
base_url = '%s/v1' % prefix

app = Flask(__name__)
app.json_encoder = CustomEncoder
app.config.from_object('config.ProductionConfig')
app.register_blueprint(admin, url_prefix=f'{base_url}/admin')
app.register_blueprint(payment_gateway, url_prefix=f'{base_url}/payment_gateway')
# app.register_blueprint(v3, url_prefix='/api/v3')
app.register_blueprint(website)
app.register_blueprint(whatsapp, url_prefix=f'{base_url}/whatsapp')

CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'


@app.errorhandler(util.exceptions.GeneralError)
def general_error(e):
    return jsonify(e.description), e.code


@app.route(f'{base_url}/cuisines')
def get_cuisines_route():
    cuisines = get_cuisines()
    return jsonify([cuisine.serialize() for cuisine in cuisines])


class MealsErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/meals', methods=['POST'])
def get_meals_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        temp = get_meals(request.json['phone_number'], request.json['password'], auth_func)
        return jsonify([meal.serialize() for meal in temp])

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(MealsErrorCode.INVALID_INPUT)
    except meals.repository.AuthenticationError:
        return respond_with_error(MealsErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/all_meals')
def get_all_meals_route():
    return jsonify([meal.serialize() for meal in get_all_meals()])


@app.route(f'{base_url}/meals_for_menu', methods=['POST'])
def get_meals_for_menu_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        _meals = meals.repository.get_meals_for_menu(
            request.json['phone_number'],
            request.json['password'],
            auth_func
        )
        return jsonify([meal.serialize() for meal in _meals])

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(MealsErrorCode.INVALID_INPUT)
    except meals.repository.AuthenticationError:
        return respond_with_error(MealsErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{prefix}/v2/packages', methods=['POST'])
def get_packages_v2_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        packages = get_packages_v2(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
        )

        return jsonify(packages)

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(MealsErrorCode.INVALID_INPUT)
    except meals.repository.AuthenticationError:
        return respond_with_error(MealsErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/all_packages')
def get_all_packages_route():
    packages = get_all_packages()
    return jsonify(packages)


@app.route(f'{base_url}/package_groups')
def get_package_groups():
    return jsonify(get_groups())


class PackagesForMenuErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/packages_for_menu', methods=['POST'])
def get_packages_for_menu_route():
    try:
        packages = get_packages_for_menu(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            datetime.now()
        )
        return jsonify(packages)
    except KeyError:
        return respond_with_error(PackagesForMenuErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(PackagesForMenuErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/packages_for_menu2', methods=['POST'])
@authenticated
def get_packages_for_menu2_route(customer_id):
    try:
        packages = get_packages_for_menu2(
            customer_id,
            datetime.now(),
            request.json['home'],
        )
        return jsonify(packages)
    except KeyError:
        return respond_with_error(PackagesForMenuErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(PackagesForMenuErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/delivery_locations')
def get_delivery_locations_route():
    return jsonify(delivery_locations())


class RegisterErrorCode(Enum):
    INVALID_INPUT = 0
    AREA_OR_LOCALITY_DOES_NOT_EXIST = 1
    CUSTOMER_ALREADY_EXISTS = 2
    CUISINE_DOES_NOT_EXIST = 3


@app.route(f'{base_url}/register', methods=['POST'])
def register_customer_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        if request.json['customer'] is None:
            raise util.exceptions.InvalidInput

        customer = Customer.from_json(request.json['customer'])

        home_address = None
        if 'home_address' in request.json:
            home_address = HomeAddress.from_json(request.json['home_address'])

        office_address = None
        if 'office_address' in request.json:
            office_address = OfficeAddress.from_json(request.json['office_address'])

        cuisine_id = request.json['cuisine_id']

        customer_repository = CustomerRepository()

        register_customer(
            customer,
            home_address,
            office_address,
            cuisine_id,
            customer_repository,
            request.json.get('token', None)
        )

        authenticate.repository.set_version(customer.phone_number, request.json.get('version'))

        send_email_async(
            customer.email,
            'Greeting from Preeti Bhojan',
            render_template('greeting.html', header=True, name=customer.first_name),
            html=True
        )

        config = front_end_config(phone_number=customer.phone_number)

        return jsonify({'success': True, 'config': config})
    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(RegisterErrorCode.INVALID_INPUT)

    except register.exceptions.CustomerAlreadyExists:
        return respond_with_error(RegisterErrorCode.CUSTOMER_ALREADY_EXISTS)

    except register.exceptions.AreaOrLocalityDoesNotExist:
        return respond_with_error(RegisterErrorCode.AREA_OR_LOCALITY_DOES_NOT_EXIST)

    except register.exceptions.CuisineDoesNotExist:
        return respond_with_error(RegisterErrorCode.CUISINE_DOES_NOT_EXIST)


class AuthenticateErrorCode(Enum):
    INVALID_INPUT = 0
    CUSTOMER_DOES_NOT_EXIST = 1
    INVALID_PASSWORD = 2


@app.route(f'{base_url}/authenticate', methods=['POST'])
def authenticate_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        phone_number = request.json['phone_number']
        password = request.json['password']

        # hacky
        try:
            result = authenticate_use_case(phone_number, password, password_for)
            if result:
                token = request.json.get('fcm_token')
                apns_token = request.json.get('apns_token')
                logger.debug(f'phone number: {phone_number}, fcm token: {token}, apns token: {apns_token}')

                authenticate.repository.update_token(phone_number, token)
                authenticate.repository.set_version(phone_number, request.json.get('version'))

                config = front_end_config(phone_number=phone_number)
                return jsonify({
                    'success': result,
                    'type': 'customer',
                    'config': config,
                    'is_regular_customer': is_regular_customer(phone_number)
                })

        except authenticate.exceptions.CustomerDoesNotExist:
            result = authenticate_use_case(phone_number, password, authenticate.repository.password_for_staff)

            permissions = []
            if result:
                permissions = permissions_for(phone_number)

            config = admin_config()
            return jsonify({
                'success': result,
                'type': 'staff',
                'permissions': permissions,
                'config': config,
            })

    except (KeyError, util.exceptions.InvalidInput) as e:
        print(e)
        return respond_with_error(AuthenticateErrorCode.INVALID_INPUT)
    except authenticate.exceptions.IncorrectPassword:
        return respond_with_error(AuthenticateErrorCode.INVALID_PASSWORD)
    except authenticate.exceptions.CustomerDoesNotExist:
        return respond_with_error(AuthenticateErrorCode.CUSTOMER_DOES_NOT_EXIST)


class MenuErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    NOT_A_REGULAR_CUSTOMER = 2


@app.route(f'{base_url}/menu', methods=['POST'])
def menu_for_me_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        _menu = get_menu(request.json['phone_number'], request.json['password'], auth_func, datetime.now())

        return jsonify(_menu)

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(MenuErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(MenuErrorCode.AUTHENTICATION_ERROR)


class DeliverablesErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/deliverables', methods=['POST'])
def get_all_deliverables_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        return jsonify(
            get_all_deliverables(request.json['phone_number'], request.json['password'], auth_func)
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(DeliverablesErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(DeliverablesErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/deliverables_from_today', methods=['POST'])
def deliverables_from_tomorrow_to_till_date_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        return jsonify(
            deliverables_from_today(request.json['phone_number'],
                                    request.json['password'],
                                    auth_func,
                                    datetime.now()
                                    )
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(DeliverablesErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(DeliverablesErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/cancelled_deliverables_from_tomorrow', methods=['POST'])
def cancelled_deliverables_from_tomorrow_to_till_date_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        return jsonify(
            cancelled_deliverables_from_tomorrow(request.json['phone_number'],
                                                 request.json['password'],
                                                 auth_func,
                                                 datetime.now()
                                                 )
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(DeliverablesErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(DeliverablesErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/cancel_deliverables_for_date', methods=['POST'])
def cancel_deliverables_for_date_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        cancel_deliverables_for_date_use_case(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            date.fromisoformat(request.json['date']),
            datetime.now()
        )
        return {}

    except (ValueError, KeyError, util.exceptions.InvalidInput):
        return respond_with_error(DeliverablesErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(DeliverablesErrorCode.AUTHENTICATION_ERROR)


class MealsIBuyErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


class CancelErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    NOT_A_REGULAR_CUSTOMER = 2


"""
A wrapper around is regular customer. It takes a phone number and returns whether customer is
regular customer or not. 

The reason I am assigning variable to lambda, is that during test, I can
reassign auth_func to something else for mocking it. This is possible because everything in 
python is a pointer.

Adding a type hint apparently fixes the warning.
"""
is_regular_customer_func: Callable[[Any], Any] = lambda pho_no: is_regular_customer(pho_no)


@app.route(f'{base_url}/cancel2', methods=['POST'])
def cancel2():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        cancel2_use_case(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['package_id'],
            request.json['quantity'],
            date.fromisoformat(request.json['date']),
            date.today(),
        )

        return {}
    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CancelErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CancelErrorCode.AUTHENTICATION_ERROR)


class ResumeErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    PAYMENT_VERIFICATION_FAILED = 2


class AmIRegularCustomerErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/am_i_regular_customer', methods=['POST'])
def am_i_regular_customer_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify({
            'result': am_i_regular_customer_use_case(request.json['phone_number'], request.json['password'], auth_func,
                                                     is_regular_customer_func)
        })

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AmIRegularCustomerErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AmIRegularCustomerErrorCode.AUTHENTICATION_ERROR)


class TransactionErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    UNAUTHORIZED_ACCESS = 2
    TRANSACTION_ID_DOES_NOT_EXIST = 3


@app.route(f'{base_url}/transaction_id', methods=['POST'])
def generate_transaction_id_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify({
            'result': generate_transaction_id(
                request.json['phone_number'],
                request.json['password'],
                auth_func,
                TransactionRepository(),
                datetime.now()
            )
        })
    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(TransactionErrorCode.INVALID_INPUT)


class CuisineIDErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_FAILED = 1


@app.route(f'{base_url}/cuisine_id', methods=['POST'])
def cuisine_id_route():
    """
    Reusing schedule.exceptions.InvalidInput cause it doesn't make sense to create another class or
    enum for that matter just for Invalid Input and authentication
    """
    # noinspection PyBroadException
    # auth func throws different exceptions for different failure.
    # We dont care about any of them. If auth fails, input is invalid
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        auth_func(request.json['phone_number'], request.json['password'])

        return jsonify({
            'cuisine_id': SaveScheduleRepository().cuisine_id_for(request.json['phone_number'])
        })

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CuisineIDErrorCode.INVALID_INPUT)
    except Exception:
        return respond_with_error(CuisineIDErrorCode.AUTHENTICATION_FAILED)


class NameErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/utility_information', methods=['POST'])
def utility_information_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify(
            utility_information_use_case(request.json['phone_number'],
                                         request.json['password'],
                                         auth_func)
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(NameErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(NameErrorCode.AUTHENTICATION_ERROR)


class CustomerPaymentInfoErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/payment_info', methods=['POST'])
def payment_info_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify(payment_info_use_case(request.json['phone_number'],
                                             request.json['password'],
                                             auth_func)
                       )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CustomerPaymentInfoErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CustomerPaymentInfoErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/booked_payment_info', methods=['POST'])
def booked_payment_info_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify(booked_payment_info(request.json['phone_number'],
                                           request.json['password'],
                                           auth_func)
                       )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CustomerPaymentInfoErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CustomerPaymentInfoErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/credit', methods=['POST'])
def get_credit_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        credit, deposit = get_credit_use_case(request.json['phone_number'], request.json['password'], auth_func, )
        return {
            'credit': credit,
            'deposit': deposit,
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CustomerPaymentInfoErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CustomerPaymentInfoErrorCode.AUTHENTICATION_ERROR)


class ChangeHomeAddressErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    LOCALITY_ID_DOES_NOT_EXIST = 2


@app.route(f'{base_url}/change_home_address', methods=['POST'])
def change_home_address_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        change_home_address_use_case(
            request.json['phone_number'],
            request.json['password'],
            HomeAddress.from_json(request.json['home_address']),
            auth_func,
        )

        return {
            'success': True
        }
    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ChangeHomeAddressErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ChangeHomeAddressErrorCode.AUTHENTICATION_ERROR)
    except change_home_address.exceptions.LocalityIDDoesNotExist:
        return respond_with_error(ChangeHomeAddressErrorCode.LOCALITY_ID_DOES_NOT_EXIST)


class ChangeOfficeAddressErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    AREA_ID_DOES_NOT_EXIST = 2


@app.route(f'{base_url}/change_office_address', methods=['POST'])
def change_office_address_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        change_office_address_use_case(request.json['phone_number'],
                                       request.json['password'],
                                       OfficeAddress.from_json(request.json['office_address']),
                                       auth_func,
                                       )

        return {
            'success': True
        }
    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ChangeOfficeAddressErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ChangeOfficeAddressErrorCode.AUTHENTICATION_ERROR)
    except change_office_address.exceptions.AreaIDDoesNotExist:
        return respond_with_error(ChangeOfficeAddressErrorCode.AREA_ID_DOES_NOT_EXIST)


class ChangeCuisineErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    CUISINE_ID_DOES_NOT_EXIST = 2


@app.route(f'{base_url}/change_cuisine', methods=['POST'])
def change_cuisine_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        change_cuisine_use_case(request.json['phone_number'],
                                request.json['password'],
                                request.json['cuisine_id'],
                                auth_func
                                )

        return {
            'success': True
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ChangeCuisineErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ChangeCuisineErrorCode.AUTHENTICATION_ERROR)
    except change_cuisine.exceptions.CuisineIDDoesNotExist:
        return respond_with_error(ChangeCuisineErrorCode.CUISINE_ID_DOES_NOT_EXIST)


class ChangeEmailErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    INVALID_EMAIL = 2


@app.route(f'{base_url}/change_email', methods=['POST'])
def change_email_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        change_email_use_case(request.json['phone_number'],
                              request.json['password'],
                              request.json['email'],
                              auth_func
                              )

        return {
            'success': True
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ChangeCuisineErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ChangeEmailErrorCode.AUTHENTICATION_ERROR)
    except change_email.exceptions.InvalidEmail:
        return respond_with_error(ChangeEmailErrorCode.INVALID_EMAIL)


class GetCustomerDetailsErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/customer_details', methods=['POST'])
def get_customer_details_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify(get_customer_details(request.json['phone_number'],
                                            request.json['password'],
                                            auth_func
                                            )
                       )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(GetCustomerDetailsErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(GetCustomerDetailsErrorCode.AUTHENTICATION_ERROR)


class GetHomeAddressErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/home_address', methods=['POST'])
def get_home_address_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify(
            get_home_address_use_case(
                request.json['phone_number'],
                request.json['password'],
                auth_func
            )
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(GetHomeAddressErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(GetHomeAddressErrorCode.AUTHENTICATION_ERROR)


class GetOfficeAddressErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/office_address', methods=['POST'])
def get_office_address_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        return jsonify(get_office_address_use_case(request.json['phone_number'],
                                                   request.json['password'],
                                                   auth_func
                                                   )
                       )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(GetOfficeAddressErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(GetOfficeAddressErrorCode.AUTHENTICATION_ERROR)


class BookingErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    INVALID_TRANSACTION_ID = 2
    PAYMENT_VERIFICATION_FAILED = 3
    NOT_SCHEDULED = 4
    NO_PACKAGE_BOOKED_FOR_MORE_THAN_3_DAYS = 5


@app.route(f'{base_url}/save_schedule', methods=['POST'])
def save_schedule_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        packages = []

        for json_ in request.json['schedule']:
            packages.append(PackageToSchedule.from_json(json_))

        save_schedule_use_case(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            packages,
            SaveScheduleRepository(),
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput) as e:
        print(e)
        return respond_with_error(BookingErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(BookingErrorCode.AUTHENTICATION_ERROR)
    except save_schedule.exceptions.NoPackageBookedForMoreThan3Days:
        return respond_with_error(BookingErrorCode.NO_PACKAGE_BOOKED_FOR_MORE_THAN_3_DAYS)


@app.route(f'{base_url}/book/spot2', methods=['POST'])
def book_spot2_route():
    try:
        if not request.is_json:
            logger.info('request does not contain json')
            raise util.exceptions.InvalidInput

        phone_number = request.json['phone_number']
        password = request.json['password']

        return jsonify(
            book_spot2_use_case(
                phone_number,
                password,
                auth_func,
                request.json['reservation_id'],
                BookingRepository(),
                datetime.now(),
                manipulate_price2
            )
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(BookingErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(BookingErrorCode.AUTHENTICATION_ERROR)
    except book_spot.exceptions.InvalidTransactionID:
        return respond_with_error(BookingErrorCode.INVALID_TRANSACTION_ID)
    except book_spot.exceptions.PaymentVerificationFailed:
        return respond_with_error(BookingErrorCode.PAYMENT_VERIFICATION_FAILED)


@app.route(f'{base_url}/book/schedule', methods=['POST'])
def book_schedule_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        config = front_end_config(request.json['phone_number'])
        use_case = BookSchedule(auth_func, calculate_scheduled_till_date, BookScheduleRepository(config))
        now = datetime.now()
        update_cart = booking_started_for_tomorrow(now.time())

        success = use_case.schedule(
            request.json['phone_number'],
            request.json['password'],
            now,
            update_cart,
        )

        return {
            'success': success
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ErrorCode.AUTHENTICATION_ERROR)


class ReserveErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


@app.route(f'{base_url}/reserve', methods=['POST'])
def reserve_route():
    try:
        if not request.is_json:
            logger.info('Request does not contain json')
            raise util.exceptions.InvalidInput

        temp = request.json['packages_to_reserve']

        packages_to_reserve = {}

        for package in temp:
            packages_to_reserve[package['package_id']] = ReservationDetails.from_json(package)

        return jsonify(
            reserve_use_case(
                request.json['phone_number'],
                request.json['password'],
                packages_to_reserve,
                auth_func,
                ReserveRepository(),
                datetime.now(),
            )
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ReserveErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ReserveErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/reserve/next_working_day', methods=['POST'])
def reserve_for_next_working_day_route():
    try:
        if not request.is_json:
            logger.info('Request does not contain json')
            raise util.exceptions.InvalidInput

        temp = request.json['packages_to_reserve']

        packages_to_reserve = {}

        for package in temp:
            packages_to_reserve[package['package_id']] = ReservationDetails.from_json(package)

        return jsonify(
            reserve_use_case(
                request.json['phone_number'],
                request.json['password'],
                packages_to_reserve,
                auth_func,
                ReserveRepository(),
                datetime.now(),
                reserve_for_next_working_day=True
            )
        )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ReserveErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ReserveErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/cancel_reservation', methods=['POST'])
def cancel_reservation_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        cancel_reservation_use_case(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['reservation_id'],
            datetime.now()
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ResumeErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ResumeErrorCode.AUTHENTICATION_ERROR)


class CartErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    INVALID_DELIVERY_LOCATION = 2


@app.route(f'{base_url}/cart', methods=['PUT'])
def set_package_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        set_in_cart(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['package_id'],
            request.json['quantity'],
        )

        return {
            'success': True
        }

    except (KeyError, util.exceptions.InvalidInput) as e:
        print(e)
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/cart2', methods=['POST'])
def get_cart_v2():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        return cartV2(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
        )

    except (KeyError, util.exceptions.InvalidInput) as e:
        print(e)
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/cart2/clear', methods=['POST'])
def clear_cart2_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        clear_cart2(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
        )

        return {
            'success': True
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/delete_from_cart', methods=['POST'])
def delete_from_cart_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        delete_from_cart(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['package_id'],
        )

        return {
            'success': True
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/cart/delivery_location', methods=['PUT'])
def update_delivery_location():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        set_delivery_location(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['meal_id'],
            request.json['delivery_location']
        )

        return {
            'success': True
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/transaction_history', methods=['POST'])
def transaction_history_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        return jsonify(transaction_history(request.json['phone_number'],
                                           request.json['password'],
                                           auth_func,
                                           TransactionRepository()
                                           )
                       )

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/refund', methods=['POST'])
def refund_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        net_banking_details = NetBankingDetails.from_json(
            request.json.get('net_banking_details')
        )

        refund(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            datetime.now(),
            PaymentMode.from_int(request.json['payment_mode']),
            request.json.get('upi_id'),
            net_banking_details,
        )
        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/discontinue_service', methods=['POST'])
def discontinue_service_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        net_banking_details = NetBankingDetails.from_json(
            request.json.get('net_banking_details')
        )

        discontinue_service(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            datetime.now(),
            PaymentMode.from_int(request.json.get('payment_mode')),
            request.json.get('upi_id'),
            net_banking_details,
        )
        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(CartErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(CartErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/front_end_config')
def front_end_config_route():
    return jsonify(front_end_config(request.args.get('phone_number', None)))


"""
The end points below are temporary. Because Heroku sleeps the dyno
after 30 minutes of inactivity, we need a way to run these scheduled
tasks manually and hence this end point. Because this is temporary,
no authentication is performed. 
"""


@app.route(f'{base_url}/delivery_charge_v2', methods=['POST'])
def delivery_charge_route_v2():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        home, office = delivery_charge_use_case_v2(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
        )

        return {
            'home': home,
            'office': office,
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/change_password', methods=['POST'])
def change_password_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        change_password(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['new_password']
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(ErrorCode.AUTHENTICATION_ERROR)


@app.route(f'{base_url}/send_forgot_password_email', methods=['POST'])
def send_forgot_password_email_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        send_forgot_password_email(
            request.json['phone_number'],
            request.json['email'],
            datetime.now()
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ErrorCode.INVALID_INPUT)


@app.route(f'{base_url}/reset_password', methods=['POST'])
def reset_password_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        reset_password(
            request.json['reset_token'],
            request.json['new_password'],
            datetime.now()
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(ErrorCode.INVALID_INPUT)


@app.route(f'{base_url}/not_serving', methods=['GET'])
def not_serving():
    return not_serving_repository(date.today())


@app.route(f'{base_url}/door_step_delivery', methods=['GET'])
def door_step_delivery_route():
    return {
        'door_step_delivery': door_step_delivery(),
    }


@app.route(f'/log', methods=['POST'])
def log():
    logger.debug(request.get_data())
    return {}


@app.get(f'{base_url}/societies_and_offices')
def societies_route():
    return jsonify({
        'societies': get_societies(),
        'offices': get_offices(),
    })


if __name__ == '__main__':
    app.run(port=os.getenv('PORT'), threaded=True, debug=IS_STAGING)
