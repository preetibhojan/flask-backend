import psycopg2
from psycopg2.extras import execute_values

import config
import util.database
from book_spot.model import ReservedPackage
from book_spot.use_case import manipulate_price2
from util.constants import HOME
from util.exceptions import AuthenticationError, InvalidInput


def cancel2_use_case(phone_number, password, auth_func, package_to_cancel, quantity_to_subtract, date_to_cancel,
                     current_date):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    if date_to_cancel < current_date:
        raise InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = util.database.get_customer_id(cur, phone_number)
        home_delivery_charge, office_delivery_charge = util.database.delivery_charge_for_v2(cur, phone_number)

        cur.execute(
            'with deleted_deliverables as ('
            'delete from deliverables where customer_id = %s '
            'and date = %s '
            'and status = \'ordered\' '
            'returning package_id, quantity, price, delivery_location, door_step_delivery '
            ') '
            'select package_id, quantity, dd.price, p.price, meal_id, delivery_location, cuisine_id, '
            'complimentary, add_on, door_step_delivery '
            'from deleted_deliverables dd '
            'inner join packages p '
            'on p.id = dd.package_id',
            (customer_id, date_to_cancel)
        )

        packages = []

        deleted_deliverable_cost = 0

        for package_id, quantity, deleted_deliverable_price, package_price, meal_id, delivery_location, \
            cuisine_id, complimentary, add_on, door_step_delivery in cur.fetchall():
            delivery_charge = home_delivery_charge[meal_id] \
                if delivery_location == HOME \
                else office_delivery_charge[meal_id]

            deleted_deliverable_cost += float(deleted_deliverable_price)

            packages.append(ReservedPackage(
                package_id,
                package_price,
                quantity,
                meal_id,
                cuisine_id,
                delivery_location,
                delivery_charge,
                complimentary,
                add_on,
                door_step_delivery=door_step_delivery
            ))

        if len(packages) == 0:
            raise InvalidInput

        package = next(filter(lambda p: p.id == package_to_cancel, packages), None)

        if package is None:
            raise InvalidInput

        if package.quantity < quantity_to_subtract:
            raise InvalidInput

        package.quantity -= quantity_to_subtract

        new_packages = list(filter(lambda p: p.quantity > 0, manipulate_price2(phone_number, packages, date_to_cancel)))

        insert_template = cur.mogrify('(%s, %%s, %%s, %%s, %s, %%s, %%s)', (customer_id, date_to_cancel)).decode('utf-8')

        execute_values(
            cur,
            'insert into deliverables(customer_id, package_id, quantity, '
            'delivery_location, date, price, door_step_delivery) '
            'values %s',
            list(map(lambda p: (p.id, p.quantity, p.delivery_location, p.price, p.door_step_delivery), new_packages)),
            template=insert_template
        )

        cur.execute(
            'update package_quantity set quantity = quantity + %s '
            'where package_id = %s and date = %s',
            (quantity_to_subtract, package.id, date_to_cancel)
        )

        new_cost = sum(map(lambda p: p.price, new_packages))

        cur.execute(
            'update paymentdetail set temp_credit = temp_credit + %s '
            'where customer_id = %s',
            (deleted_deliverable_cost - new_cost, customer_id)
        )

        conn.commit()
