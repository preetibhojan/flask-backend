import logging

import psycopg2

import config
from change_home_address.exceptions import LocalityIDDoesNotExist
from util.database import get_customer_id
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger(__name__)


def change_home_address_use_case(phone_number, password, home_address, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication failed')
        raise AuthenticationError

    if not home_address.is_valid():
        raise InvalidInput

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute('select id from tower where id = %(id)s '
                    'and home is true',
                    {
                        'id': home_address.society_id
                    })

        if cur.rowcount == 0:
            raise LocalityIDDoesNotExist

        cur.execute(
            'insert into homeaddress(customer_id, flat_number, building, '
            'tower_id, door_step_delivery) '
            'values (%(cust_id)s, %(flat_no)s, %(building)s, %(soc_id)s, %(door_step_delivery)s) '
            'on conflict (customer_id) do update set '
            'flat_number = %(flat_no)s, '
            'building = %(building)s, '            
            'tower_id = %(soc_id)s, '
            'door_step_delivery = %(door_step_delivery)s',
            {
                'cust_id': customer_id,
                'flat_no': home_address.flat_number,
                'building': home_address.building,
                'soc_id': home_address.society_id,
                'door_step_delivery': home_address.door_step_delivery
            }
        )

        logger.debug(cur.query)

        conn.commit()

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()
