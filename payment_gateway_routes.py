import logging
from datetime import datetime

import psycopg2
from flask import Blueprint, request

import config
from book_schedule.repository import BookScheduleRepository
from book_schedule.use_case import BookSchedule, calculate_scheduled_till_date
from book_spot.repository import BookingRepository
from book_spot.use_case import book_spot2_use_case, manipulate_price2
from front_end_config.repository import front_end_config
from payment_gateway.initiate_transaction.use_case import initiate_transaction_use_case, \
    initiate_transaction_v2_use_case, SCHEDULE, SPOT_BOOKING2
from payment_gateway.paytm.gateway_implementation import PayTM
from payment_gateway.update_transaction_status.repository import update_transaction_status
from transactions.transactionID import transaction_id
from util.database import get_customer_id, booking_started_for_tomorrow
from util.exceptions import InvalidInput, AuthenticationError
from util.flask import respond_with_error, ErrorCode, auth_func

payment_gateway = Blueprint('payment_gateway', __name__)

logger = logging.getLogger(__name__)


@payment_gateway.route('/initiate_transaction', methods=['POST'])
def initiate_transaction_route():
    try:
        if not request.is_json:
            raise InvalidInput

        def _customer_id(phone_number):
            with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
                return get_customer_id(cur, phone_number)

        t_id, token, callback_url = initiate_transaction_use_case(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['amount'],
            lambda cust_id, amount: transaction_id(cust_id, amount, datetime.now()),
            PayTM(),
            _customer_id,
        )

        return {
            'transaction_id': t_id,
            'merchant_id': config.MERCHANT_ID,
            'token': token,
            'callback_url': callback_url,
            'is_staging': config.IS_STAGING,
        }

    except (KeyError, InvalidInput):
        return respond_with_error(ErrorCode.INVALID_INPUT)
    except AuthenticationError:
        return respond_with_error(ErrorCode.AUTHENTICATION_ERROR)


@payment_gateway.route('/initiate_transaction_v2', methods=['POST'])
def initiate_transaction_route_v2():
    try:
        if not request.is_json:
            raise InvalidInput

        def _customer_id(phone_number):
            with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
                return get_customer_id(cur, phone_number)

        t_id, token, callback_url = initiate_transaction_v2_use_case(
            request.json['phone_number'],
            request.json['password'],
            auth_func,
            request.json['amount'],
            request.json['source'],
            request.json['reservation_id'],
            lambda cust_id, amount, source, reservation_id:
            transaction_id(cust_id, amount, datetime.now(), source, reservation_id),
            PayTM(),
            _customer_id,
        )

        return {
            'transaction_id': t_id,
            'merchant_id': config.MERCHANT_ID,
            'token': token,
            'callback_url': callback_url,
            'is_staging': config.IS_STAGING,
        }

    except (KeyError, InvalidInput):
        return respond_with_error(ErrorCode.INVALID_INPUT)
    except AuthenticationError:
        return respond_with_error(ErrorCode.AUTHENTICATION_ERROR)


@payment_gateway.route('/payment_status', methods=['POST'])
def payment_status_route():
    # We need this assignment. If there is an error before result is
    # assigned, we will get a name error.
    result_tuple = None
    try:
        paytm_params = request.form.to_dict()
        logger.debug(f'paytm params: {paytm_params}')

        result_tuple = update_transaction_status(paytm_params)
    except InvalidInput:
        logger.debug('Invalid input')
        logger.debug(f'paytm params: {request.form.to_dict()}')
        return {}
    except Exception as e:
        logger.critical(e)
        logger.debug(f'paytm params: {request.form.to_dict()}')
        return respond_with_error(ErrorCode.INVALID_INPUT)

    try:
        # if transaction is failed, dont do anything. update_transaction_status will
        # return nothing when transaction fails
        if not result_tuple:
            return {}

        source, reservation_id, phone_number = result_tuple

        auth_func = lambda _, __: None

        if source == SCHEDULE:
            config = front_end_config(phone_number)
            use_case = BookSchedule(
                auth_func,
                calculate_scheduled_till_date,
                BookScheduleRepository(config),
            )
            now = datetime.now()
            update_cart = booking_started_for_tomorrow(now.time())

            success = use_case.schedule(
                phone_number,
                '',
                now,
                update_cart,
            )

            if not success:
                logger.critical('Schedule failed!!!!')
        elif source == SPOT_BOOKING2:
            book_spot2_use_case(
                phone_number,
                '',
                auth_func,
                reservation_id,
                BookingRepository(),
                datetime.now(),
                manipulate_price2,
            )
        # explicitly not handling recharge or resume because in those cases, we just
        # credit the amount which is already done by update_transaction_status
    except Exception as e:
        logger.info(f'Failed to book: {e}')

    return {}
