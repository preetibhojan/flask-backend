import psycopg2

import config
from util.exceptions import AuthenticationError


def customers(auth_func, phone_number, password):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        home_address_charge, office_address_charge = _delivery_charge(cur)
        cur.execute(
            'select c.id, isregularcustomer, first_name, last_name, h.door_step_delivery, pd.temp_credit, phone_number '
            'from customer c '
            'inner join paymentdetail pd on c.id = pd.customer_id '
            'left outer join homeaddress h '
            'on c.id = h.customer_id '
        )

        result = {}
        for id, regular_customer, first_name, last_name, door_step_delivery, temp_credit, phone_number in cur.fetchall():
            result[id] = {
                'id': id,
                'regular_customer': regular_customer,
                'first_name': first_name,
                'last_name': last_name,
                # door step delivery can be None. So convert it to
                # False if needed
                'door_step_delivery': bool(door_step_delivery),
                'regular_packages': 0,

                # Adding none here to handle the case where office address is not set
                'home_address_delivery_charge': home_address_charge.get(id, None),
                'office_address_delivery_charge': office_address_charge.get(id, None),
                'temp_credit': float(temp_credit),

                'phone_number': phone_number,
            }

        cur.execute(
            'with package_quantity as ( '
            '    select distinct customer_id, package_id, quantity '
            '    from tiffininformation '
            '         inner join packages p '
            '         on p.id = tiffininformation.package_id '
            '    where customer_id in (select distinct customer_id from tiffininformation) '
            '      and p.add_on is false '
            '), '
            'meal_quantity as ( '
            '    select customer_id, meal_id, sum(quantity) as sum '
            '    from package_quantity '
            '    inner join packages p '
            '        on p.id = package_quantity.package_id '
            '    group by (customer_id, meal_id) '
            ') '
            'select customer_id, max(sum) '
            'from meal_quantity '
            'group by customer_id '
        )

        for customer_id, regular_packages in cur.fetchall():
            result[customer_id]['regular_packages'] = regular_packages

        cur.execute('select id from customer where exceptional is true')
        for (customer_id,) in cur.fetchall():
            result[customer_id]['regular_packages'] = 10 ** 7

        return list(result.values())


def _delivery_charge(cur):
    cur.execute(
        'select id, door_step_delivery_delta '
        'from meals'
    )

    door_step_delivery_delta = {}
    for meal_id, delta in cur.fetchall():
        door_step_delivery_delta[str(meal_id)] = delta

    cur.execute(
        'select customer_id, l.delivery_charge, ha.door_step_delivery '
        'from homeaddress ha '
        'inner join tower t '
        'on t.id = ha.tower_id '
        'and home is true '
        'inner join localities l '
        'on l.id = t.locality_id '
        'and l.home is true '
    )

    home_address_charge = {}

    for customer_id, delivery_charge, door_step_delivery in cur.fetchall():
        home_address_charge[customer_id] = {}
        if door_step_delivery:
            for meal_id, delta in door_step_delivery_delta.items():
                home_address_charge[customer_id][meal_id] = delivery_charge + delta
        else:
            for meal_id in door_step_delivery_delta.keys():
                home_address_charge[customer_id][meal_id] = delivery_charge

    cur.execute(
        'select customer_id, l.delivery_charge '
        'from officeaddress oa '
        'inner join tower t '
        'on oa.tower_id = t.id '
        'and t.home is false '
        'inner join localities l '
        'on l.id = t.locality_id '
        'and l.office is true '
    )

    office_address_charge = {}

    for customer_id, delivery_charge in cur.fetchall():
        office_address_charge[customer_id] = {}
        for meal_id in door_step_delivery_delta.keys():
            office_address_charge[customer_id][meal_id] = delivery_charge

    return home_address_charge, office_address_charge
