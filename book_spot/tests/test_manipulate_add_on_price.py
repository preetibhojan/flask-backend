import unittest

from book_spot.model import ReservedPackage
from book_spot.use_case import manipulate_add_on_price
from util.constants import HOME


class ManipulateAddOnPriceTestCase(unittest.TestCase):
    def test_manipulate_add_on_price_returns_the_updated_price_for_need_based_customer(self):
        add_ons = [
            ReservedPackage(3, 60, 4, 1, 0, HOME, None, True, True),
            ReservedPackage(2, 50, 4, 1, 0, HOME, None, False, True),
            ReservedPackage(1, 30, 4, 1, 0, HOME, None, False, True),
        ]

        self.assertListEqual(manipulate_add_on_price(add_ons, False), [
            ReservedPackage(3, 240, 4, 1, 0, HOME, None, True, True),
            ReservedPackage(2, 200, 4, 1, 0, HOME, None, False, True),
            ReservedPackage(1, 120, 4, 1, 0, HOME, None, False, True),
        ])

    def test_manipulate_add_on_price_provides_one_complimentary_package_for_free_for_regular_customer(self):
        add_ons = [
            ReservedPackage(3, 60, 4, 1, 0, HOME, None, True, True),
            ReservedPackage(2, 50, 4, 1, 0, HOME, None, False, True),
            ReservedPackage(1, 30, 4, 1, 0, HOME, None, False, True),
        ]

        self.assertListEqual(manipulate_add_on_price(add_ons, True), [
            # 60 x 4 is 240 but because the package is complimentary,
            # one if given for free. Hence the price of 240 - 60 = 180
            ReservedPackage(3, 180, 4, 1, 0, HOME, None, True, True),
            ReservedPackage(2, 200, 4, 1, 0, HOME, None, False, True),
            ReservedPackage(1, 120, 4, 1, 0, HOME, None, False, True),
        ])

        add_ons = [
            ReservedPackage(1, 30, 1, 1, None, HOME, None, True, True),
        ]

        self.assertListEqual(manipulate_add_on_price(add_ons, True), [
            ReservedPackage(1, 0, 1, 1, None, HOME, None, True, True),
        ])
