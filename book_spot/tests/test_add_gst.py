import unittest

from book_spot.model import ReservedPackage
from book_spot.use_case import add_gst_and_discount_if_regular_customer
from util.constants import HOME


class AddGSTTestCase(unittest.TestCase):
    packages = [
        ReservedPackage(1, 40, 2, 1, 0, HOME, 10, None, False),
        ReservedPackage(2, 50, 3, 2, 0, HOME, 10, None, False),
        ReservedPackage(3, 20, 4, 1, 0, HOME, 10, None, False)
    ]

    need_based_customer_delta = {
        1: 10,
        2: 5
    }

    gst = 5

    def test_add_gst_works_for_regular_customer(self):
        packages = add_gst_and_discount_if_regular_customer(
            '', self.packages, self.need_based_customer_delta,
            self.gst, lambda _: True, 2,
            {
                1: 1,
            }
        )

        for p in packages:
            print(f'{p.id} {p.price}')

        cost = sum(map(lambda p: p.price, packages))
        self.assertEqual(len(packages), 3)
        self.assertEqual(next(filter(lambda p: p.id == 1, packages)).price, 115.5)
        self.assertEqual(next(filter(lambda p: p.id == 2, packages)).price, 194.25)
        self.assertEqual(next(filter(lambda p: p.id == 3, packages)).price, 168)
        self.assertEqual(cost, 477.75)

    def test_add_gst_does_not_discount_if_quantity_is_greater_than_scheduled_quantity(self):
        packages = add_gst_and_discount_if_regular_customer(
            '', self.packages, self.need_based_customer_delta,
            self.gst, lambda _: True, 2,
            {
                1: 2,
                2: 1,
            }
        )

        for p in packages:
            print(f'{p.id} {p.price}')

        cost = sum(map(lambda p: p.price, packages))
        self.assertEqual(len(packages), 3)
        self.assertEqual(next(filter(lambda p: p.id == 1, packages)).price, 126)
        self.assertEqual(next(filter(lambda p: p.id == 2, packages)).price, 199.5)
        self.assertEqual(next(filter(lambda p: p.id == 3, packages)).price, 168)
        self.assertEqual(cost, 493.5)

    def test_add_gst_works_for_need_based_customer(self):
        packages = add_gst_and_discount_if_regular_customer(
            '', self.packages, self.need_based_customer_delta,
            self.gst, lambda _: False, 1, {
                1: 1,
                2: 2,
            })

        for p in packages:
            print(f'{p.id} {p.price}')

        cost = sum(map(lambda p: p.price, packages))
        self.assertEqual(next(filter(lambda p: p.id == 1, packages)).price, 126)
        self.assertEqual(next(filter(lambda p: p.id == 2, packages)).price, 204.75)
        self.assertEqual(next(filter(lambda p: p.id == 3, packages)).price, 168)

        self.assertEqual(cost, 498.75)


if __name__ == '__main__':
    unittest.main()
