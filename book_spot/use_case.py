"""
Used to book_spot according to schedule
"""

import logging

import psycopg2

from config import DATABASE_URL
from front_end_config.repository import front_end_config
from util.constants import HOME, OFFICE
from util.exceptions import AuthenticationError
from util.exceptions import InvalidInput
from util.notification import send_notification

logger = logging.getLogger(__name__)


def book_spot2_use_case(phone_number, password, auth_func, reservation_id,
                        repository, now, manipulate_price):
    """
    Spot booking of packages. Overwrites deliverables for customer meal ids of packages reserved
    :param phone_number: phone number of customer
    :param password: password of customer
    :param auth_func: a function that throws exception on authentication failure. Returns
    None otherwise
    :param reservation_id: reservation id returned by reserve
    :param repository: repository to use
    :param now: current date time
    :param manipulate_price: The price of package returned by repository is the price for 1 quantity
    and for regular customer. But deliverables needs the total price charged for that package including
    tax and regular customer discount if applicable.
    A function that takes phone number and packages and -
    1. sets price according to quantity.
    2. adds tax to price
    3. subtracts regular customer discount if applicable
    :return:
    """
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication error')
        raise AuthenticationError

    if not reservation_id:
        logger.info('Reservation id is Non')
        raise InvalidInput

    try:
        timestamp, packages, date_to_reserve = repository.reservation_details(phone_number, reservation_id)

        logger.debug(f'reservation time: {timestamp}')
        logger.debug(f'date: {date_to_reserve}')
        logger.debug('packages: [')

        for package in packages:
            logger.debug(f'{package},')
        logger.debug(']')

        reservation_exist = timestamp is not None
        # if reservation exist, timestamp will not be null
        if not reservation_exist:
            logger.info('reservation id does not exist for phone number')
            raise InvalidInput

        difference_in_minutes = (now - timestamp).seconds / 60

        if difference_in_minutes >= 10:
            logger.info('reservation id issued before 10 minutes')
            raise InvalidInput

        """
        Meal IDs dont need to be checked because meal ids which cannot
        be ordered are set to "delivered" status in deliverables and
        delete deliverables will only delete where status is ordered
        """

        rough_price = sum(map(lambda p: p.price * p.quantity, packages))
        logger.debug(f'rough price: {rough_price}')

        packages = manipulate_price(phone_number, packages, date_to_reserve)
        logger.debug('packages after manipulating price: ')

        for package in packages:
            logger.debug(f'{package}')

        money_available = repository.temp_credit(phone_number)
        logger.debug(f'money available: {money_available}')

        cost = sum(map(lambda p: p.price, packages))
        logger.debug(f'cost: {cost}')

        # something is really wrong if cost < rough price.
        if cost < rough_price:
            raise InvalidInput

        if money_available < cost:
            logger.info(f'money available <= cost. money available: {money_available}, cost: {cost}')
            raise InvalidInput

        new_credit = money_available - cost
        logger.debug(f'setting credit to {new_credit}')
        repository.set_credit(phone_number, new_credit)

        repository.insert_deliverables(phone_number, packages, date_to_reserve)
        repository.remove_reservation(reservation_id)
        repository.commit()

        logger.info('success')

        send_notification(
            phone_number,
            'Tiffin Booked Successfully',
            'Dear Customer, You Tiffin was booked Successfully',
        )

        logger.info('tiffin booked successfully message sent')

        return {
            'success': True
        }
    except:
        send_notification(
            phone_number,
            'Tiffin Booking Failed',
            'Dear Customer, You Tiffin was not booked. Please try again or contact 7030700477 for help',
        )
        logger.info('tiffin not booked message sent')
        raise
    finally:
        repository.unlock()


# the default argument for packages_ordered_wrt_meal is never changed.
# So, its fine
# noinspection PyDefaultArgument
def add_gst_and_discount_if_regular_customer(
        phone_number,
        packages,
        need_based_customer_delta,
        config,
        is_regular_customer,
        max_quantity_scheduled_per_meal,
        packages_ordered_wrt_meal={},
):
    logger.debug(f'max quantity scheduled: {max_quantity_scheduled_per_meal}')
    logger.debug(f'packages ordered wrt meal: {packages_ordered_wrt_meal}')

    def _add_delivery_charge_and_need_based_delta(p):
        logger.debug(f'price: {p.price}, delivery charge: {p.delivery_charge}, '
                     f'need based customer delta: {need_based_customer_delta[p.meal_id]}, '
                     f'quantity: {p.quantity}')

        new_price = (p.price + p.delivery_charge + need_based_customer_delta[p.meal_id]) * p.quantity
        logger.debug(f'new price: {new_price}')
        return p.copy_with(new_price)

    packages = list(map(_add_delivery_charge_and_need_based_delta, packages))

    if is_regular_customer(phone_number):
        logger.debug('customer is regular')

        distinct_meal_ids = set(map(lambda p: p.meal_id, packages))
        logger.debug(f'distinct meal ids: {distinct_meal_ids}')

        for meal_id in distinct_meal_ids:
            packages_with_meal_id = list(filter(lambda p: p.meal_id == meal_id, packages))
            logger.debug(f'packages with meal id: {meal_id}:')

            for package in packages_with_meal_id:
                logger.debug(f'{package}')

            quantity_to_book = sum(map(lambda p: p.quantity, packages_with_meal_id))
            logger.debug(f'quantity to book: {quantity_to_book}')

            count = min(quantity_to_book, max_quantity_scheduled_per_meal - packages_ordered_wrt_meal.get(meal_id, 0))
            logger.debug(f'count of packages to discount per meal: {count}')

            for package in packages_with_meal_id:
                if count < 1:
                    break

                logger.debug(f'count: {count}')
                logger.debug(f'quantity of package {package.id}: {package.quantity}')

                quantity = min(package.quantity, count)
                logger.debug(f'quantity to remove need based delta from: {quantity}')
                logger.debug(f'need based delta: {need_based_customer_delta[package.meal_id]}')

                price_subtracted = need_based_customer_delta[package.meal_id] * quantity
                package.price -= price_subtracted
                logger.debug(f'price subtracted: {price_subtracted}')

                count -= package.quantity

    gst_at_home = config['gst'][str(HOME)]
    logger.debug(f'gst at home is: {gst_at_home}')

    gst_at_office = config['gst'][str(OFFICE)]
    logger.debug(f'gst at office is: {gst_at_office}')

    for package in packages:
        logger.debug(f'price of package: {package.id}: {package.price}')
        logger.debug(f'delivery location {package.id}: {package.delivery_location}')

        gst_percent = config['gst'][str(package.delivery_location)]
        logger.debug(f'gst percent: {gst_percent}')

        gst = package.price * gst_percent / 100
        logger.debug(f'gst: {gst}')

        package.price += gst
        logger.debug(f'price after adding gst: {package.price}')

    return packages


def manipulate_add_on_price(add_on_packages, regular_customer):
    add_ons = []

    for add_on in add_on_packages:
        quantity = add_on.quantity
        logger.debug(f'add on: {add_on.id}, quantity: {quantity}')

        if regular_customer and add_on.complimentary:
            quantity -= 1
            logger.debug(
                f'{add_on.id} is complimentary and customer is regular. '
                f'Subtracting 1 quantity for price calculation'
            )
            logger.debug(f'new quantity: {quantity}')

        logger.debug(f'old price: {add_on.price}')
        new_price = add_on.price * quantity
        add_ons.append(add_on.copy_with(new_price))
        logger.debug(f'new price: {new_price}')

    return add_ons


def manipulate_price2(phone_number, packages, date):
    # we need to pass phone number to get need based customer delta
    config = front_end_config(phone_number=phone_number)
    need_based_customer_delta = config['need_based_customer_delta']

    normal_packages = list(filter(lambda p: p.add_on is False, packages))
    add_ons = list(filter(lambda p: p.add_on is True, packages))

    with psycopg2.connect(DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select isregularcustomer, exceptional from customer '
            'where phone_number=%s',
            (phone_number,)
        )

        regular_customer, exceptional = cur.fetchone()

        if exceptional:
            regular_customer = True
            max_package_per_meal = 10 ** 7
            packages_ordered_wrt_meal = {}
        else:
            cur.execute(
                'with package_quantity as '
                '(select distinct package_id, quantity '
                'from tiffininformation '
                'inner join packages p '
                'on p.id = tiffininformation.package_id '
                'where customer_id = (select id from customer where phone_number = %s) '
                'and p.add_on is false), '
                'meal_quantity as (select meal_id, sum(quantity) as sum '
                'from package_quantity '
                'inner join packages p on p.id = package_quantity.package_id '
                'group by meal_id) '
                'select max(sum) '
                'from meal_quantity ',
                (phone_number,)
            )

            max_package_per_meal = cur.fetchone()[0]

            cur.execute(
                'select p.meal_id, sum(quantity) from deliverables '
                'inner join packages p on '
                'p.id = deliverables.package_id '
                'and p.add_on is false '
                'where customer_id = (select id from customer where phone_number = %s) '
                'and date = %s '
                'group by (meal_id, package_id) ',
                (phone_number, date)
            )

            packages_ordered_wrt_meal = {}
            for meal_id, quantity in cur.fetchall():
                packages_ordered_wrt_meal[meal_id] = quantity

    updated_normal_packages = add_gst_and_discount_if_regular_customer(
        phone_number, normal_packages,
        need_based_customer_delta, config,
        lambda *x: regular_customer,
        max_package_per_meal,
        packages_ordered_wrt_meal,
    )

    updated_add_ons = manipulate_add_on_price(
        add_ons,
        regular_customer,
    )

    return updated_normal_packages + updated_add_ons
