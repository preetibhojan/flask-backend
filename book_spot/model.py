class ReservedPackage:
    def __init__(self, id, price, quantity, meal_id, cuisine_id, delivery_location, delivery_charge, complimentary,
                 add_on, door_step_delivery=False, status='ordered'):
        self.id = id
        self.price = price
        self.quantity = quantity
        self.meal_id = meal_id
        self.cuisine_id = cuisine_id
        self.delivery_location = delivery_location
        self.delivery_charge = delivery_charge
        self.complimentary = complimentary
        self.add_on = add_on
        self.door_step_delivery = door_step_delivery
        self.status = status
        
    def copy_with(self, price):
        return ReservedPackage(
            self.id,
            price,
            self.quantity,
            self.meal_id,
            self.cuisine_id,
            self.delivery_location,
            self.delivery_charge,
            self.complimentary,
            self.add_on,
            door_step_delivery=self.door_step_delivery,
            status=self.status,
        )

    def __str__(self):
        return f'id: {self.id}, ' \
               f'price: {self.price}, ' \
               f'quantity: {self.quantity}, ' \
               f'meal id: {self.meal_id}, ' \
               f'cuisine id: {self.cuisine_id}, ' \
               f'delivery location: {self.delivery_location}, ' \
               f'delivery charge {self.delivery_charge}, ' \
               f'complimentary: {self.complimentary}'

    def __format__(self, format_spec):
        return self.__str__()

    def __eq__(self, other):
        return isinstance(other, ReservedPackage) \
            and self.id == other.id \
            and self.price == other.price \
            and self.quantity == other.quantity \
            and self.meal_id == other.meal_id \
            and self.cuisine_id == other.cuisine_id \
            and self.delivery_location == other.delivery_location \
            and self.delivery_charge == other.delivery_charge \
            and self.complimentary == other.complimentary
