"""
Repository for booking
"""

import logging

import psycopg2

import config
import util.database
from book_spot.model import ReservedPackage
from deliverables.model import Deliverable
from util.constants import HOME

logger = logging.getLogger(__name__)


class BookingRepository:
    """
    Repository for booking. 
    Meant to be short lived. Create a new object for each request
    """

    def __init__(self):
        self.conn = None

    def reservation_details(self, phone_number, reservation_id):
        """
        Return the created of creation, the packages reserved and
        the date to reserve for the given customer and reservation id.

        **Note:** Locks the rows in table for update

        :param phone_number: phone number of customer
        :param reservation_id: reservation id
        :return: a tuple with created and a list of packages
        """
        if self.conn is None:
            self.conn = psycopg2.connect(config.DATABASE_URL)

        cur = None
        try:
            cur = self.conn.cursor()

            customer_id = util.database.get_customer_id(cur, phone_number)
            home_delivery_charge, office_delivery_charge = util.database.delivery_charge_for_v2(cur, phone_number)

            door_step_delivery = False
            cur.execute('select door_step_delivery from homeaddress where customer_id = %s', (customer_id,))
            if cur.rowcount > 0:
                door_step_delivery = cur.fetchone()[0]

            cur.execute(
                'select package_id, quantity, price, meal_id, '
                'created, delivery_location, date_to_reserve, '
                'cuisine_id, complimentary, add_on, extra_quantity '
                'from reservations r '
                'inner join packages p '
                'on r.package_id = p.id '
                'where customer_id = %(cust_id)s '
                'and r.id = %(r_id)s for update',
                {
                    'cust_id': customer_id,
                    'r_id': reservation_id
                })

            rows = cur.fetchall()

            if cur.rowcount == 0:
                return None, [], None

            # created and date are going to be same for all.
            # So just use whatever the first row has.
            created = rows[0][4]
            date_to_reserve = rows[0][6]

            packages = []
            for package_id, quantity, price, meal_id, _, \
                delivery_location, __, cuisine_id, complimentary, add_on, extra_quantity in rows:
                delivery_charge = home_delivery_charge[meal_id] \
                    if delivery_location == HOME \
                    else office_delivery_charge[meal_id]

                packages.append(ReservedPackage(
                    package_id,
                    price,
                    quantity,
                    meal_id,
                    cuisine_id,
                    delivery_location,
                    delivery_charge,
                    complimentary,
                    add_on,
                    door_step_delivery=door_step_delivery and delivery_location == HOME,
                    status='ordered' if (extra_quantity is None or extra_quantity == 0) else 'delivered'
                ))

            return created, packages, date_to_reserve
        finally:
            if cur is not None:
                cur.close()

    def delete_deliverables(self, phone_number, date):
        """
        Deletes deliverables where status is ordered for customer
        on date returning list of deleted Deliverables(s)

        :param phone_number: phone number of customer
        :param date: date to use
        :return: list of deleted Deliverables(s)
        """
        if self.conn is None:
            self.conn = psycopg2.connect(config.DATABASE_URL)

        cur = None
        try:
            cur = self.conn.cursor()

            customer_id = util.database.get_customer_id(cur, phone_number)

            cur.execute('delete from deliverables '
                        'where customer_id = %(cust_id)s '
                        'and status = \'ordered\' '
                        'and date = %(date)s '
                        'returning package_id, quantity, '
                        'delivery_location, date, price, status', {
                            'cust_id': customer_id,
                            'date': date
                        })

            logger.debug(cur.query)

            deleted_deliverables = []
            for package_id, quantity, delivery_location, date, \
                price, status in cur.fetchall():
                deliverable = Deliverable(package_id, quantity, delivery_location,
                                          float(price), date, status)
                logger.debug(f'deleted deliverable: {deliverable}')
                deleted_deliverables.append(deliverable)

            logger.debug('deleting cancelled meals')
            cur.execute(
                'delete from deliverables '
                'where customer_id = %s '
                'and date = %s '
                'and status = \'cancelled\'',
                (customer_id, date)
            )
            logger.debug(cur.query)

            return deleted_deliverables

        finally:
            if cur is not None:
                cur.close()

    def set_credit(self, phone_number, credit):
        """
        set credit for customer with phone number to credit

        :param phone_number: phone number of customer
        :param credit: the credit to set for customer
        :return: nothing
        """
        if self.conn is None:
            self.conn = psycopg2.connect(config.DATABASE_URL)

        util.database.set_credit(self.conn, phone_number, credit)

    def insert_deliverables(self, phone_number, packages, date):
        """
        Add packages to deliverables for now.date()

        :param phone_number: phone number of customer
        :param packages: list of packages to add to deliverables
        :param date: date to use
        :return: nothing
        """
        if self.conn is None:
            self.conn = psycopg2.connect(config.DATABASE_URL)
        cur = None
        try:
            cur = self.conn.cursor()

            customer_id = util.database.get_customer_id(cur, phone_number)

            for p in packages:
                cur.execute(
                    'insert into deliverables(customer_id, package_id, quantity, '
                    'delivery_location, date, price, door_step_delivery, status) '
                    'values (%s, %s, %s, %s, %s, %s, %s, %s) '
                    'on conflict (customer_id, package_id, date) '
                    'do update set quantity = deliverables.quantity + %s, '
                    'price = deliverables.price + %s',
                    (customer_id, p.id, p.quantity, p.delivery_location,
                     date, p.price, p.door_step_delivery, p.status, p.quantity, p.price,)
                )

                logger.debug(cur.query)
        finally:
            if cur is not None:
                cur.close()

    def remove_reservation(self, reservation_id):
        """
        Delete rows in reservation with the given id

        :param reservation_id:
        :return:
        """
        if self.conn is None:
            self.conn = psycopg2.connect(config.DATABASE_URL)

        cur = None
        try:
            cur = self.conn.cursor()

            cur.execute('delete from reservations '
                        'where id = %(r_id)s',
                        {
                            'r_id': reservation_id
                        })

            logger.debug(cur.query)
        finally:
            if cur is not None:
                cur.close()

    def temp_credit(self, phone_number):
        """
        Fetch temp credit for customer with the given phone number.

        **Note:** Locks the rows in table for update

        :param phone_number:
        :return:
        """
        if self.conn is None:
            self.conn = psycopg2.connect(config.DATABASE_URL)

        return util.database.temp_credit(self.conn, phone_number)

    def commit(self):
        """
        If conn is not none, commit the connection
        :return: nothing
        """
        if self.conn is not None:
            self.conn.commit()

    def unlock(self):
        """
        If conn is not none close the connection
        :return: nothing
        """
        if self.conn is not None:
            self.conn.close()
