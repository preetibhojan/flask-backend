"""
Exceptions thrown by book_spot.use_case
"""


class InvalidTransactionID(Exception):
    """
    Either
    1. Transaction id does not exist or
    2. Transaction id is used
    """


class PaymentVerificationFailed(Exception):
    """
    Payment verification failed
    """