import hashlib
import hmac
import logging

from flask import Blueprint, request

from config import WHATSAPP_VERIFY_TOKEN, WHATSAPP_APP_SECRET

whatsapp = Blueprint('whatsapp', __name__)

logger = logging.getLogger(__name__)


@whatsapp.get('/webhook')
def verification_request():
    """
    for reference:
    https://www.facebookblueprint.com/uploads/resource_courses/targets/615983/original/index.html?_courseId=293817#/page/61f199096e646f63774d3c86
    :return:
    """
    print(f'headers: {request.headers}')
    print(f'args: {request.args}')

    assert request.args.get('hub.mode') == 'subscribe'
    assert request.args.get('hub.verify_token') == WHATSAPP_VERIFY_TOKEN

    return f'{request.args.get("hub.challenge")}'


@whatsapp.post('/webhook')
def event_notification():
    print(f'headers: {request.headers}')
    print(f'args: {request.args}')
    print(f'json: {request.json}')
    body = request.get_data()
    print(f'body: {body}')

    signature = request.headers.get('X-Hub-Signature-256')
    print(f'signature: {signature}')
    assert signature.startswith('sha256=')

    # sha256= is 7 letters. get the part after that
    signature = signature[7:]
    digest = hmac.new(WHATSAPP_APP_SECRET.encode('utf-8'), body, 'sha256').hexdigest()
    print(f'digest: {digest}')
    # https://docs.python.org/3/library/hmac.html#hmac.compare_digest
    assert hmac.compare_digest(digest, signature)
    return {}
