from email_validator import validate_email, EmailNotValidError

from util.exceptions import InvalidInput
from util.string_util import is_alphanumeric_underscore_or_space


# Default arguments are added to allow positional arguments


class Customer:
    first_name: str
    last_name: str
    email: str
    phone_number: str
    password: str

    def __init__(self, first_name, last_name, email, phone_number, password):

        if first_name is None or last_name is None or email is None or phone_number is None or password is None:
            raise InvalidInput

        self.first_name = first_name
        self.last_name = last_name
        self.email = ''
        try:
            self.email = validate_email(email.strip(), check_deliverability=False).email
            self.is_valid_email = True
        except EmailNotValidError:
            self.is_valid_email = False

        self.phone_number = phone_number
        self.password = password.strip()

    def is_first_name_valid(self):
        return self.first_name.isalpha()

    def is_last_name_valid(self):
        return self.last_name.isalpha()

    def is_email_valid(self):
        return self.is_valid_email

    def is_phone_number_valid(self):
        try:
            return len(self.phone_number) == 10 and int(self.phone_number) > 0
        except ValueError:
            # ValueError is thrown by int() when phone number is not a valid integer.
            # If that's the case, phone number is invalid
            return False

    def is_password_valid(self):
        return 8 <= len(self.password.strip()) <= 70

    def is_valid(self):

        return self.is_first_name_valid() and \
               self.is_last_name_valid() and \
               self.is_email_valid() and \
               self.is_phone_number_valid() and \
               self.is_password_valid()

    @staticmethod
    def from_json(json):
        return Customer(
            json['first_name'],
            json['last_name'],
            json['email'],
            json['phone_number'],
            json['password']
        )


"""
If you change Home address, remember to change change_home_address
"""


class HomeAddress:
    society: str
    building: str
    flat_number: str
    locality_id: int
    society_id: int
    door_step_delivery: bool = False

    def __init__(self, flat_number, building, society_id, door_step_delivery):
        if flat_number is None or building is None:
            raise InvalidInput

        self.society_id = society_id
        self.building = building.strip()
        self.flat_number = flat_number.strip()
        self.door_step_delivery = door_step_delivery

    def is_building_valid(self):
        return is_alphanumeric_underscore_or_space(self.building)

    def is_flat_number_valid(self):
        return is_alphanumeric_underscore_or_space(self.flat_number)

    def is_locality_valid(self):
        return self.locality_id >= 0

    def is_valid(self):
        return self.is_flat_number_valid() and \
               self.is_building_valid() and \
               self.is_society_id_valid()

    @staticmethod
    def from_json(json):
        return HomeAddress(
            json['flat_number'],
            json['building'],
            json['society_id'],
            json.get('door_step_delivery', False),
        )

    def is_society_id_valid(self):
        return self.society_id >= 0


"""
If you change office address, remember to change change_office_address
"""


class OfficeAddress:
    office_number: str
    floor: int
    tower: str
    locality_id: int
    company: str

    def __init__(self, office_number, floor, company, tower_id):
        if office_number is None or floor is None or company is None:
            raise InvalidInput

        self.company = company.strip()
        self.office_number = office_number.strip()
        self.floor = floor
        self.tower_id = tower_id

    def is_office_number_valid(self):
        return is_alphanumeric_underscore_or_space(self.office_number)

    def is_floor_valid(self):
        return self.floor >= 0

    def is_company_valid(self):
        return is_alphanumeric_underscore_or_space(self.company)

    def is_valid(self):
        return self.is_company_valid() and \
               self.is_office_number_valid() and \
               self.is_floor_valid() and \
               self.is_tower_id_valid()

    @staticmethod
    def from_json(json):
        return OfficeAddress(
            json['office_number'],
            json['floor'],
            json['company'],
            json['tower_id']
        )

    def is_tower_id_valid(self):
        return self.tower_id >= 0
