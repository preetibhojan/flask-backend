import logging
import uuid

import psycopg2

import config
from cuisines.repository import get_cuisines
from delivery_locations.repository import delivery_locations

logger = logging.getLogger(__name__)


class CustomerRepository:
    def __init__(self):
        self.delivery_locations = delivery_locations()

    def tower_exists(self, tower_id):
        # Society and towers both were merged into towers.
        # Towers with home = False are towers and others are societies
        return tower_id in map(lambda l: l.id, filter(lambda l: l.home is False, self.delivery_locations['towers']))

    def society_exists(self, tower_id):
        # Society and towers both were merged into towers.
        # Towers with home = True are towers and others are societies
        return tower_id in map(lambda l: l.id, filter(lambda l: l.home is True, self.delivery_locations['towers']))

    def cuisine_exists(self, cuisine_id):
        cuisine_ids = map(lambda cuisine: cuisine.id, get_cuisines())
        return cuisine_id in cuisine_ids

    def customer_exists(self, phone_number):
        conn = None
        cur = None
        try:
            # conn = psycopg2.connect(host=config.DATABASE_URL,
            #                         database=config.DATABASE,
            #                         user=config.REGISTER_USER,
            #                         password=config.REGISTER_PASSWORD
            #                         )
            conn = psycopg2.connect(config.DATABASE_URL)
            cur = conn.cursor()

            cur.execute('SELECT id from customer where phone_number = %(pho_no)s', {'pho_no': phone_number})
            logger.debug(cur.query)
            cur.fetchone()

            return cur.rowcount == 1
        finally:
            if conn is not None:
                conn.close()
            if cur is not None:
                cur.close()

    def insert(self, customer, home_address, office_address, cuisine_id, token):
        conn = None
        cur = None

        try:
            # conn = psycopg2.connect(host=config.DATABASE_URL,
            #                         database=config.DATABASE,
            #                         user=config.REGISTER_USER,
            #                         password=config.REGISTER_PASSWORD
            #                         )
            conn = psycopg2.connect(config.DATABASE_URL)
            cur = conn.cursor()

            customer_id = str(uuid.uuid4())
            cur.execute(
                'insert into customer(id, phone_number, password, '
                'first_name, last_name, email, cuisine_id, fcm_token) '
                'values (%s, %s, %s, %s, %s, %s, %s, %s)',
                (
                    customer_id, customer.phone_number, customer.password,
                    customer.first_name, customer.last_name, customer.email,
                    cuisine_id, token
                )
            )

            logger.debug(cur.query)
            if home_address is not None:
                cur.execute(
                    'insert into homeaddress(customer_id, flat_number, '
                    'building, tower_id, door_step_delivery) '
                    'values (%s, %s, %s, %s, %s)',
                    (customer_id, home_address.flat_number, home_address.building,
                     home_address.society_id, home_address.door_step_delivery)
                )

            logger.debug(cur.query)

            if office_address is not None:
                cur.execute(
                    'insert into officeaddress(customer_id, office_number, '
                    'floor, company, tower_id) '
                    'values (%s, %s, %s, %s, %s)',
                    (customer_id, office_address.office_number,
                     office_address.floor, office_address.company,
                     office_address.tower_id)
                )

            logger.debug(cur.query)

            conn.commit()
            return customer_id

        finally:
            if conn is not None:
                conn.close()
            if cur is not None:
                cur.close()

    def add_payment_details(self, customer_id):
        conn = None
        cur = None

        try:
            conn = psycopg2.connect(config.DATABASE_URL)
            cur = conn.cursor()

            cur.execute('insert into paymentdetail(customer_id) values (%s)', (customer_id,))

            logger.debug(cur.query)

            conn.commit()
        finally:
            if conn is not None:
                conn.close()
            if cur is not None:
                cur.close()
