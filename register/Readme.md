# Register

This module is used to register a new customer. The end point to register
a customer is `localhost:5000/api/v1/register`. Send a post request with
appropriate data in json format to the above url to register the customer.  

**Notes:**
1. Inputs allowing alphabets, number, _ and spaces are trimmed/stripped
2. Customers are uniquely identified using their phone number
3. Only one of home address or office address is required. But both
are also allowed.

# Sample Input

```json
{
    "customer": {
        "first_name": "FName",
        "last_name": "LName",
        "email": "abc@def.com",
        "phone_number": "0987654321",
        "password": "abcdefghi"
    },
    "home_address": {
        "flat_number": "104",
        "building": "def",
        "society": "ghi",
        "locality_id": 1
    },
    "office_address": {
        "office_number": "jkl",
        "floor": 1,
        "tower": "mno",
        "area_id": 1,
        "company": "abc"
    },
    "cuisine_id": 1
}
```

# Explanation

1. Customer - It consists of first name, last name, email, phone number
and password. First name and last name must only contain alphabets. 
Company may contain alphabets, numbers, _ and spaces. Password may
consist of any characters but has a minimum length of 8 characters 
and maximum length of 70. Email is any valid email

2. Home address - It consists of flat number, building, society and
locality_id. Flat number, building and society can contain alphabets, 
numbers, _ and spaces. Locality_id is an int that needs to refer to
a valid id returned by delivery_locations.

3. Office address - It consists of office number, floor, tower, area_id
and company. Office number, tower and company can contain alphabets, 
numbers, _ and spaces. Area_id must be a int referring to a valid id
returned by delivery_locations.

4. Cuisine id - The id of the cuisine the user wants.

# Errors

In case of an error with a request ex - a field missing or a required
field contains null, status code 400 (BAD) request is returned with
json response containing a field `error` with error code. ex-
```json
{
  "error": <error code>
}
```
 
Here is the list if errors:

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Area or locality does not exist |
|  2         | Customer already exists         |
|  3         | Cuisine does not exist          |

## Invalid input 
InvalidInput is thrown when -
1. Input contains null where it shouldn't
2. A required field is omitted
3. Customer, home address and/or office address are invalid
4. Request body does not contain json
5. You are trying to book a meal for a day it cannot be served on

## Area or locality does not exist
Pretty self explanatory. Area id of the office address and/or 
locality id of the home address does not exist

## Customer already exists
Again pretty self explanatory. A customer with this phone number
is already registered. 

## Cuisine does not exist
Again pretty self explanatory. Cuisine id does not exist