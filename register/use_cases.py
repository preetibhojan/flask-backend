import logging

from register.exceptions import *
from util.exceptions import InvalidInput
from util.password import hash_password


logger = logging.getLogger(__name__)


def register_customer(customer, home_address, office_address, cuisine_id, repository, token):
    if customer is None or \
            cuisine_id is None:
        logger.info('customer or cuisine id is null')
        raise InvalidInput('Customer or cuisine id is null')

    if home_address is None and office_address is None:
        logger.info('both home address and customer address are none')
        raise InvalidInput('Both office and home address are nome')

    if not customer.is_valid():
        logger.info('customer is invalid')
        raise InvalidInput('Customer is invalid')

    if home_address is not None:
        if not home_address.is_valid():
            logger.info('home address is invalid')
            raise InvalidInput('Home address is invalid')

        if not repository.society_exists(home_address.society_id):
            logger.info('society of home address does not exist')
            raise AreaOrLocalityDoesNotExist

    if office_address is not None:
        if not office_address.is_valid():
            logger.info('office address is not valid')
            raise InvalidInput('Office address is invalid')

        if not repository.tower_exists(office_address.tower_id):
            logger.info('tower of office address does not exist')
            raise AreaOrLocalityDoesNotExist

    if repository.customer_exists(customer.phone_number):
        logger.info('customer already exists')
        raise CustomerAlreadyExists

    if not repository.cuisine_exists(cuisine_id):
        logger.info('cuisine does not exists')
        raise CuisineDoesNotExist

    # hash the password and store it in customer.password field again to store in database
    customer.password = hash_password(customer.password)
    customer_id = repository.insert(customer, home_address, office_address, cuisine_id, token)
    repository.add_payment_details(customer_id)
    return True
