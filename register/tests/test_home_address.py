import unittest

from register.models import HomeAddress, InvalidInput
from register.tests.input import alphaNumericWithSpacesAndUnderscore


class JsonTest(unittest.TestCase):
    def test_from_json(self):
        home_address = HomeAddress.from_json({
            'flat_number': 'abc',
            'building': 'def',
            'society_id': 2,
        })

        self.assertEqual(home_address.flat_number, 'abc')
        self.assertEqual(home_address.building, 'def')
        self.assertEqual(home_address.society_id, 2)


class NoneTest(unittest.TestCase):
    def test_constructor_throws_invalid_input_when_flat_number_is_none_and_society_id_is_not_none(self):
        with self.assertRaises(InvalidInput):
            HomeAddress(None, '', 1, False)

    def test_constructor_throws_invalid_input_when_building_is_none_and_society_id_is_not_none(self):
        with self.assertRaises(InvalidInput):
            HomeAddress('', None, 1, False)


class IndividualFieldsTest(unittest.TestCase):
    def test_building_should_contain_alphabets_numbers_underscore_and_space(self):
        for building, isValid in alphaNumericWithSpacesAndUnderscore.items():
            home_address = HomeAddress('', building, 1, False)
            self.assertEqual(home_address.is_building_valid(), isValid)

    def test_flat_number_should_contain_alphabets_numbers_underscore_and_space(self):
        for flat_number, isValid in alphaNumericWithSpacesAndUnderscore.items():
            home_address = HomeAddress(flat_number, '', 1, False)
            self.assertEqual(home_address.is_flat_number_valid(), isValid)

    def test_society_id_is_not_negative(self):
        home_address = HomeAddress('', '', -1, False)
        self.assertFalse(home_address.is_society_id_valid())

        home_address.society_id = 0
        self.assertTrue(home_address.is_society_id_valid())

        home_address.society_id = 1
        self.assertTrue(home_address.is_society_id_valid())


# validation tests with society id.
class NewValidationTest(unittest.TestCase):
    def test_is_valid_returns_false_when_flat_number_is_invalid(self):
        home_address = HomeAddress('', 'abc', society_id=0, door_step_delivery=False)
        self.assertFalse(home_address.is_valid())

    def test_is_valid_returns_false_when_building_is_invalid(self):
        home_address = HomeAddress('abc', '', society_id=0, door_step_delivery=False)
        self.assertFalse(home_address.is_valid())

    def test_is_valid_returns_true_when_everything_is_valid(self):
        home_address = HomeAddress('abc', 'def', society_id=0, door_step_delivery=False)
        self.assertTrue(home_address.is_valid())


if __name__ == '__main__':
    unittest.main()
