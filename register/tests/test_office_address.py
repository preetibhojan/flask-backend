import unittest

from register.models import OfficeAddress, InvalidInput
from register.tests.input import alphaNumericWithSpacesAndUnderscore


class JsonTest(unittest.TestCase):
    def test_from_json_with_tower_id(self):
        office_address = OfficeAddress.from_json(
            {
                'office_number': '123',
                'floor': 1,
                'company': 'def',
                'tower_id': 2
            }
        )

        self.assertEqual(office_address.company, 'def')
        self.assertEqual(office_address.office_number, '123')
        self.assertEqual(office_address.floor, 1)
        self.assertEqual(office_address.tower_id, 2)


class NoneTest(unittest.TestCase):
    def test_constructor_throws_invalid_input_when_office_number_is_none(self):
        with self.assertRaises(InvalidInput):
            OfficeAddress(None, 0, '', 0)

    def test_constructor_throws_invalid_input_when_floor_is_none(self):
        with self.assertRaises(InvalidInput):
            OfficeAddress('', None, '', 0)

    def test_constructor_throws_invalid_input_when_company_is_none(self):
        with self.assertRaises(InvalidInput):
            OfficeAddress('', 0, None, 0)


class IndividualFieldsTest(unittest.TestCase):
    def test_office_number_should_contain_alphabets_numbers_underscore_and_space(self):
        for office_number, isValid in alphaNumericWithSpacesAndUnderscore.items():
            office_address = OfficeAddress(office_number, 0, '', 0)
            self.assertEqual(office_address.is_office_number_valid(), isValid)

    def test_floor_be_a_number_greater_than_0(self):
        office_address = OfficeAddress('', -13, '', 0)
        self.assertFalse(office_address.is_floor_valid())

        office_address.floor = 12
        self.assertTrue(office_address.is_floor_valid())

    def test_company_should_contain_alphabets_numbers_underscore_and_space(self):
        for company, isValid in alphaNumericWithSpacesAndUnderscore.items():
            office_address = OfficeAddress('', 0, company, 0)
            self.assertEqual(office_address.is_company_valid(), isValid)

    def test_tower_id_should_not_be_negative(self):
        office_address = OfficeAddress('', 0, '', tower_id=-1)
        self.assertFalse(office_address.is_tower_id_valid())

        office_address.tower_id = 0
        self.assertTrue(office_address.is_tower_id_valid())

        office_address.tower_id = 1
        self.assertTrue(office_address.is_tower_id_valid())


# validation tests with society id.
class NewValidationTest(unittest.TestCase):
    def test_is_valid_should_be_false_when_office_number_is_invalid(self):
        office_address = OfficeAddress('', 12, 'efhkg', 0)
        self.assertFalse(office_address.is_valid())

    def test_is_valid_should_be_false_when_floor_is_invalid(self):
        office_address = OfficeAddress('abc', -12, 'wifh', 0)
        self.assertFalse(office_address.is_valid())

    def test_is_valid_should_be_false_when_company_is_invalid(self):
        office_address = OfficeAddress('abc', 12, '', 0)
        self.assertFalse(office_address.is_valid())

    def test_is_valid_should_be_false_when_tower_id_is_invalid(self):
        office_address = OfficeAddress('abc', 12, 'weufy', -1)
        self.assertFalse(office_address.is_valid())


if __name__ == '__main__':
    unittest.main()
