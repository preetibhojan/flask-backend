import unittest
import uuid
from unittest.mock import Mock

from register.exceptions import *
from register.models import Customer, HomeAddress, OfficeAddress
from register.use_cases import register_customer, CustomerAlreadyExists
from util.exceptions import InvalidInput
from util.password import is_correct


# Done to avoid warning. 6-7 lines duplicated is not big deal in a test
# noinspection DuplicatedCode
class NeedBasedCustomerRegistration(unittest.TestCase):
    def test_register_raises_invalid_input_when_customer_is_none(self):
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        with self.assertRaises(InvalidInput):
            register_customer(None, home_address, office_address, 1, None, 'abc')

    def test_register_raises_invalid_input_when_home_and_office_address_is_none(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')

        with self.assertRaises(InvalidInput):
            register_customer(customer, None, None, 1, None, 'abc')

    def test_register_raises_invalid_input_when_cuisine_id_is_none(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        with self.assertRaises(InvalidInput):
            register_customer(customer, home_address, office_address, None, None, 'abc')

    def test_register_raises_invalid_input_when_customer_is_invalid(self):
        customer = Customer('', '', '', '', '')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 0)

        with self.assertRaises(InvalidInput):
            register_customer(customer, home_address, office_address, 1, None, 'abc')

    def test_register_raises_invalid_input_when_home_address_is_invalid(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('', '', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 0)

        with self.assertRaises(InvalidInput):
            register_customer(customer, home_address, office_address, 1, None, 'abc')

    def test_register_raises_invalid_input_when_office_address_is_invalid(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('', '', '', 0)

        repository = Mock()
        repository.society_exists.return_value = True

        with self.assertRaises(InvalidInput):
            register_customer(customer, home_address, office_address, 1, repository, 'abc')

    def test_register_raises_area_or_locality_does_not_exist_when_home_address_society_id_does_not_exist(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        repository = Mock()
        repository.tower_exists.return_value = True
        repository.society_exists.return_value = False

        with self.assertRaises(AreaOrLocalityDoesNotExist):
            register_customer(customer, home_address, office_address, 1, repository, 'abc')

        repository.society_exists.assert_called_once_with(0)

    def test_raises_area_or_locality_does_not_exist_when_office_address_tower_id_does_not_exist(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 0, 'abc', 1)

        repository = Mock()
        repository.society_exists.return_value = True
        repository.tower_exists.return_value = False

        with self.assertRaises(AreaOrLocalityDoesNotExist):
            register_customer(customer, home_address, office_address, 1, repository, 'abc')

        repository.tower_exists.assert_called_once_with(1)
        repository.society_exists.assert_called_once_with(0)

    def test_register_raises_customer_already_exists_when_customer_exists(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        repository = Mock()

        repository.tower_exists.return_value = True
        repository.society_exists.return_value = True
        repository.customer_exists.return_value = True

        with self.assertRaises(CustomerAlreadyExists):
            register_customer(customer, home_address, office_address, 1, repository, 'abc')

        repository.tower_exists.assert_called_once_with(1)
        repository.society_exists.assert_called_once_with(0)

        repository.customer_exists.assert_called_once_with('1234567890')

    def test_register_raises_cuisine_does_not_exist_when_cuisine_does_not_exist(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        repository = Mock()

        repository.tower_exists.return_value = True
        repository.society_exists.return_value = True
        repository.customer_exists.return_value = False
        repository.cuisine_exists.return_value = False

        with self.assertRaises(CuisineDoesNotExist):
            register_customer(customer, home_address, office_address, 1, repository, 'abc')

        repository.tower_exists.assert_called_once_with(1)
        repository.society_exists.assert_called_once_with(0)

        repository.customer_exists.assert_called_once_with('1234567890')
        repository.cuisine_exists.assert_called_once_with(1)

    def test_success(self):
        pho_no = '1234567890'
        customer = Customer('abc', 'abc', 'abc@def.com', pho_no, '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        repository = Mock()

        repository.tower_exists.return_value = True
        repository.society_exists.return_value = True
        repository.customer_exists.return_value = False
        repository.cuisine_exists.return_value = True

        customer_id = uuid.uuid4()
        repository.insert.return_value = customer_id

        cuisine_id = 1

        token = 'abc'
        register_customer(customer, home_address, office_address, cuisine_id, repository, token)

        repository.tower_exists.assert_called_once_with(1)
        repository.society_exists.assert_called_once_with(0)

        repository.customer_exists.assert_called_once_with(pho_no)
        repository.cuisine_exists.assert_called_once_with(1)
        repository.insert.assert_called_once_with(customer, home_address, office_address, cuisine_id, token)
        repository.add_payment_details.assert_called_once_with(customer_id)

        # make sure password is hashed
        self.assertTrue(is_correct('0987654321', customer.password))


class RegisterWithHomeAddressOnly(unittest.TestCase):
    def test_register_raises_invalid_input_when_home_address_is_invalid(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('', '', 0, False)

        with self.assertRaises(InvalidInput):
            register_customer(customer, home_address, None, 1, None, 'abc')

    def test_register_raises_area_or_locality_does_not_exist_when_home_address_society_id_does_not_exist(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)

        repository = Mock()
        repository.society_exists.return_value = False

        with self.assertRaises(AreaOrLocalityDoesNotExist):
            register_customer(customer, home_address, None, 1, repository, 'abc')

        repository.society_exists.assert_called_once_with(0)

    def test_success(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        home_address = HomeAddress('abc', 'abc', 0, False)

        repository = Mock()

        repository.society_exists.return_value = True
        repository.customer_exists.return_value = False
        repository.cuisine_exists.return_value = True

        cuisine_id = 1

        token = 'abc'
        register_customer(customer, home_address, None, cuisine_id, repository, token)

        repository.society_exists.assert_called_once_with(0)

        repository.customer_exists.assert_called_once_with('1234567890')
        repository.cuisine_exists.assert_called_once_with(1)
        repository.insert.assert_called_once_with(customer, home_address, None, cuisine_id, token)

        # make sure password is hashed
        self.assertTrue(is_correct('0987654321', customer.password))


class RegisterWithOfficeAddressOnly(unittest.TestCase):
    def test_register_raises_invalid_input_when_office_address_is_invalid(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        office_address = OfficeAddress('', '', '', 1)

        with self.assertRaises(InvalidInput):
            register_customer(customer, None, office_address, 1, None, 'abc')

    def test_register_raises_area_or_locality_does_not_exist_when_office_area_does_not_exist(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        repository = Mock()
        repository.tower_exists.return_value = False

        with self.assertRaises(AreaOrLocalityDoesNotExist):
            register_customer(customer, None, office_address, 1, repository, 'abc')

        repository.tower_exists.assert_called_once_with(1)

    def test_register_raises_area_or_locality_does_not_exist_when_office_address_tower_id_does_not_exist(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        office_address = OfficeAddress('abc', 0, 'abc', 0)

        repository = Mock()
        repository.tower_exists.return_value = False

        with self.assertRaises(AreaOrLocalityDoesNotExist):
            register_customer(customer, None, office_address, 1, repository, 'abc')

        repository.tower_exists.assert_called_once_with(0)

    def test_success(self):
        customer = Customer('abc', 'abc', 'abc@def.com', '1234567890', '0987654321')
        office_address = OfficeAddress('abc', 1, 'abc', 1)

        repository = Mock()

        repository.customer_exists.return_value = False
        repository.cuisine_exists.return_value = True
        repository.tower_exists.return_value = True

        cuisine_id = 1

        token = 'abc'
        register_customer(customer, None, office_address, cuisine_id, repository, token)

        repository.tower_exists.assert_called_once_with(1)
        repository.customer_exists.assert_called_once_with('1234567890')
        repository.cuisine_exists.assert_called_once_with(1)
        repository.insert.assert_called_once_with(customer, None, office_address, cuisine_id, token)

        # make sure password is hashed
        self.assertTrue(is_correct('0987654321', customer.password))


if __name__ == '__main__':
    unittest.main()
