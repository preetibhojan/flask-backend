class AreaOrLocalityDoesNotExist(Exception):
    """
    Pretty self explanatory. Thrown when area of locality in a request is invalid
    """
    pass


class CustomerAlreadyExists(Exception):
    pass


class InvalidPackage(Exception):
    """
    Thrown when -
    1. At least one of the package is invalid
    2. Two or more packages have same id
    3. Package does not exist
    4. Two or more packages have same meal id
    """
    pass


class CuisineDoesNotExist(Exception):
    """
    Self explanatory. Thrown when cuisine does not exist
    """
    pass
