# Changelog

Brief description about the notable changes in the project. 

## Unstable

### Added

- An extra module - password_util for password related utilities
so that passwords are treated in the same way in all modules.

- Gitlab CI file to automate testing. Currently only register 
module has tests so only that is tested. Other modules except
(to be added) order are too trivial to test. They consists of
a select statement and storing the data in objects and returning
that

### Changes
- Before, the server performed a bunch of joins to return the data
as a map. But due to overwhelming complexity of some things, I 
decided to return the foreign keys in the output instead of joining
myself.
This series of change began with f0c954e4 

-  The register module needed a few tweaks in its input to account for
change in output of other modules due to the above change 

