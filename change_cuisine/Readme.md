# Change cuisine

Change cuisine for customer. Takes in phone number and password
for authentication. Other than take takes the cuisine_id

**Note:** Changing cuisine deletes schedule because the schedule
is no longer valid as it references packages from old schedule. 
.If cuisine id is same as what the database already had, nothing
is deleted 

# Sample input
```json
{
    "phone_number": "1234567890",
	"password": "abcdefghi",
    "cuisine_id": 1
}
```

# Error codes
It returns a 400 Bad request in case of an error with and error code.

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication error            |
|  2         | Cuisine ID does not exist       |

## Invalid input
1. Request is not json
2. A required parameter is missing or null

## Authentication Error
Phone number password combo does not match or customer does not exist

## Cuisine id does not exist
Self explanatory