import logging

import psycopg2

import config
from change_cuisine.exceptions import CuisineIDDoesNotExist
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger()


def change_cuisine_use_case(phone_number, password, cuisine_id, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication error')
        raise AuthenticationError

    if cuisine_id is None or cuisine_id < 0:
        raise InvalidInput

    conn = None
    cur = None

    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('select id from cuisines where id = %(id)s',
                    {
                        'id': cuisine_id
                    })

        logger.debug(cur.query)

        if cur.rowcount == 0:
            raise CuisineIDDoesNotExist

        cur.execute('select id, cuisine_id from customer where phone_number = %(pho_no)s',
                    {
                        'pho_no': phone_number
                    })

        logger.debug(cur.query)

        row = cur.fetchone()
        customer_id = row[0]
        old_cuisine_id = row[1]

        if old_cuisine_id != cuisine_id:
            cur.execute('update customer set cuisine_id = %(cuisine_id)s where id = %(cust_id)s',
                        {
                            'cust_id': customer_id,
                            'cuisine_id': cuisine_id
                        })

            logger.debug(cur.query)

            cur.execute('delete from tiffininformation where customer_id = %(cust_id)s',
                        {
                            'cust_id': customer_id
                        })

            logger.debug(cur.query)

            cur.execute('delete from deliverables where customer_id = %(cust_id)s and date > current_date',
                        {
                            'cust_id': customer_id
                        })

            logger.debug(cur.query)

            conn.commit()

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()
