# Payment Info

Returns the details like schedule, credit_available, temp_credit, deposit
and optionally till date if it not null. 

Temp credit is credit deposited due to cancellation of meals. The reason
this is kept separate is if the user cancelled a package worth 20 bucks,
and they buy something worth 40 bucks, they should pay 20 bucks even
if they have credit available. This is done in order to not interfere
with their schedule.     

# Sample input
```json
{
  "phone_number": "0987654321",
  "password": "abcdefghi"
}
```

# Sample output
```json
{
  "credit_available": "35.00",
  "deposit": "250.00",
  "package_info": [
    {
      "delivery_info": [
        {
          "day": 6,
          "delivery_location": 1
        },
        {
          "day": 5,
          "delivery_location": 1
        },
        {
          "day": 4,
          "delivery_location": 1
        }        
      ],
      "meal_id": 2,
      "package_id": 21,
      "price": 65
    },
    {
      "delivery_info": [
        {
          "day": 3,
          "delivery_location": 1
        },
        {
          "day": 2,
          "delivery_location": 1
        },
        {
          "day": 1,
          "delivery_location": 1
        }
      ],
      "meal_id": 3,
      "package_id": 24,
      "price": 65
    }
  ],
  "till_date": "2020-08-30",
  "temp_credit": "0.00"
}
```