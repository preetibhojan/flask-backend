"""
Use case for getting the payment info for a customer. In addition to the credit available and
till date payment info also returns the schedule of the customer
"""
import logging

import psycopg2

import config
import util.database
from payment_info.models import DeliveryInfo, PackagePaymentInfo
from util.constants import HOME
from util.database import get_customer_id
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


def payment_info_use_case(phone_number, password, auth_func):
    """
    Returns the payment information for a given customer
    :param phone_number: phone number of customer
    :param password: password of customer
    :param auth_func: A function that throws Exception on authentication failure
    :return: a list of package payment info
    """
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication Error')
        raise AuthenticationError

    return get_payment_info(phone_number, 'temp_tiffininformation')


def booked_payment_info(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication Error')
        raise AuthenticationError

    return get_payment_info(phone_number, 'tiffininformation')


def get_payment_info(phone_number, table):
    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        # Customer is authenticated. So they exists for sure
        customer_id = get_customer_id(cur, phone_number)

        cur.execute('select temp_credit, deposit from PaymentDetail where customer_id = %(cust_id)s',
                    {
                        'cust_id': customer_id
                    })

        logger.debug(cur.query)

        result = {}

        row = cur.fetchone()
        result['temp_credit'] = str(row[0])
        result['deposit'] = str(row[1])

        home_delivery_charge, office_delivery_charge = util.database.delivery_charge_for_v2(cur, phone_number)

        cur.execute(
            'select package_id, quantity, day, delivery_location, '
            'p.meal_id, p.price, p.cuisine_id, p.add_on '
            f'from {table} ti '
            'inner join Packages p '
            'on p.id = package_id '
            'where customer_id = %s',
            (customer_id,)
        )

        logger.debug(cur.query)

        rows = cur.fetchall()

        package_info = []
        add_on_info = []

        for package_id, quantity, day, delivery_location, meal_id, price, cuisine_id, add_on in rows:

            delivery_charge = home_delivery_charge[meal_id] if delivery_location == HOME \
                else office_delivery_charge[meal_id]

            # convenience function as we are doing the same operation on two lists
            # check if the package exists in the list. If so, append the delivery
            # info. Else append the package itself.
            #
            # We want to do this for both, add ons and packages. Hence, this
            # convenience function.
            def add_to_list_or_append_delivery_info(_list):
                package = next(filter(lambda p: p.package_id == package_id, _list), None)
                if not package:
                    package = PackagePaymentInfo(
                        package_id, meal_id,
                        [DeliveryInfo(day, delivery_location, delivery_charge)], price, quantity)
                    _list.append(package)
                else:
                    package.delivery_info.append(DeliveryInfo(day, delivery_location, delivery_charge))

            if add_on is True:
                add_to_list_or_append_delivery_info(add_on_info)
            else:
                add_to_list_or_append_delivery_info(package_info)

        result['package_info'] = package_info
        result['add_on_info'] = add_on_info

        return result
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()
