class DeliveryInfo:
    """
    Represents delivery information for one package. It has a day which is an int representing
    an iso weekday and delivery location which can be HOME or OFFICE
    """

    def __init__(self, day, delivery_location, delivery_charge=None):
        self.day = day
        self.delivery_location = delivery_location
        self.delivery_charge = delivery_charge

    def serialize(self):
        """
        Returns a dictionary representing this object. Used to convert DeliveryInfo to Json
        :return: a dictionary representing DeliveryInfo
        """
        return {
            'day': self.day,
            'delivery_location': self.delivery_location,
            'delivery_charge': self.delivery_charge,
        }

    def is_valid(self):
        """
        delivery info is valid if -
        1. day is greater than or equal to 1 and less than equal to 6
        2. delivery_location is 0 or 1
        :return:
        """
        if self.day < 1 or self.day > 7:
            return False

        if self.delivery_location != 0 and self.delivery_location != 1:
            return False

        return True

    @staticmethod
    def from_json(json):
        return DeliveryInfo(
            json['day'],
            json['delivery_location'],
            json.get('delivery_charge', None)
        )

    def __eq__(self, other):
        return isinstance(other, DeliveryInfo) \
               and self.day == other.day \
               and self.delivery_location == other.delivery_location \
               and self.delivery_charge == other.delivery_charge

    def __repr__(self):
        return f'day: {self.day}, delivery location: {self.delivery_location}, delivery charge {self.delivery_charge}'


class PackagePaymentInfo:
    """
    Package along with its payment information. It contains package id, meal id, price
    and a list of delivery info for all the days scheduled.
    """

    def __init__(self, package_id, meal_id, delivery_info=None, price=0, quantity=1):
        self.package_id = package_id
        self.meal_id = meal_id
        if delivery_info is None:
            delivery_info = []
        self.delivery_info = delivery_info
        self.price = price
        self.quantity = quantity

    def serialize(self):
        """
        Returns a dictionary representing this object. Used to convert PackagePaymentInfo to Json
        :return: a dictionary representing PackagePaymentInfo
        """
        return {
            'price': self.price,
            'delivery_info': self.delivery_info,
            'package_id': self.package_id,
            'meal_id': self.meal_id,
            'quantity': self.quantity,
        }
