import logging
import os
from email.message import EmailMessage
from email.mime.text import MIMEText
from smtplib import SMTP_SSL, SMTPException, SMTP
from threading import Thread

from dotenv import load_dotenv

from config import EMAIL_ID, EMAIL_PASSWORD

load_dotenv()


logger = logging.getLogger(__name__)


def send_email_sync(to, subject, body, html=False):
    try:
        with SMTP_SSL('smtpout.secureserver.net') as smtp:
            smtp.ehlo()
            smtp.login(EMAIL_ID, EMAIL_PASSWORD)

            msg = MIMEText(body, 'plain' if not html else 'html')
            msg['Subject'] = subject
            msg['From'] = EMAIL_ID
            msg['To'] = to

            smtp.send_message(msg)
            logger.info('mail sent')
    except SMTPException as e:
        logger.critical(e)


def send_email_async(to, subject, body, html=False):
    thread = Thread(target=send_email_sync, args=[to, subject, body, html])
    thread.start()
