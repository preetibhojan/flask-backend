"""
Routes for admin related things
"""
import logging
from datetime import datetime, date
from enum import Enum

import psycopg2.errors
from flask import Blueprint, request, jsonify

import add_quantity.exceptions
import authenticate.repository
import config
import util.exceptions
from add_order.repository import CustomerNotFound, PackageNotFound, DeliveryLocationNotFound, add_order
from add_quantity.model import PackageToSet
from add_quantity.use_case import add_quantity_use_case
from authenticate.use_case import authenticate_use_case
from cancel_order.repository import cancel_order
from convert.repository import package_conversions, convert_use_case
from customers.repository import customers
from delivery_locations.repository import localities_for
from front_end_config.repository import front_end_config
from menu.repository import menu_for_admin
from notifications.notifications_sent import notifications_sent, notifications_sent2
from notifications.send_notifications import send_notification, send_notification2
from packages.repository import get_packages_for_menu_for_admin
from report.customer_wise_packages import customer_wise_packages_report, \
    customer_wise_packages_report_for_delivery_executive
from report.summary import summary_use_case, summary_use_case2
from set_menu.model import MenuItem
from set_menu.repository import SetMenuRepository
from set_menu.use_case import set_menu_use_case
from update_payment_detail.repository import update_payment_detail
from util.datetime_util import time_to_seconds
from util.flask import respond_with_error

admin = Blueprint('admin', __name__)


def staff_auth_func(phone_number, password):
    return authenticate_use_case(phone_number, password, authenticate.repository.password_for_staff)


class AdminErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1
    UNAUTHORIZED_ACCESS = 3
    NOT_ALL_MEALS_CAN_BE_ORDERED_RIGHT_NOW = 4
    DUPLICATES_FOUND = 5
    CUSTOMER_NOT_FOUND = 6
    PACKAGE_NOT_FOUND = 7
    DELIVERY_LOCATION_NOT_FOUND = 8


logger = logging.getLogger(__name__)


@admin.route('/add_quantity', methods=['POST'])
def add_quantity_route():
    try:
        if not request.is_json:
            logger.info('request is not json')
            raise util.exceptions.InvalidInput

        temp = request.json['packages_to_set']

        packages_to_set = []
        for json in temp:
            packages_to_set.append(PackageToSet.from_json(json))

        config = front_end_config()

        add_quantity_use_case(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            packages_to_set,
            date.fromisoformat(request.json['date']),
            config['GST_percent'],
            datetime.now(),
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput, psycopg2.errors.ForeignKeyViolation):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)
    except add_quantity.exceptions.NotAllMealsCanBeOrderedNow:
        return respond_with_error(AdminErrorCode.NOT_ALL_MEALS_CAN_BE_ORDERED_RIGHT_NOW)
    except add_quantity.exceptions.DuplicatesFound as d:
        return {'error': AdminErrorCode.DUPLICATES_FOUND.value, 'duplicates': d.duplicates}, 400


@admin.route('/report/summary', methods=['POST'])
def summary_route():
    try:
        if not request.is_json:
            logger.info('request is not json')
            raise util.exceptions.InvalidInput

        meal_id = request.json['meal_id']
        _date = date.fromisoformat(request.json['date'])

        roti, sabji1, sabji2, sabji_s, sabji_p, \
        daal, daal_s, rice, pulav, pulav_s, \
        sweet, farsan, packages = summary_use_case(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            meal_id,
            _date
        )

        return {
            'sabji1': sabji1,
            'sabji2': sabji2,
            'sabji_s': sabji_s,
            'sabji_p': sabji_p,
            'daal': daal,
            'daal_s': daal_s,
            'rice': rice,
            'pulav': pulav,
            'pulav_s': pulav_s,
            'sweet': sweet,
            'farsan': farsan,
            'roti': roti,
            'packages': packages
        }

    # value error is raised by fromisoformat if date is incorrect
    except (ValueError, KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/report/summary2', methods=['POST'])
def summary_route2():
    try:
        if not request.is_json:
            logger.info('request is not json')
            raise util.exceptions.InvalidInput

        meal_id = request.json['meal_id']
        _date = date.fromisoformat(request.json['date'])

        return summary_use_case2(
                request.json['phone_number'],
                request.json['password'],
                staff_auth_func,
                meal_id,
                _date
            )

    # value error is raised by fromisoformat if date is incorrect
    except (ValueError, KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/report/customer_wise_packages', methods=['POST'])
def customer_wise_packages_route():
    try:
        if not request.is_json:
            logger.info('Request is not json')
            raise util.exceptions.InvalidInput

        meal_id = request.json['meal_id']
        _date = date.fromisoformat(request.json['date'])

        report = customer_wise_packages_report(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            meal_id,
            _date,
        )

        return jsonify(report)

    # value error is raised by fromisoformat if date is incorrect
    except (ValueError, KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/report/customer_wise_packages_for_delivery_executive', methods=['POST'])
def customer_wise_packages_for_delivery_executives_route():
    try:
        if not request.is_json:
            logger.info('Request is not json')
            raise util.exceptions.InvalidInput

        meal_id = request.json['meal_id']
        _date = date.fromisoformat(request.json['date'])

        report = customer_wise_packages_report_for_delivery_executive(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            meal_id,
            _date,
        )

        return jsonify(report)

    # value error is raised by fromisoformat if date is incorrect
    except (ValueError, KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/set_menu', methods=['POST'])
def set_menu_route():
    try:
        if not request.is_json:
            logger.info('Request is not json')
            raise util.exceptions.InvalidInput

        temp = request.json['menu']

        menu_items = []
        for json in temp:
            menu_items.append(MenuItem.from_json(json))

        set_menu_use_case(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            menu_items,
            date.today(),
            SetMenuRepository(),
        )

        return {}

    # value error is raised by fromisoformat if date is incorrect
    except (ValueError, KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/packages_for_menu', methods=['POST'])
def admin_packages_for_menu():
    try:
        if not request.is_json:
            logger.info('Request is not json')
            raise util.exceptions.InvalidInput

        packages = get_packages_for_menu_for_admin(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            datetime.now(),
        )

        return jsonify(packages)
    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/menu')
def menu_from_tomorrow_route():
    return jsonify(menu_for_admin(date.today()))


@admin.route('/localities_for', methods=['POST'])
def localities_for_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        localities = localities_for(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
        )

        return jsonify(localities)

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)


@admin.route('/notify', methods=['POST'])
def notify_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        send_notification(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            request.json['customers'],
            request.json.get('title', None),
            request.json['body'],
            request.json.get('highPriority', False)
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/notify2', methods=['POST'])
def notify2_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        send_notification2(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            request.json['customers'],
            request.json['meal_id'],
            request.json.get('title', None),
            request.json['body'],
            request.json['add_to_notification_log'],
            date.today(),
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/notifications_sent', methods=['POST'])
def notifications_sent_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        return {
            'notifications_sent': notifications_sent(
                request.json['phone_number'],
                request.json['password'],
                staff_auth_func,
                request.json['meal_id'],
                date.fromisoformat(request.json['date'])
            )
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/notifications_sent2', methods=['POST'])
def notifications_sent2_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        return {
            'notifications_sent': notifications_sent2(
                request.json['phone_number'],
                request.json['password'],
                staff_auth_func,
                request.json['meal_id'],
                date.fromisoformat(request.json['date'])
            )
        }

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/conversions')
def conversions_route():
    return {
        'conversions': list(package_conversions().values())
    }


@admin.route('/convert', methods=['POST'])
def convert_route():
    try:
        if not request.is_json:
            raise util.exceptions.InvalidInput

        config = front_end_config()
        gst = config['GST_percent']

        package_conversion = package_conversions()[request.json['conversion']]

        convert_use_case(
            request.json['phone_number'],
            request.json['password'],
            staff_auth_func,
            package_conversion,
            request.json['meal_id'],
            request.json['cuisine_id'],
            gst,
            date.fromisoformat(request.json['date']),
            datetime.now(),
        )

        return {}

    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except util.exceptions.UnauthorizedAccess:
        return respond_with_error(AdminErrorCode.UNAUTHORIZED_ACCESS)


@admin.route('/report_time')
def delivery_time_route():
    with psycopg2.connect(config.DATABASE_URL) as conn:
        with conn.cursor() as cur:
            cur.execute(
                'select meal_id, till from report_time'
            )

            result = []
            for meal_id, till in cur.fetchall():
                result.append({
                    'meal_id': meal_id,
                    'till': time_to_seconds(till),
                })

            return jsonify(result)


@admin.route('/add_order', methods=['POST'])
def add_order_route():
    try:
        if not request.json:
            raise util.exceptions.InvalidInput

        add_order(
            staff_auth_func,
            request.json['phone_number'],
            request.json['password'],
            request.json['customer_id'],
            request.json['package_id'],
            date.fromisoformat(request.json['date']),
            request.json['delivery_location'],
            request.json['quantity'],
            request.json['price'],
            request.json['debit'],
            request.json['door_step_delivery'],
        )
        return {}
    except (KeyError, ValueError, util.exceptions.InvalidInput) as e:
        print(e)
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
    except CustomerNotFound:
        return respond_with_error(AdminErrorCode.CUSTOMER_NOT_FOUND)
    except PackageNotFound:
        return respond_with_error(AdminErrorCode.PACKAGE_NOT_FOUND)
    except DeliveryLocationNotFound:
        return respond_with_error(AdminErrorCode.DELIVERY_LOCATION_NOT_FOUND)


@admin.route('/cancel_order', methods=['POST'])
def cancel_order_route():
    try:
        if not request.json:
            raise util.exceptions.InvalidInput

        cancel_order(
            staff_auth_func,
            request.json['phone_number'],
            request.json['password'],
            request.json['customer_id'],
            request.json['package_id'],
            date.fromisoformat(request.json['date']),
            request.json['quantity'],
            request.json['price'],
            request.json['credit']
        )
        return {}
    except (KeyError, ValueError, util.exceptions.InvalidInput) as e:
        print(e)
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)


@admin.route('/get_customers', methods=['POST'])
def customers_route():
    try:
        if not request.json:
            raise util.exceptions.InvalidInput

        result = customers(staff_auth_func, request.json['phone_number'], request.json['password'])
        return {'customers': result}
    except (KeyError, util.exceptions.InvalidInput):
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)


@admin.route('/update_payment_detail', methods=['POST'])
def update_payment_detail_route():
    try:
        if not request.json:
            raise util.exceptions.InvalidInput

        update_payment_detail(
            staff_auth_func,
            request.json['phone_number'],
            request.json['password'],
            request.json['customer_id'],
            request.json['amount'],
            request.json['update_type'],
            request.json.get('remark')
        )

        return {}
    except (KeyError, util.exceptions.InvalidInput) as e:
        print(e)
        return respond_with_error(AdminErrorCode.INVALID_INPUT)
    except util.exceptions.AuthenticationError:
        return respond_with_error(AdminErrorCode.AUTHENTICATION_ERROR)
