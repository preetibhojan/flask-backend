# Get customer details

Takes phone_number and password for authentication and returns flat
number, building, society and locality_id of the customer. 

# Sample input
```json
{
    "phone_number": "1234567890",
	"password": "abcdefghi"
}
```

# Sample output
```json
{
    "area_id": 1,
    "company": "company",
    "floor": 2,
    "office_number": "officewiguow",
    "tower": "abc"
}
```

In case the customer has not registered a home address, this is returned:
```json
{
    "enabled": false
}
```

# Error codes
It returns a 400 Bad request in case of an error with and error code.

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication error            |

## Invalid input
1. Request is not json
2. A required parameter is missing or null

## Authentication Error
Phone number password combo does not match or customer does not exist
