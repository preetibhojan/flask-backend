import logging

import psycopg2

import config
from util.database import get_customer_id
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


def get_office_address(phone_number):
    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute('select office_number, floor, company, tower_id, '
                    't.name, l.locality, a.area '
                    'from officeAddress oa '
                    'inner join tower t '
                    'on t.id = oa.tower_id '
                    'and t.active is true '
                    'inner join localities l '
                    'on l.id = t.locality_id '
                    'inner join areas a '
                    'on a.id = l.area_id '
                    'where customer_id = %(cust_id)s',
                    {
                        'cust_id': customer_id
                    })

        logger.debug(cur.query)

        row = cur.fetchone()

        if cur.rowcount == 0:
            return {
                'enabled': False
            }

        return {
            'office_number': row[0],
            'floor': row[1],
            'company': row[2],
            'tower_id': row[3],
            'tower': row[4],
            'locality': row[5],
            'area': row[6],
        }

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def get_office_address_use_case(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    return get_office_address(phone_number)
