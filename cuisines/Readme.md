# Cuisines

It returns a list of cuisines currently served. A Cuisine consists 
of and id which is an integer and a name example - Gujarati, 
Maharashtrian, etc. Each cuisine is uniquely identified by its id.  

**Notes:** 
1. The name are capitalized but this may change in future. This is 
because the number of records is going to be small and (probably) 
not change frequently

# Sample Output
It returns a lit of Cuisines. The app should only
be interested in the id for ordering. The name is for the UI to 
display. 

```json
[
  {
    "id": 1,
    "name": "Gujarati"
  },
  {
    "id": 2,
    "name": "Maharashtrian"
  }    
]
```
 