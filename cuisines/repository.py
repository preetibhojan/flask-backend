import psycopg2

import config


class Cuisine:
    def __init__(self, id, name, show_home_address, show_office_address):
        self.id = id
        self.name = name
        self.show_home_address = show_home_address
        self.show_office_address = show_office_address

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'show_home_address': self.show_home_address,
            'show_office_address': self.show_office_address,
        }


def get_cuisines():
    """
    All cuisines from database
    select privilege on Cuisines
    :return: List of Cuisine
    """
    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('SELECT id, name, show_home_address, show_office_address from Cuisines where active is true')
        rows = cur.fetchall()

        cuisines = []
        for id, name, show_home_address, show_office_address in rows:
            cuisines.append(Cuisine(id, name, show_home_address, show_office_address))

        return cuisines
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


if __name__ == '__main__':
    print([cuisine.__dict__ for cuisine in get_cuisines()])
