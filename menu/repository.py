import logging

import psycopg2

import config
from util.datetime_util import next_working_day
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


def _cuisine_id_for(phone_number):
    conn = None
    cur = None
    try:
        # conn = psycopg2.connect(host=config.DATABASE_URL,
        #                         database=config.DATABASE,
        #                         user=config.GET_MENU_USER,
        #                         password=config.GET_MENU_PASSWORD
        #                         )
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        # Select the name of meal, the menu and the image for today
        # phone number guaranteed to be unique
        cur.execute('Select cuisine_id from Customer where phone_number = %(pho_no)s',
                    {
                        'pho_no': phone_number
                    })

        logger.debug(cur.query)

        # fetchone returns tuple. We only want the first column
        return cur.fetchone()[0]
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def get_menu(phone_number, password, authenticate, now):
    try:
        authenticate(phone_number, password)
    except Exception:
        logger.info('Authentication error')
        raise AuthenticationError

    conn = None
    cur = None
    try:
        # conn = psycopg2.connect(host=config.DATABASE_URL,
        #                         database=config.DATABASE,
        #                         user=config.GET_MENU_USER,
        #                         password=config.GET_MENU_PASSWORD
        #                         )
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        """
        select menu for the given cuisine where either:
        1. date is current date but meal is not yet delivered
        2. menu for next working day
        """
        cur.execute('SELECT meal_id, cuisine_id, sabji, daal, rice, date, sweet from Menu '
                    'where cuisine_id = %(cuisine_id)s and '
                    # '(date = %(now)s or date = %(next_working_date)s)',
                    'date >= %(now)s',
                    {
                        'now': now.date(),
                        'next_working_date': next_working_day(now).date(),
                        'cuisine_id': _cuisine_id_for(phone_number),
                    })

        logger.debug(cur.query)

        rows = cur.fetchall()

        result = []
        for meal_id, cuisine_id, sabji, daal, rice, date, sweet in rows:
            result.append({
                'meal_id': meal_id,
                'cuisine_id': cuisine_id,
                'sabji': sabji,
                'daal': daal,
                'rice': rice,
                'date': date,
                'sweet': sweet
            })

        return result
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def menu_for_admin(today):
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('SELECT meal_id, cuisine_id, sabji, daal, rice, date, sweet from Menu '
                    'where date >= %s', (today,))

        result = []
        for meal_id, cuisine_id, sabji, daal, rice, date, sweet in cur.fetchall():
            result.append({
                'meal_id': meal_id,
                'cuisine_id': cuisine_id,
                'sabji': sabji,
                'daal': daal,
                'rice': rice,
                'date': date,
                'sweet': sweet
            })

        return result
