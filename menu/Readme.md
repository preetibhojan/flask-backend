# Menu
Returns the menu items for today (even the ones that are delivered)
and menu items for tomorrow. The time is in IST (+5:30 hours). It 
returns the menu for the given sabji, roti, daal, rice, date of the
menu and meal id. Other information can be fetched using meals end 
point. 
  
# Sample input
```json
{
  "phone_number": "0987654321",
  "password": "abcdefghi"
}
```

# Sample output
```json
[
    {
        "daal": null,
        "date": "2020-08-05",
        "meal_id": 1,
        "rice": null,
        "sabji": "Poha"
    },
    {
        "daal": null,
        "date": "2020-08-05",
        "meal_id": 2,
        "rice": null,
        "sabji": "Paneer"
    },
    {
        "daal": null,
        "date": "2020-08-05",
        "meal_id": 3,
        "rice": null,
        "sabji": "Test"
    }
]
```

# Errors

In case of an error, the end point returns 400 bad request with json 
response containing an `error` member with an integer error code.

```json
{
  "error": <error code> 
}
```

Error codes:

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authorization error             |
