import psycopg2
from firebase_admin import initialize_app
from firebase_admin.messaging import MulticastMessage, Notification, send_multicast

import config

initialize_app()

with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
    cur.execute('select fcm_token from customer where fcm_token is not null')

    rows = cur.fetchall()
    tokens = list(map(lambda r: r[0], rows))

    for i in range(0, len(tokens), 500):
        message = MulticastMessage(
            notification=Notification(
                title='Changes in Booking Timings',
                body='To ensure delivery on time, we have decided to '
                     'close our booking @ 10:30 AM for Lunch and 4:30 PM for Dinner'
            ),
            tokens=tokens[i:i+500],
        )

        response = send_multicast(message)
        print(response.success_count)
