from util.constants import HOME, OFFICE


class ReservationDetails:
    def __init__(self, quantity, delivery_location):
        self.quantity = quantity
        self.delivery_location = delivery_location

    def is_valid(self):
        return self.quantity > 0 and \
               (self.delivery_location == HOME
                or self.delivery_location == OFFICE)

    def __eq__(self, o: object) -> bool:
        return isinstance(o, ReservationDetails) and \
               self.quantity and \
               self.delivery_location == self.delivery_location

    def __format__(self, format_spec):
        return f'{{quantity: {self.quantity}, delivery location: {self.delivery_location}}}'

    @staticmethod
    def from_json(json):
        return ReservationDetails(
            json['quantity'],
            json['delivery_location']
        )
