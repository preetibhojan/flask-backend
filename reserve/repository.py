import logging
import uuid
from datetime import datetime

import psycopg2
from psycopg2.extras import execute_values

import config
import util.database
from util.address import AddressRepository
from util.constants import HOME, OFFICE
from util.database import get_customer_id

logger = logging.getLogger(__name__)


class _Package:
    def __init__(self, id, add_on, quantity, extra_quantity, delivery_location, tower_id):
        self.id = id
        self.add_on = add_on
        self.quantity = quantity
        self.extra_quantity = extra_quantity
        self.delivery_location = delivery_location
        self.tower_id = tower_id

    def __str__(self) -> str:
        return f'_Package[{self.id}, {self.add_on}, {self.quantity}, {self.extra_quantity}, {self.delivery_location}, {self.tower_id}]'


class ReserveRepository:
    """
    **Note:**
    Reserve Repository locks package_quantity in **access exclusive mode**. Which is released
    by calling unlock. Hence -
    1. Always pass a new object of repository to use case everytime you call the use case!!!!!!
    2. Do not share the object of this class!!!!!!
    3. This class is meant to be short lived!!!!!
    Repository for reserve.
    """

    def __init__(self):
        self.conn = None

    def has_home_address(self, phone_number):
        return AddressRepository().has_home_address(phone_number)

    def has_office_address(self, phone_number):
        return AddressRepository().has_office_address(phone_number)

    def packages_wrt_meal_ids(self, phone_number, packages_to_reserve, date_to_reserve, now):
        cur = None
        try:
            self.conn = psycopg2.connect(config.DATABASE_URL)
            cur = self.conn.cursor()

            customer_id = util.database.get_customer_id(cur, phone_number)
            util.database.remove_redundant_reservations(now, customer_id=customer_id)

            cur.execute(
                'select tower_id from homeaddress ha '            
                'where customer_id = %(cust_id)s',
                {'cust_id': customer_id},
            )
            logger.debug(cur.query)
            home_address_tower = None
            if cur.rowcount > 0:
                home_address_tower = cur.fetchone()[0]

            cur.execute(
                'select tower_id from officeaddress '
                'where customer_id = %(cust_id)s',
                {'cust_id': customer_id},
            )
            logger.debug(cur.query)
            office_address_tower = None
            if cur.rowcount > 0:
                office_address_tower = cur.fetchone()[0]

            cur.execute('lock table package_quantity in ACCESS EXCLUSIVE mode')
            cur.execute('lock table extra_quantity in ACCESS EXCLUSIVE mode')

            cur.execute(
                'select p.id, coalesce(pq.quantity, 0), p.add_on, '
                'coalesce(home_extra.quantity, 0), coalesce(office_extra.quantity, 0), '
                'p.meal_id, m.deliverytime, m.orderbefore '
                'from package_quantity pq '
                'full outer join ( '
                'select package_id, quantity, date from extra_quantity '
                'where date = %(date)s '
                'and staff_id = ( '
                'select id '
                'from staff '
                'inner join notification_permission np '
                'on staff.id = np.staff_id '
                'and np.tower_id = ( '
                'select tower_id from homeaddress '
                'where customer_id = %(cust_id)s) '
                'and staff.id != 1) '
                ') home_extra '
                'on pq.package_id = home_extra.package_id '
                'and pq.date = home_extra.date '
                'full outer join ( '
                'select package_id, quantity, date '
                'from extra_quantity '
                'where date = %(date)s '
                'and staff_id = ( '
                'select id from staff '
                'inner join notification_permission np '
                'on staff.id = np.staff_id '
                'and np.tower_id = ( '
                'select tower_id from officeaddress '
                'where customer_id = %(cust_id)s) '
                'and staff.id != 1 '
                ')) office_extra '
                'on coalesce(pq.package_id, home_extra.package_id) = office_extra.package_id '
                'and coalesce(pq.date, home_extra.date) = office_extra.date '
                'inner join packages p on p.id = pq.package_id '
                'or home_extra.package_id = p.id '
                'or office_extra.package_id = p.id '
                'inner join meals m '
                'on p.meal_id = m.id '
                'and m.active is true '
                'where '
                'coalesce(pq.package_id, home_extra.package_id, office_extra.package_id) in %(package_ids)s '
                'and coalesce(pq.date, home_extra.date, office_extra.date) = %(date)s ',
                {
                    'date': date_to_reserve,
                    'package_ids': tuple(packages_to_reserve.keys()),
                    'cust_id': customer_id,
                })

            logger.debug(cur.query)

            result = {}

            for package_id, quantity, add_on, home_extra, office_extra, meal_id, \
                delivery_time, order_before, in cur.fetchall():

                if result.get(meal_id, None) is None:
                    result[meal_id] = []

                delivery_time_for_date_to_reserve = datetime.combine(date_to_reserve, delivery_time)
                extra_quantity = home_extra \
                    if packages_to_reserve[package_id].delivery_location == HOME \
                    else office_extra

                tower_id = home_address_tower if packages_to_reserve[
                                                     package_id].delivery_location == HOME else office_address_tower

                if delivery_time_for_date_to_reserve - order_before < now:
                    quantity = 0
                else:
                    quantity += extra_quantity

                result[meal_id].append(
                    _Package(
                        package_id,
                        add_on,
                        quantity,
                        extra_quantity,
                        packages_to_reserve[package_id].delivery_location,
                        tower_id,
                    )
                )

            if len(result) > 0:
                cur.execute(
                    'select meal_id, day from cannot_order_on where meal_id in %s',
                    (tuple(result.keys()),)
                )

                cannot_order_on = {}
                for meal_id, day in cur.fetchall():
                    if cannot_order_on.get(meal_id, None) is None:
                        cannot_order_on[meal_id] = []

                    cannot_order_on[meal_id].append(day)

                for meal_id, packages in result.items():
                    for package in packages:
                        if date_to_reserve.isoweekday() in cannot_order_on.get(meal_id, []):
                            package.quantity = 0

            return result
        finally:
            if cur is not None:
                cur.close()

    def update_quantities(self, phone_number, packages_to_reserve, date_to_reserve):
        cur = None
        try:
            if self.conn is None:
                self.conn = psycopg2.connect(config.DATABASE_URL)

            cur = self.conn.cursor()

            # packages_to_reserve is a dict of package returned by packages_wrt_meal_ids
            # mapping to quantity to book

            def _packages_to_update_quantity(item):
                quantity_available = item[0].quantity - item[0].extra_quantity
                quantity_to_reserve = item[1]
                return min(quantity_available, quantity_to_reserve) > 0

            package_quantity_to_update = tuple(map(lambda p: (p[0].id, min(p[0].quantity - p[0].extra_quantity, p[1]), date_to_reserve),
                          filter(_packages_to_update_quantity, packages_to_reserve.items())))
            if len(package_quantity_to_update) > 0:
                cur.execute(
                    'UPDATE package_quantity SET quantity = quantity - data.quantity '
                    'FROM (VALUES %s) AS data (package_id, quantity, date) '
                    'WHERE package_quantity.package_id = data.package_id '
                    'and package_quantity.date = data.date',
                    (
                        package_quantity_to_update,
                    ),
                )
                logger.debug(cur.query)

            packages = list(filter(
                lambda p: p[1] >= p[0].quantity - p[0].extra_quantity and p[0].extra_quantity > 0,
                packages_to_reserve.items()))

            if len(packages) > 0:
                cur.execute(
                    'update extra_quantity eq set quantity = eq.quantity - data.quantity '
                    'from (values %s) as data(package_id, quantity, date, tower_id)',
                    tuple(map(
                        lambda p: (p[0].id, p[1] - min(p[0].quantity - p[0].extra_quantity, p[1]),
                                   date_to_reserve, p[0].tower_id),
                        packages
                    ))
                )
                logger.debug(cur.query)

            self.conn.commit()
        except Exception as e:
            print(e)
            print(cur.query)
        finally:
            if cur is not None:
                cur.close()

    def reserve(self, phone_number, packages_to_reserve, date_to_reserve, now):
        cur = None
        try:
            if self.conn is None:
                self.conn = psycopg2.connect(config.DATABASE_URL)

            cur = self.conn.cursor()

            reservation_id = str(uuid.uuid4())

            customer_id = get_customer_id(cur, phone_number)

            insert_template = cur.mogrify('(%(r_id)s, %%s, %%s, %(now)s, %(c_id)s, %%s, %(date)s, %%s, %%s)', {
                'r_id': reservation_id,
                'now': now,
                'c_id': customer_id,
                'date': date_to_reserve,
            }).decode('utf-8')

            execute_values(
                cur,
                'INSERT INTO reservations (id, package_id, quantity, '
                'created, customer_id, delivery_location, date_to_reserve, extra_quantity, tower_id) VALUES %s',
                list(map(lambda p: (p[0].id, p[1], p[0].delivery_location, p[0].extra_quantity, p[0].tower_id),
                         packages_to_reserve.items())),
                insert_template)

            logger.debug(cur.query)

            self.conn.commit()
            return reservation_id
        finally:
            if cur is not None:
                cur.close()

    def unlock(self):
        if self.conn is not None:
            self.conn.close()
            self.conn = None
