import logging

import psycopg2

import config
import util.database
from util.constants import HOME, OFFICE
from util.datetime_util import next_working_day
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger(__name__)


def cancel_reservation_use_case(phone_number, password, auth_func, reservation_id, now):
    """
    Cancel reservation for customer with the given reservation id.

    It deletes the rows in reservation table with the give id and adds
    the quantity back to package_quantity.

    :param phone_number: phone number of customer
    :param password: password of customer
    :param auth_func: a function which throws Exception when
    authentication fails
    :param reservation_id: reservation id to delete
    :param now: current date time
    :return:
    """
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    if not reservation_id:
        raise InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = util.database.get_customer_id(cur, phone_number)

        cur.execute(
            'delete from reservations '
            'where id = %s '
            'and customer_id = %s '
            'and date_to_reserve > %s '
            'returning package_id, quantity, extra_quantity, date_to_reserve, tower_id ',
            (reservation_id, customer_id, now.date())
        )
        logger.debug(cur.query)

        rows = cur.fetchall()

        if len(rows) > 0:
            cur.execute(
                'update package_quantity pq '
                'set quantity = pq.quantity + data.quantity '
                'from (values %s) as data(package_id, quantity, extra_quantity, date_to_reserve, tower_id) '
                'where pq.package_id = data.package_id '
                'and pq.date = data.date_to_reserve ',
                (rows,)
            )
            logger.debug(cur.query)

            cur.execute(
                'update extra_quantity eq '
                'set quantity = eq.quantity + data.quantity '
                'from (values %s) as data(package_id, quantity, extra_quantity, date_to_reserve, tower_id) '
                'where eq.package_id = data.package_id '
                'and eq.date = data.date_to_reserve '
                'and eq.staff_id = ('
                'select staff_id from notification_permission '
                'where tower_id = data.tower_id and '
                'eq.staff_id != 1 '
                ')',
                (rows,)
            )
            logger.debug(cur.query)

        conn.commit()


def reserve_use_case(phone_number, password, packages_to_reserve, auth_func, repository, now,
                     reserve_for_next_working_day=False):
    """
    Reserve the items for the given customer. Reserve either reserves everything or nothing.
    If it fails to reserve at least one item, it returns a dictionary with a "failed" key
    which has a list of items to reserve with the same package id and the available quantity.
    If everything succeeds, it returns a dictionary with a "success" key with the items to
    reserve that it received as input.

    If reserve calls, repository.quantity_of_package_ids_for, it is guaranteed to call
    repository.unlock

    **Note:** Result is only applicable for today with respect to now.

    :param reserve_for_next_working_day: By default, reserve books for today.
    If reserve for next working day is true, reserve for next working day
    (tomorrow if today is not saturday else monday).
    :param phone_number: phone number of the customer
    :param password: password of the customer
    :param packages_to_reserve: a dictionary with int key
    representing package id and PackageToReserve
    :param auth_func: function that throws exception when authentication fails and returns
    None in case everything okay
    :param repository: resume repository to use
    address exists for the given user
    :param now: the current date time
    :return: A dictionary with either "failed" key in case of failure or a "success" key
    in case reserve is successful.
    """
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication Error')
        raise AuthenticationError

    logger.debug('packages to reserve: ')
    for package_id, reservation_details in packages_to_reserve.items():
        logger.debug(f'package id: {package_id}, reservation details: {reservation_details}')

    if not packages_to_reserve:
        logger.info('Items to reserve is null or empty')
        raise InvalidInput

    for _, package_to_reserve in packages_to_reserve.items():
        if not package_to_reserve.is_valid():
            logger.info('An item to reserve is invalid')
            raise InvalidInput

    package_ids = list(packages_to_reserve.keys())
    logger.debug(f'package_ids: {package_ids}')

    delivery_locations = set(map(lambda p: p.delivery_location, packages_to_reserve.values()))
    logger.debug(f'delivery locations: {delivery_locations}')

    if not repository.has_home_address(phone_number) and HOME in delivery_locations:
        logger.info('home address not set')
        raise InvalidInput

    if not repository.has_office_address(phone_number) and OFFICE in delivery_locations:
        logger.info('office address not set')
        raise InvalidInput

    try:
        _next_working_day = next_working_day(now).date()

        date_to_reserve = _next_working_day if reserve_for_next_working_day else now.date()

        """
        packages_wrt_meal_ids of should return quantities of package id 
        even when they have 0 quantity because this helps the server differentiate 
        between invalid requests from genuine requests
        """
        packages_wrt_meal_id = repository.packages_wrt_meal_ids(phone_number, packages_to_reserve, date_to_reserve, now)
        for meal_id, packages in packages_wrt_meal_id.items():
            logger.debug(f'meal id: {meal_id}')
            for package in packages:
                logger.debug(f'{package},')

        total_packages = sum(map(lambda _list: len(_list), packages_wrt_meal_id.values()))
        logger.debug(f'total packages: {total_packages}')

        if total_packages != len(package_ids):
            logger.info('Not all package ids can be reserved for today')
            logger.info(f'package ids requested: {package_ids}')
            logger.info(f'package ids from database: {packages_wrt_meal_id}')
            return {'failed': True}

        failed = {}
        successful = {}

        for _, packages in packages_wrt_meal_id.items():
            normal_packages = list(filter(lambda p: p.add_on is False, packages))
            for package in normal_packages:
                quantity_to_reserve = packages_to_reserve[package.id].quantity

                if package.quantity < quantity_to_reserve:
                    logger.debug(
                        f'could not reserve package id: {package.id}. '
                        f'Quantity requested: {quantity_to_reserve}. '
                        f'Quantity available: {package.quantity}'
                    )
                    failed[package.id] = package.quantity
                    continue

                logger.debug(
                    f'reserved package: {package.id}. '
                    f'Quantity available before reserving: {package.quantity}. '
                    f'Quantity requested to reserve: {quantity_to_reserve}'
                )
                successful[package] = quantity_to_reserve

            add_ons = list(filter(lambda p: p.add_on is True, packages))

            normal_packages_ordered = 0
            for package_id, reservation_details in packages_to_reserve.items():
                if package_id in map(lambda p: p.id, normal_packages):
                    normal_packages_ordered += reservation_details.quantity

            logger.debug(f'normal packages ordered: {normal_packages_ordered}')

            for add_on in add_ons:
                quantity_to_reserve = packages_to_reserve[add_on.id].quantity

                if quantity_to_reserve > normal_packages_ordered:
                    logger.debug(
                        f'add on: {add_on.id} requested {quantity_to_reserve} quantities '
                        f'which is more than normal package count: {normal_packages_ordered}'
                    )
                    raise InvalidInput

                if add_on.quantity < quantity_to_reserve:
                    logger.debug(
                        f'could not reserve package id: {add_on.id}. '
                        f'Quantity requested: {quantity_to_reserve}. '
                        f'Quantity available: {add_on.quantity}'
                    )
                    failed[add_on.id] = add_on.quantity
                    continue

                logger.debug(
                    f'reserved add on: {add_on.id}. '
                    f'Quantity available before reserving: {add_on.quantity}. '
                    f'Quantity requested to reserve: {quantity_to_reserve}'
                )
                successful[add_on] = quantity_to_reserve

        if failed:
            logger.info(f'Could not reserve these packages: {failed}')
            return {'failed': failed}

        repository.update_quantities(phone_number, successful, date_to_reserve)
        reservation_id = repository.reserve(phone_number, successful, date_to_reserve, now)
        logger.info('Package repository quantity updated')

        return {
            'reservation_id': reservation_id
        }
    finally:
        repository.unlock()
        logger.info('package quantity unlocked')
