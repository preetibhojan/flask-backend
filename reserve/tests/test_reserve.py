import unittest
from datetime import datetime, timedelta
from unittest.mock import Mock

from packages.repository import Package
from reserve.model import ReservationDetails
from reserve.use_cases import reserve_use_case
from util.constants import HOME, OFFICE
from util.exceptions import AuthenticationError, InvalidInput


class ReserveTestCase(unittest.TestCase):
    def test_reserve_raises_authentication_error_when_auth_func_throws(self):
        with self.assertRaises(AuthenticationError):
            reserve_use_case('', '', [], lambda *x: exec('raise Exception()'), None, None)

    def test_reserve_raises_invalid_input_when_items_to_resume_is_empty(self):
        with self.assertRaises(InvalidInput):
            reserve_use_case('', '', [], lambda *x: None, None, None)

    def test_reserve_raises_invalid_input_when_a_package_is_invalid(self):
        with self.assertRaises(InvalidInput):
            packages_to_reserve = {
                1: ReservationDetails(2, HOME),
                2: ReservationDetails(0, OFFICE)
            }

            reserve_use_case('', '',
                             packages_to_reserve,
                             lambda *x: None,
                             None, None)

    def test_reserve_raises_invalid_input_when_home_address_is_not_set_but_delivery_location_contains_home(self):
        items_to_reserve = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(2, OFFICE),
            3: ReservationDetails(3, HOME),
        }

        repository = Mock()
        repository.has_home_address.return_value = False

        now = datetime.now()
        with self.assertRaises(InvalidInput):
            reserve_use_case('', '', items_to_reserve, lambda *x: None, repository, now)

        repository.has_home_address.assert_called_once_with('')

    def test_reserve_raises_invalid_input_when_office_address_is_not_set_but_delivery_location_contains_office(self):
        items_to_resume = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(2, OFFICE),
            3: ReservationDetails(3, HOME),
        }

        repository = Mock()
        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = False

        now = datetime.now()
        with self.assertRaises(InvalidInput):
            reserve_use_case('', '', items_to_resume, lambda *x: None, repository, now)

        repository.has_home_address.assert_called_once_with('')
        repository.has_office_address.assert_called_once_with('')

    def test_reserve_returns_failed_when_package_does_not_exist(self):
        items_to_resume = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(2, OFFICE),
            3: ReservationDetails(3, HOME),
        }

        repository = Mock()

        now = datetime.now()
        repository.packages_wrt_meal_ids.side_effect = lambda _, __, ___, ____: {
            1: [Package(1, 0, '', '', 0, 0, 1, '', now, False, False)],
            2: [Package(2, 0, '', '', 0, 0, 2, '', now, False, False)],
        }

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True
        
        result = reserve_use_case('', '', items_to_resume, lambda *x: None, repository, now)
        self.assertTrue(result['failed'])

        repository.unlock.assert_called_once()
        repository.packages_wrt_meal_ids.assert_called_once_with('', [1, 2, 3], now.date(), now)

        repository.has_home_address.assert_called_once_with('')
        repository.has_office_address.assert_called_once_with('')

    def test_reserve_returns_packages_because_of_which_reservation_failed_when_reservation_fails(self):
        items_to_resume = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(2, OFFICE),
            3: ReservationDetails(1, HOME),
        }

        repository = Mock()
        repository.packages_wrt_meal_ids.side_effect = lambda _, __, ___, ____: {
            1: [
                Package(1, 1, '', '', 0, 0, 1, '', now, False, False),
                Package(3, 1, '', '', 0, 0, 1, '', now, False, False),
            ],
            2: [Package(2, 1, '', '', 0, 0, 2, '', now, False, False)],
        }

        now = datetime.now()

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        result = reserve_use_case('', '', items_to_resume, lambda *x: None, repository, now)

        self.assertEqual(result['failed'], {
            2: 1,
        })

        repository.unlock.assert_called_once()
        repository.packages_wrt_meal_ids.assert_called_once_with('', [1, 2, 3], now.date(), now)

        repository.has_home_address.assert_called_once_with('')
        repository.has_office_address.assert_called_once_with('')

    def test_reserve_calls_unlock_even_if_a_repository_throws(self):
        items_to_resume = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(1, OFFICE),
            3: ReservationDetails(1, HOME),
        }

        repository = Mock()
        repository.packages_wrt_meal_ids.side_effect = Exception

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        now = datetime.now()

        with self.assertRaises(Exception):
            reserve_use_case('', '', items_to_resume, lambda *x: None, repository, now)

        repository.unlock.assert_called_once()

        repository.has_home_address.assert_called_once_with('')
        repository.has_office_address.assert_called_once_with('')

    def test_raises_invalid_input_when_quantity_of_add_ons_is_more_than_normal_packager_for_a_meal(self):
        items_to_resume = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(1, OFFICE),
            3: ReservationDetails(3, HOME),
        }

        repository = Mock()
        repository.packages_wrt_meal_ids.side_effect = lambda _, __, ___, ____: {
            1: [
                Package(1, 1, '', '', 0, 0, 1, '', now, False, False),
                Package(2, 1, '', '', 0, 0, 2, '', now, False, False),
                # this package is add on because cuisine id is null
                Package(3, 3, '', '', 0, 0, 1, '', now, False, True),
            ],
        }

        now = datetime.now()

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        with self.assertRaises(InvalidInput):
            reserve_use_case('', '', items_to_resume, lambda *x: None, repository, now)

        repository.unlock.assert_called_once()
        repository.packages_wrt_meal_ids.assert_called_once_with('', [1, 2, 3], now.date(), now)

        repository.has_home_address.assert_called_once_with('')
        repository.has_office_address.assert_called_once_with('')

    def test_success(self):
        items_to_resume = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(1, OFFICE),
            3: ReservationDetails(1, HOME),
        }

        repository = Mock()
        repository.packages_wrt_meal_ids.side_effect = lambda _, __, ___, ____: {
            1: [
                Package(1, 1, '', '', 0, 0, 1, '', now, False, False),
                Package(3, 1, '', '', 0, 0, 1, '', now, False, False),
            ],
            2: [Package(2, 1, '', '', 0, 0, 2, '', now, False, False)],
        }

        reservation_id = '12345678'
        repository.reserve.return_value = reservation_id

        now = datetime.now()

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        result = reserve_use_case('', '', items_to_resume, lambda *x: None, repository, now)

        self.assertEqual(result, {
            'successful': {
                1: 1,
                2: 1,
                3: 1
            },
            'reservation_id': reservation_id
        })

        repository.update_quantities.assert_called_once_with({1: 0, 2: 0, 3: 0}, now.date())
        repository.unlock.assert_called_once()
        repository.packages_wrt_meal_ids.assert_called_once_with('', [1, 2, 3], now.date(), now)
        repository.reserve.assert_called_once_with('', items_to_resume, now.date(), now)

        repository.has_home_address.assert_called_once_with('')
        repository.has_office_address.assert_called_once_with('')

    def test_success_for_next_working_day(self):
        items_to_resume = {
            1: ReservationDetails(1, HOME),
            2: ReservationDetails(1, OFFICE),
            3: ReservationDetails(1, HOME),
        }

        repository = Mock()
        repository.packages_wrt_meal_ids.side_effect = lambda _, __, ___, ____: {
            1: [
                Package(1, 1, '', '', 0, 0, 1, '', now, False, False),
                Package(3, 1, '', '', 0, 0, 1, '', now, False, False),
            ],
            2: [Package(2, 1, '', '', 0, 0, 2, '', now, False, False)],
        }

        reservation_id = '12345678'
        repository.reserve.return_value = reservation_id

        # a friday
        now = datetime(2020, 9, 11)

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        result = reserve_use_case('', '', items_to_resume, lambda *x: None, repository, now,
                                  reserve_for_next_working_day=True)

        self.assertEqual(result, {
            'successful': {
                1: 1,
                2: 1,
                3: 1
            },
            'reservation_id': reservation_id
        })

        date_to_reserve = now.date() + timedelta(days=1)

        repository.update_quantities.assert_called_once_with({1: 0, 2: 0, 3: 0}, date_to_reserve)
        repository.unlock.assert_called_once()
        repository.packages_wrt_meal_ids.assert_called_once_with('', [1, 2, 3], date_to_reserve, now)
        repository.reserve.assert_called_once_with('', items_to_resume, date_to_reserve, now)

        repository.has_home_address.assert_called_once_with('')
        repository.has_office_address.assert_called_once_with('')
