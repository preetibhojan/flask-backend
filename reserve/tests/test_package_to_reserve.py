import unittest

from reserve.model import ReservationDetails
from util.constants import HOME, OFFICE


class PackageToReserveTestCase(unittest.TestCase):
    def test_is_valid_returns_false_when_quantity_is_0(self):
        package = ReservationDetails(0, HOME)

        self.assertFalse(package.is_valid())

    def test_is_valid_returns_false_when_quantity_is_negative(self):
        package = ReservationDetails(-1, HOME)

        self.assertFalse(package.is_valid())

    def test_is_valid_returns_false_when_delivery_location_is_not_home_or_office(self):
        package = ReservationDetails(0, 12)

        self.assertFalse(package.is_valid())

    def test_from_json_passes_correct_parameters_to_correct_fields(self):
        package = ReservationDetails.from_json({
            'quantity': 2,
            'delivery_location': OFFICE
        })

        self.assertEqual(package, ReservationDetails(2, OFFICE))


if __name__ == '__main__':
    unittest.main()
