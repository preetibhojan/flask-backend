import logging

import psycopg2

from config import DATABASE_URL
from convert.model import PackageConversion
from util.exceptions import InvalidInput, UnauthorizedAccess

logger = logging.getLogger(__name__)


def package_conversions():
    with psycopg2.connect(DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('select id, column_name, name from package_conversion')
        
        result = {}
        for id, column, name in cur.fetchall():
            result[id] = PackageConversion(id, column, name)

        return result


def convert_use_case(phone_number, password, auth_func, package_conversion, meal_id, cuisine_id, gst, date, now):
    try:
        auth_func(phone_number, password)
    except:
        raise InvalidInput

    if date < now.date():
        raise InvalidInput

    with psycopg2.connect(DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select convert from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        if cur.fetchone()[0] is not True:
            logger.critical('unauthorised access to set quantity')
            raise UnauthorizedAccess

        # sql injection cannot happen because package_conversion is 
        # not inputted by user
        cur.execute(
            'with pkgs as ( '
            f'select p1.id as old_id, c.{package_conversion.column} as new_id, p1.price - p2.price as price '
            'from packages p1 '
            'inner join conversions c '
            'on p1.id = c.package_id '
            'and p1.meal_id = %(m_id)s '
            'and p1.cuisine_id = %(c_id)s '
            'inner join packages p2 '
            f'on p2.id = c.{package_conversion.column}'
            '), '
            'update_deliverables as ( '
            'update deliverables d '
            'set package_id = pkgs.new_id, '
            # 
            'price = d.price - ((pkgs.price * quantity) + (pkgs.price * quantity) * %(gst)s / 100::real) '
            'from pkgs '
            'where package_id = pkgs.old_id '
            'and date = %(date)s '
            'and (status = \'ordered\' or status = \'delivered\') '
            'returning customer_id, ((pkgs.price * quantity) + (pkgs.price * quantity) * %(gst)s / 100::real) as price '
            ') '
            'update paymentdetail pd '
            'set temp_credit = temp_credit + update_deliverables.price '
            'from update_deliverables '
            'where pd.customer_id = update_deliverables.customer_id '
            'returning pd.customer_id, temp_credit',
            {
                'date': date,
                'gst': gst,
                'm_id': meal_id,
                'c_id': cuisine_id,
                'col': package_conversion.column
            }
        )
        logger.debug(cur.query)

        conn.commit()
