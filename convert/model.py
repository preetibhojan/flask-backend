class PackageConversion:
    def __init__(self, id, column, name):
        self.id = id
        self.column = column
        self.name = name

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name
        }
