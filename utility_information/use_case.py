import logging

import psycopg2

import config
from util.exceptions import AuthenticationError


logger = logging.getLogger(__name__)


def utility_information_use_case(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        # Customer is authenticated. We know for sure customer exists
        cur.execute('select first_name, isregularcustomer from customer where phone_number = %(pho_no)s',
                    {
                        'pho_no': phone_number
                    })

        logger.debug(cur.query)

        row = cur.fetchone()
        name = row[0]
        is_regular_customer = row[1]

        return {
            'name': name,
            'is_regular_customer': is_regular_customer
        }
    finally:
        if conn is not None:
            conn.close()

        if cur is not None:
            cur.close()
