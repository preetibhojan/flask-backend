# Utility information

Returned utility information required for the app to function. 
Currently returns name and has_scheduled_before

# Sample input
```json
{
	"phone_number": "1234567890",
	"password": "abcdef"
}
```

# Success
If the phone number and password combo match, the response contains
result parameter with a string 
```json
{
    "is_regular_customer": false,
    "name": "Hemil"
}
```

# Errors
In case of an error, a 400 bad request is returned along with a json
response along the lines of 
```json
{
  "error": <error code>
}
```

List of errors:

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication error            |

## Invalid input
It means either -
1. Input contains null where it shouldn't
2. A required field is omitted
3. Request body does not contain json

## Authentication Error
Phone number, password combo does not match 