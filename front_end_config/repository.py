import psycopg2

import config
from util.constants import VERSION, HOME, OFFICE


def front_end_config(phone_number=None):
    """
    Returns the config needed for front end. Currently gst percent 
    and if phone number is not None, then also return need based customer
    delta
    """
    conn = None
    cur = None

    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('select parameter, value from config')

        result = {}

        for parameter, value in cur.fetchall():
            result[parameter] = float(value)

        result['version'] = VERSION

        if phone_number is not None:
            cur.execute('select id, cuisine_id, exceptional from customer where phone_number=%s', (phone_number,))
            customer_id, cuisine_id, exceptional = cur.fetchone()

            cur.execute(
                'select meal_id, need_based_customer_delta from mealscuisines '
                'where cuisine_id = %s',
                (cuisine_id,)
            )

            need_based_customer_delta = {}
            if exceptional:
                for meal_id, delta in cur.fetchall():
                    need_based_customer_delta[meal_id] = 0.0
            else:
                for meal_id, delta in cur.fetchall():
                    need_based_customer_delta[meal_id] = float(delta)

            cur.execute(
                'select s.gst '
                'from homeaddress ha '
                'inner join society s '
                'on ha.tower_id = s.tower_id '
                'and ha._flat_number = s.building '
                'and ha._building = s.flat_number '
                'and ha.customer_id = %s',
                (customer_id,)
            )

            # only 0 or 1 row should be returned
            assert 0 <= cur.rowcount <= 1
            gst_at_home = None

            if cur.rowcount == 1:
                gst_at_home = float(cur.fetchone()[0])

            cur.execute(
                'select o.gst '
                'from officeaddress oa '
                'inner join office o '
                'on oa._company = o.company '
                'and oa.tower_id = o.tower_id '
                'and oa._office_number = o.office_number '
                'and oa.customer_id = %s',
                (customer_id,)
            )

            # only 0 or 1 row should be returned
            assert 0 <= cur.rowcount <= 1
            gst_at_office = None
            if cur.rowcount == 1:
                gst_at_office = float(cur.fetchone()[0])

            result['gst'] = {
                str(HOME): gst_at_home if gst_at_home is not None else result['GST_percent'],
                str(OFFICE): gst_at_office if gst_at_office is not None else result['GST_percent'],
            }

            result['need_based_customer_delta'] = need_based_customer_delta

        return result

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def admin_config():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select meal_id, cuisine_id, need_based_customer_delta '
            'from mealscuisines'
        )

        result = []
        for meal_id, cuisine_id, need_based_customer_delta in cur.fetchall():
            result.append(
                {
                    'meal_id': meal_id,
                    'cuisine_id': cuisine_id,
                    'need_based_customer_delta': float(need_based_customer_delta),
                }
            )

        return result
