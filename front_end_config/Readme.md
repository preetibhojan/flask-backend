# Front end config

Some configuration parameters that front end needs to work but the
client wants to control. Its a simple get request. No params.

# Output
```json
{
    "GST_percent": 5.0,
    "home_delivery_delta": 5.0,
    "need_based_customer_delta": 5.0
}
```

1. GST percent - The percent GST to apply on purchase. 
2. Home delivery delta - The extra cost that needs to be paid to 
deliver at home
3. Need based customer delta - Prices of packages are for regular 
customer. If a need based customer wants the package, he has to pay
the need based customer delta too.

**Note:**
1. If a need based customer wants package to be delivered at home,
they have to pay both `home_delivery_delta` and 
`need_based_customer_delta`
2. Price of all packages are excluding GST