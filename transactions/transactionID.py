import logging
import uuid

import psycopg2
import psycopg2.extras

import config
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger(__name__)


def generate_transaction_id(phone_number, password, auth_func, repository, now):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication error')
        raise AuthenticationError

    if not phone_number.isnumeric() or \
            len(phone_number) != 10:
        logger.info('phone number is not 10 digit')
        raise InvalidInput

    return repository.get_next_value_for(phone_number, now)


def transaction_id(customer_id, amount, now, source=None, reservation_id=None):
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        t_id = str(uuid.uuid4())

        cur.execute(
            'insert into transactions(customer_id, created, id, amount, source, reservation_id, remark) '
            'values (%s, %s, %s, %s, %s, %s, %s)',
            (customer_id, now, t_id, amount, source, reservation_id, 'App')
        )

        conn.commit()

        return t_id
