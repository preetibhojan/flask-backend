import unittest
from datetime import datetime
from unittest.mock import Mock

from transactions.transactionID import *


class GenerateTransactionID(unittest.TestCase):
    def test_generate_transaction_id_raises_authentication_error_when_auth_func_throws(self):
        with self.assertRaises(AuthenticationError):
            generate_transaction_id(None, None, lambda *x: exec('raise Exception()'), None, None)

    def test_generate_transaction_id_raises_invalid_input_when_phone_number_does_not_consists_of_only_numbers(self):
        with self.assertRaises(InvalidInput):
            generate_transaction_id('098765432a', 'test abc', lambda *x: None, None, None)

    def test_generate_transaction_id_raises_invalid_input_when_phone_number_is_not_10_digit(self):
        with self.assertRaises(InvalidInput):
            generate_transaction_id('09876543211', 'test abc', lambda *x: None, None, None)

    def test_generate_transaction_id_works_in_april(self):
        repository = Mock()
        now = datetime(2020, 4, 1)
        repository.get_next_value_for.return_value = '2020-21-04-12345'

        phone_number = '1234567890'
        password = 'test abc'
        transaction_id = generate_transaction_id(phone_number, password, lambda *x: None, repository, now)

        self.assertEqual(transaction_id, '2020-21-04-12345')
        repository.get_next_value_for.assert_called_once_with(phone_number, now)

    def test_generate_transaction_id_works_in_march(self):
        repository = Mock()
        now = datetime(2020, 3, 31)
        repository.get_next_value_for.return_value = '2019-20-03-12345'

        phone_number = '1234567890'
        password = 'test abc'
        transaction_id = generate_transaction_id(phone_number, password, lambda *x: None, repository, now)

        self.assertEqual(transaction_id, '2019-20-03-12345')
        repository.get_next_value_for.assert_called_once_with(phone_number, now)


if __name__ == '__main__':
    unittest.main()
