class UnauthorizedAccess(Exception):
    pass


class TransactionIDDoesNotExist(Exception):
    pass
