import logging
from datetime import date

import psycopg2
from psycopg2 import errors

import config
from transactions.models import TransactionDetails, TransactionStatus
from util.database import get_customer_id
from util.datetime_util import to_financial_year_month_string

logger = logging.getLogger(__name__)


class TransactionRepository:
    def get_next_value_for(self, phone_number, now: date):
        conn = None
        cur = None
        try:
            # conn = psycopg2.connect(host=config.DATABASE_URL,
            #                         database=config.DATABASE,
            #                         user=config.PAYMENT_USER,
            #                         password=config.PAYMENT_PASSWORD
            #                         )
            conn = psycopg2.connect(config.DATABASE_URL)
            cur = conn.cursor()

            financial_year = to_financial_year_month_string(now)
            cur.execute('select max(substring(id from 12 for 5)::int) from transactions '
                        'where id like %(date)s',
                        {
                            'date': f'{financial_year}%'
                        })

            logger.debug(cur.query)

            row = cur.fetchone()

            # Cannot use row count because max will return 1 row even if the content is null.
            # So check is first column of the returned row is null
            if row[0]:
                bill_no = str(int(row[0]) + 1).zfill(5)
            else:
                bill_no = '00001'

            next_transaction_id = f'{financial_year}-{bill_no}'

            customer_id = get_customer_id(cur, phone_number)

            cur.execute('insert into transactions(customer_id, id, created) '
                        'values (%(cust_id)s, %(id)s, %(date)s)',
                        {
                            'cust_id': customer_id,
                            'id': next_transaction_id,
                            'date': now
                        })

            logger.debug(cur.query)

            conn.commit()

            return next_transaction_id

        except psycopg2.errors.lookup('23505'):
            """
                23505 is Unique Violation. This happens due to a race condition. Because isolation level is
                repeatable read, if another transactions inserts the next value before this, database will
                return unique violation. 
                
                In case database returns unique violation, recursively call get_next_value_for until either
                some other exception is thrown (bug in the code) or the insertion is successful 
            """
            return self.get_next_value_for(phone_number, now)
        finally:
            if conn is not None:
                conn.close()
            if cur is not None:
                cur.close()

    def successful_transaction_for(self, phone_number):
        conn = None
        cur = None
        try:
            conn = psycopg2.connect(config.DATABASE_URL)
            cur = conn.cursor()

            customer_id = get_customer_id(cur, phone_number)

            cur.execute(
                'select id, created, status, amount '
                'from transactions '
                'where customer_id = %s '
                'and status = \'successful\'',
                (customer_id,)
            )

            logger.info(cur.query)

            result = []

            for t_id, created, status, amount in cur.fetchall():
                transaction = TransactionDetails(
                    phone_number,
                    t_id,
                    created,
                    TransactionStatus.from_string(status),
                    amount
                )
                result.append(transaction)

            return result
        finally:
            if conn is not None:
                conn.close()
            if cur is not None:
                cur.close()
