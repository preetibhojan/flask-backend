import util.exceptions


def transaction_history(phone_number, password, auth_func, repository):
    """
    Returns transaction history of given customer

    :param phone_number: phone number of customer
    :param password: password of customer
    :param auth_func: a function which raises Excepion (or its subclass)
    in case of authentication failure
    :param repository: repository to use

    :return: a list of transactions
    """
    try:
        auth_func(phone_number, password)
    except Exception:
        raise util.exceptions.AuthenticationError

    return repository.successful_transaction_for(phone_number)
