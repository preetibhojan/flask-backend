# Transaction

Transaction related utilities like creating a transaction id, checking
if they are valid, etc.

## Generate transaction id

It generates a transaction id. It accepts phone number and name for
authentication

### Sample input
```json
{
	"phone_number": "1234567890",
	"password": "Test ABC"
}
```

### Sample output
```json
{
    "result": "2020-21-07-00001"
}
```

### Error codes
In case of error, it returns a 400 bad request along with an `error`
field in json. Error codes are -

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication Error            |


#### Invalid input
Invalid input is returned when -
1. Required parameters are null or empty
2. Phone number does not consist of only 10 digits. 

### Authentication error
Returned when -
1. Phone number of password is empty or missing
2. Phone number password combo does not match


## Is transaction ID used
Returns if a given transaction id is used to fulfil a payment.

Takes phone_number and password for authentication and transaction id
which is the transaction id to verify

### Sample input
```json
{
	"phone_number": "1234567890",
	"password": "Test ABC",
	"result": "2020-21-07-00001"
}
```

### Sample output
If result is true, the transaction id has been used in the transaction.
Otherwise the transaction id has not been used. 
```json
{
  "result": true
}
```

### Error codes
In case of error, it returns a 400 bad request along with an `error`
field in json. Error codes are -

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication Error            |
|  2         | Unauthorized access             |
|  3         | Transaction ID does not exist   |

Invalid input and authentication error are the same as their counterparts 
in generate transaction id. 

### Unauthorized access
The transaction id you want information about is not created by you


### Transaction ID does not exit
Self explanatory. Transaction id does not exist. 
