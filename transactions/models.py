from enum import IntEnum


class TransactionDetails:
    def __init__(self, phone_number, id, created, status, amount):
        self.phone_number = phone_number
        self.id = id
        self.created = created
        self.status = status
        self.amount = amount

    def serialize(self):
        return {
            'phone_number': self.phone_number,
            'id': self.id,
            'created': self.created.isoformat(),
            'status': self.status,
            'amount': str(self.amount)
        }


class TransactionStatus(IntEnum):
    SUCCESSFUL = 0
    NOT_USED = 1

    @staticmethod
    def from_string(string):
        if string == 'successful':
            return TransactionStatus.SUCCESSFUL
        if string is None:
            return TransactionStatus.NOT_USED

        raise ValueError('Transaction status should either be successful or None')
