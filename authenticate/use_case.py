"""
Authenticate use case. Checks if customer exists in the repository using phone number.
If so, checks if the given password and the password hash match
"""

import logging

from authenticate.exceptions import CustomerDoesNotExist, IncorrectPassword
from util.exceptions import InvalidInput
from util.password import is_correct


logger = logging.getLogger(__name__)


def authenticate_use_case(phone_number, password, password_for):
    """
    Authenticate the given user with the given phone number and password
    :param phone_number: phone number of customer
    :param password: password of customer
    :param password_for: a function that fetches the hashed password for customer with the given
    phone number from repository
    :return: True if authentication is successful. False otherwise
    """
    if not phone_number or not password:
        logger.info('phone number or password is null or empty')
        raise InvalidInput

    hashed_password = password_for(phone_number)

    if hashed_password is None:
        logger.info('customer does not exist')
        raise CustomerDoesNotExist

    if not is_correct(password, hashed_password):
        logger.info('incorrect password')
        raise IncorrectPassword

    logger.info('authentication successful')
    return True
