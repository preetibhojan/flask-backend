"""
Repository for authenticate. It connects to the database and fetches the hashed password
"""

import logging

import psycopg2

import config

logger = logging.getLogger(__name__)


def password_for_customer(phone_number):
    """
    Password hash of the customer with the given phone number stored in database
    :param phone_number: phone number of customer
    :return: string representing the password hash
    """
    conn = None
    cur = None
    try:
        # conn = psycopg2.connect(host=config.DATABASE_URL,
        #                         database=config.DATABASE,
        #                         user=config.AUTHENTICATE_USER,
        #                         password=config.AUTHENTICATE_PASSWORD
        #                         )
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('SELECT password from Customer where phone_number = %(pho_no)s',
                    {'pho_no': phone_number})
        logger.debug(cur.query)

        row = cur.fetchone()

        if row is None:
            return None

        return row[0]
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def password_for_staff(phone_number):
    """
    Password hash of the staff with the given phone number stored in
    database
    :param phone_number: phone number of staff
    :return: string representing the password hash
    """
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('SELECT password from staff where phone_number = %s',
                    (phone_number,))

        logger.debug(cur.query)

        row = cur.fetchone()

        if row is None:
            return None

        return row[0]


def update_token(phone_number, new_token):
    if not new_token:
        return

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'update customer '
            'set fcm_token = %s '
            'where phone_number = %s',
            (new_token, phone_number)
        )
        logger.debug(cur.query)

        conn.commit()


def set_version(phone_number, version):
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'update customer '
            'set version = %s '
            'where phone_number = %s',
            (version, phone_number)
        )
        logger.debug(cur.query)

        conn.commit()
