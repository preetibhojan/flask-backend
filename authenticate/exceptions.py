"""
Exceptions raised by authenticate use case in addition to common.exceptions
"""


class CustomerDoesNotExist(Exception):
    """
    Raised when a customer with the given phone number does not exist
    """


class IncorrectPassword(Exception):
    """
    Raised when password is wrong
    """
