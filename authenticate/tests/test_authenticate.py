"""
Tests for authenticate use case
"""

import unittest
from unittest.mock import Mock

from authenticate.exceptions import CustomerDoesNotExist, IncorrectPassword
from authenticate.use_case import authenticate_use_case
from util.exceptions import InvalidInput
from util.password import hash_password


class AuthenticateTestCase(unittest.TestCase):
    def test_authenticate_raises_invalid_input_when_phone_number_is_none(self):
        with self.assertRaises(InvalidInput):
            authenticate_use_case(None, 'ajhdf', None)

    def test_authenticate_raises_invalid_input_when_password_is_none(self):
        with self.assertRaises(InvalidInput):
            authenticate_use_case('kughaf', None, None)

    def test_authenticate_raises_invalid_input_when_phone_number_is_empty(self):
        with self.assertRaises(InvalidInput):
            authenticate_use_case('', 'ajhdf', None)

    def test_authenticate_raises_invalid_input_when_password_is_empty(self):
        with self.assertRaises(InvalidInput):
            authenticate_use_case('None', '', None)

    def test_authenticate_raises_customer_does_not_exist_when_customer_does_not_exist(self):
        password_for = Mock()
        password_for.return_value = None

        phone_number = 'abc'
        password = 'def'

        with self.assertRaises(CustomerDoesNotExist):
            authenticate_use_case(phone_number, password, password_for)

        password_for.assert_called_once_with(phone_number)

    def test_authenticate_raises_invalid_password_when_password_is_invalid(self):
        phone_number = 'abc'
        password = 'def'

        password_for = Mock()
        password_for.return_value = hash_password('a')

        with self.assertRaises(IncorrectPassword):
            self.assertFalse(authenticate_use_case(phone_number, password, password_for))
        password_for.assert_called_once_with(phone_number)

    def test_authenticate_returns_true_when_everything_is_valid(self):
        phone_number = 'abc'
        password = 'def'

        password_for = Mock()
        password_for.return_value = hash_password(password)

        self.assertTrue(authenticate_use_case(phone_number, password, password_for))
        password_for.assert_called_once_with(phone_number)


if __name__ == '__main__':
    unittest.main()
