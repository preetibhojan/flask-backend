# Authenticate

Being a Restful API, the back end has no concept of login and logout.
Each request that needs authentication needs to send the phone number
and password along with any other data required. This end point is
used to authenticate the user. 

# Sample input
```json
{
	"phone_number": "1234567890",
	"password": "abcdef"
}
```

# Success
If the phone number and password combo match, the response is with 
200 OK HTTP status code
```json
{
  "success": true
}
```

# Errors
In case of an error, a 400 bad request is returned along with a json
response along the lines of 
```json
{
  "error": <error code>
}
```

List of errors:

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Customer does not exist         |
|  2         | Incorrect password              |

## Invalid input
It means either -
1. Input contains null where it shouldn't
2. A required field is omitted
3. Request body does not contain json

## Customer does not exist
Self explanatory

## Invalid password
Self explanatory