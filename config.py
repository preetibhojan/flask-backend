import distutils.util
import logging
import os

from dotenv import load_dotenv

load_dotenv()


class Config(object):
    DEBUG = False
    TESTING = False


class ProductionConfig(Config):
    pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


DATABASE_URL = os.getenv('DATABASE_URL')

# If log level is defined as an environment variable and it is valid, set it to the respective level
_log_level = os.getenv('LOG_LEVEL', logging.INFO)

if _log_level == 'DEBUG':
    log_level = logging.DEBUG
elif _log_level == 'INFO':
    log_level = logging.INFO
elif _log_level == 'ERROR':
    log_level = logging.ERROR
elif _log_level == 'CRITICAL':
    log_level = logging.CRITICAL

MERCHANT_KEY = os.getenv('MERCHANT_KEY')
MERCHANT_ID = os.getenv('MERCHANT_ID')
WEBSITE = os.getenv('WEBSITE')

INITIATE_TRANSACTION_URL = os.getenv('INITIATE_TRANSACTION_URL')
TRANSACTION_STATUS_URL = os.getenv('TRANSACTION_STATUS_URL')
CALLBACK_URL = os.getenv('CALLBACK_URL')

PRODUCTION = bool(distutils.util.strtobool(os.getenv('PRODUCTION')))
IS_STAGING = not PRODUCTION

FCM_SERVER_KEY = os.getenv('FCM_SERVER_KEY')
FCM_SEND_URL = os.getenv('FCM_SEND_URL')

EMAIL_ID = os.getenv('EMAIL_ID')
EMAIL_PASSWORD = os.getenv('EMAIL_PASSWORD')

WHATSAPP_VERIFY_TOKEN = os.getenv('WHATSAPP_VERIFY_TOKEN')
WHATSAPP_APP_SECRET = os.getenv('WHATSAPP_APP_SECRET')
