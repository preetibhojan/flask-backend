import unittest
from unittest.mock import Mock

from payment_gateway.initiate_transaction.use_case import initiate_transaction_use_case
from util.exceptions import AuthenticationError, InvalidInput


class InitiateTransactionTransactionTestCase(unittest.TestCase):
    def test_initiate_transaction_raises_authentication_error_when_auth_func_raises_exception(self):
        auth_func = Mock()
        auth_func.side_effect = Exception

        with self.assertRaises(AuthenticationError):
            initiate_transaction_use_case('', '', auth_func, 0, None, None, None)

        auth_func.assert_called_once_with('', '')

    def test_initiate_transaction_raises_invalid_input_when_amount_is_0(self):
        auth_func = Mock()

        with self.assertRaises(InvalidInput):
            initiate_transaction_use_case('', '', auth_func, 0, None, None, None)

        auth_func.assert_called_once_with('', '')

    def test_initiate_transaction_raises_invalid_input_when_amount_is_negative(self):
        auth_func = Mock()

        with self.assertRaises(InvalidInput):
            initiate_transaction_use_case('', '', auth_func, -1, None, None, None)

        auth_func.assert_called_once_with('', '')

    def test_success(self):
        auth_func = Mock()

        payment_gateway = Mock()
        token = 'wukftwerv'
        callback_url = 'wyrfw ef'
        payment_gateway.initiate_transaction.return_value = (token, callback_url)

        transaction_id = Mock()
        t_id = 'rruktfwef'
        transaction_id.return_value = t_id

        customer_id = Mock()
        cust_id = 'wuftbwef'
        customer_id.return_value = cust_id

        amount = 10
        _t_id, _token, _callback_url = initiate_transaction_use_case(
            '',
            '',
            auth_func,
            amount,
            transaction_id,
            payment_gateway,
            customer_id
        )

        self.assertEqual(_token, token)
        self.assertEqual(_t_id, t_id)
        self.assertEqual(_callback_url, callback_url)

        auth_func.assert_called_once_with('', '')
        transaction_id.assert_called_once_with(cust_id, amount)
        customer_id.assert_called_once_with('')
        payment_gateway.initiate_transaction.assert_called_once_with(cust_id, t_id, amount)


if __name__ == '__main__':
    unittest.main()
