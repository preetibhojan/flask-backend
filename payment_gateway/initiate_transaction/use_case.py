from util.exceptions import AuthenticationError, InvalidInput


def initiate_transaction_use_case(phone_number, password, auth_func, amount, transaction_id, payment_gateway,
                                  customer_id):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    if amount < 1:
        raise InvalidInput

    cust_id = str(customer_id(phone_number))
    t_id = str(transaction_id(cust_id, amount))
    token, callback_url = payment_gateway.initiate_transaction(cust_id, t_id, amount)
    return t_id, token, callback_url


SCHEDULE = 0
# SPOT_BOOKING was 1 but it is no longer supported so its removed
RECHARGE = 2
RESUME = 3
SPOT_BOOKING2 = 4
MANUAL_ENTRY = 5


def initiate_transaction_v2_use_case(
        phone_number, password, auth_func, amount, source, reservation_id, transaction_id,
        payment_gateway, customer_id,
):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    if amount < 1:
        raise InvalidInput

    valid_sources = [SCHEDULE, RECHARGE, RESUME, SPOT_BOOKING2]

    if source not in valid_sources:
        raise InvalidInput

    cust_id = str(customer_id(phone_number))
    t_id = str(transaction_id(cust_id, amount, source, reservation_id))
    token, callback_url = payment_gateway.initiate_transaction(cust_id, t_id, amount)
    return t_id, token, callback_url
