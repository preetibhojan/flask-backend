import logging

import psycopg2
from paytmchecksum import PaytmChecksum

import config
from payment_gateway.update_transaction_status.model import PaymentStatus
from util.database import get_customer_id
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger(__name__)


def update_transaction_status_use_case(phone_number, password, auth_func, payment_gateway, transaction_id):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)

        cur.execute(
            'select amount, status '
            'from transactions '
            'where id = %s '
            'and customer_id = %s '
            'for update',
            (transaction_id, customer_id)
        )
        logger.info(cur.query)

        amount, status = cur.fetchone()

        if amount is None or status == 'successful':
            return

        payment_status = payment_gateway.payment_status(transaction_id)

        cur.execute(
            'update transactions '
            'set status = %s '
            'where id = %s',
            (payment_status.to_string(), transaction_id)
        )
        logger.info(cur.query)

        if payment_status == PaymentStatus.SUCCESSFUL:
            cur.execute(
                'update paymentdetail '
                'set temp_credit = temp_credit + %s '
                'where customer_id = %s',
                (amount, customer_id)
            )
            logger.info(cur.query)

        conn.commit()


def update_transaction_status(paytm_params):
    checksum = paytm_params['CHECKSUMHASH']
    paytm_params.pop('CHECKSUMHASH', None)

    valid = PaytmChecksum.verifySignature(paytm_params, config.MERCHANT_KEY, checksum)
    if not valid:
        logger.critical('Someone sent an invalid checksum!')
        raise InvalidInput

    status_code = paytm_params['RESPCODE']
    logger.debug(f'status code: {status_code}')

    if status_code == '01':
        payment_status = PaymentStatus.SUCCESSFUL
    else:
        payment_status = PaymentStatus.FAILED

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'update transactions '
            'set status = %s '
            'where id = %s '
            'returning customer_id, source, reservation_id, amount',
            (payment_status.to_string(), paytm_params['ORDERID'])
        )
        logger.debug(cur.query)

        row = cur.fetchone()
        customer_id = row[0]
        source = row[1]
        reservation_id = row[2]

        amount = paytm_params['TXNAMOUNT']
        print(f'amount from paytm = {amount}')
        print(f'amount in transaction {row[3]}')

        if payment_status == PaymentStatus.SUCCESSFUL:
            cur.execute('select temp_credit from paymentdetail where customer_id = %s', (customer_id,))
            logger.debug(f'temp credit before adding amount: {cur.fetchone()[0]}')

            cur.execute(
                'update paymentdetail '
                'set temp_credit = temp_credit + %s '
                'where customer_id = %s returning temp_credit',
                (amount, customer_id)
            )
            logger.debug(cur.query)
            logger.debug(f'temp credit after adding amount: {cur.fetchone()[0]}')

            cur.execute('select phone_number from customer where id = %s', (customer_id,))

            return source, reservation_id, cur.fetchone()[0]

        conn.commit()
