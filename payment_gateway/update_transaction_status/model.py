from enum import Enum


class PaymentStatus(Enum):
    SUCCESSFUL = 0
    PENDING = 1
    INVALID = 2
    FAILED = 3
    REFUNDED = 4

    def to_string(self):
        if self.value == self.SUCCESSFUL.value:
            return 'successful'
        if self.value == self.PENDING.value:
            return 'pending'
        if self.value == self.INVALID.value:
            return 'invalid'
        if self.value == self.FAILED.value:
            return 'failed'
        if self.value == self.REFUNDED.value:
            return 'refunded'
