import logging
import uuid

from paytmpg import MerchantProperty, LibraryConstants, EChannelId, Money, EnumCurrency, UserInfo, \
    PaymentDetailsBuilder, Payment, PaymentStatusDetailBuilder, PaymentMode
from paytmpg.merchant.models.PaymentStatusDetail import PaymentStatusDetail

from config import MERCHANT_ID, MERCHANT_KEY, WEBSITE, PRODUCTION, CALLBACK_URL
from payment_gateway.update_transaction_status.model import PaymentStatus

logger = logging.getLogger(__name__)

environment = LibraryConstants.PRODUCTION_ENVIRONMENT \
    if PRODUCTION else LibraryConstants.STAGING_ENVIRONMENT

MerchantProperty.initialize(environment, MERCHANT_ID, MERCHANT_KEY, '1', WEBSITE)
MerchantProperty.set_callback_url(CALLBACK_URL)
MerchantProperty.logger = logger


class PayTM:
    def initiate_transaction(self, customer_id, order_id, amount):
        channel_id = EChannelId.APP
        txn_amount = Money(EnumCurrency.INR, '%.2f' % amount)
        user_info = UserInfo()
        user_info.set_cust_id(customer_id)
        builder = PaymentDetailsBuilder(channel_id, order_id, txn_amount, user_info)
        payment_details = builder.build()
        response = Payment.createTxnToken(payment_details)
        logger.info(response)
        body = response.get_response_object().get_body()

        return body.txnToken, body.callbackUrl

    def payment_status(self, transaction_id):
        builder = PaymentStatusDetailBuilder(transaction_id)
        response = Payment.getPaymentStatus(PaymentStatusDetail(builder))
        logger.info(response)

        result_code = response.get_response_object().get_body().resultInfo.resultCode

        if result_code == '01':
            return PaymentStatus.SUCCESSFUL

        if result_code == '402' or result_code == '400':
            return PaymentStatus.PENDING

        return PaymentStatus.FAILED


if __name__ == '__main__':
    logging.basicConfig(level='DEBUG', format='%(levelname)s %(name)s %(asctime)s %(message)s')
    order_id = str(uuid.uuid4())
    paytm = PayTM()
    print(paytm.initiate_transaction(str(uuid.uuid4()), order_id, 1))
    print(paytm.payment_status(order_id))
