import psycopg2

import config
from util.exceptions import AuthenticationError, InvalidInput


class CustomerNotFound(Exception):
    pass


class PackageNotFound(Exception):
    pass


class DeliveryLocationNotFound(Exception):
    pass


def add_order(
        auth_func,
        phone_no,
        password,
        customer_id,
        package_id,
        date,
        delivery_location,
        quantity,
        price,
        debit,
        door_step_delivery,
):
    try:
        auth_func(phone_no, password)
    except:
        raise AuthenticationError

    if delivery_location != 0 and delivery_location != 1:
        raise DeliveryLocationNotFound

    if price < 1 or quantity < 1:
        raise InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select add_order from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_no,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise AuthenticationError

        cur.execute('select id from customer where id = %s', (customer_id,))
        if cur.rowcount == 0:
            raise CustomerNotFound

        cur.execute('select id from packages where id = %s', (package_id,))
        if cur.rowcount == 0:
            raise PackageNotFound

        cur.execute(
            'insert into deliverables(customer_id, package_id, quantity, '
            'delivery_location, date, price, status, door_step_delivery) '
            'values (%(cust_id)s, %(p_id)s, %(quantity)s, %(delivery_location)s, '
            '%(date)s, %(price)s, \'delivered\', %(door_step_delivery)s) '
            'on conflict (customer_id, package_id, date) '
            'do update set quantity = deliverables.quantity + %(quantity)s, '
            'delivery_location = %(delivery_location)s, '
            'price = deliverables.price + %(price)s, '
            'status = \'delivered\', '
            'door_step_delivery = %(door_step_delivery)s',
            {
                'cust_id': customer_id,
                'p_id': package_id,
                'date': date,
                'delivery_location': delivery_location,
                'quantity': quantity,
                'price': price,
                'door_step_delivery': door_step_delivery,
            }
        )

        cur.execute(
            'update package_quantity set quantity = quantity - 1 '
            'where package_id = %s and date = %s and quantity > 0',
            (package_id, date)
        )

        if not debit:
            conn.commit()
            return

        cur.execute(
            'update paymentdetail '
            'set temp_credit = temp_credit - %s '
            'where customer_id= %s',
            (price, customer_id)
        )

        conn.commit()
