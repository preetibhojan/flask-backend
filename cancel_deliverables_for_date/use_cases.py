import logging
from datetime import datetime, time

import psycopg2
from psycopg2.extras import execute_values

import config
from util.database import get_customer_id
from util.exceptions import AuthenticationError, InvalidInput

logger = logging.getLogger(__name__)


def cancel_deliverables_for_date_use_case(phone_number, password, auth_func, date, now):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    if date < now.date():
        raise InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)

        if date != now.date():
            now = datetime.combine(date, time())

        cur.execute(
            'with packages_that_can_be_ordered_now as ( '
            'select id from packages '
            'where meal_id in ( '
            'select id from meals '
            'where deliverytime - orderbefore > %(now)s) '
            ')'
            'update deliverables '
            'set status = \'cancelled\''
            'where customer_id = %(cust_id)s '
            'and status = \'ordered\''
            'and date = %(date)s '
            'and package_id in ('
            'select *'
            'from packages_that_can_be_ordered_now)'
            'returning package_id, quantity, date, price', {
                'cust_id': customer_id,
                'date': date,
                'now': now.time()
            }
        )
        logger.debug(cur.query)

        cancelled_deliverables = cur.fetchall()

        if len(cancelled_deliverables) > 0:
            credit = sum((map(lambda d: d[-1:][0], cancelled_deliverables)))
            cur.execute(
                'update paymentdetail '
                'set temp_credit = temp_credit + %s '
                'where customer_id = %s',
                (credit, customer_id)
            )
            logger.debug(cur.query)

            execute_values(
                cur,
                'update package_quantity pq '
                'set quantity = pq.quantity + data.quantity '
                'from (values %s) as data(package_id, quantity, date) '
                'where pq.package_id = data.package_id '
                'and pq.date = data.date',
                list(map(lambda d: d[:-1], cancelled_deliverables))
            )
            logger.debug(cur.query)

            cur.execute(
                'delete from cart where customer_id = %s '
                'and package_id in %s',
                (customer_id, tuple(map(lambda d: d[0], cancelled_deliverables)))
            )

            logger.debug(cur.query)
