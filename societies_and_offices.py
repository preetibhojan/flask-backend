from typing import List

import psycopg2

import config
from util.database import db_connection


class Society:
    tower_id: int
    name: str
    building: str
    flat_number: str

    def __init__(self, tower_id, name, building, flat_number):
        self.tower_id = tower_id
        self.name = name
        self.building = building
        self.flat_number = flat_number

    def serialize(self):
        return {
            'tower_id': self.tower_id,
            'name': self.name,
            'building': self.building,
            'flat_number': self.flat_number
        }


def get_societies() -> List[Society]:
    with db_connection() as conn, conn.cursor() as cur:
        cur.execute(
            'select t.id, t.name, s.building, s.flat_number from tower t '
            'inner join society s '
            'on s.tower_id = t.id '
            'and t.home is true'
        )

        return list(map(lambda t: Society(t[0], t[1], t[2], t[3]), cur.fetchall()))


class Office:
    company: str
    tower_id: int
    tower_name: str
    floor: int

    def __init__(self, company, tower_id, tower_name, floor):
        self.company = company
        self.tower_id = tower_id
        self.tower_name = tower_name
        self.floor = floor

    def serialize(self):
        return {
            'company': self.company,
            'tower_id': self.tower_id,
            'tower_name': self.tower_name,
            'floor': self.floor,
        }


def get_offices() -> List[Office]:
    with db_connection() as conn, conn.cursor() as cur:
        cur.execute(
            'select o.company, t.id, t.name, o.floor from tower t '
            'inner join office o '
            'on o.tower_id = t.id '
            'and t.home is false'
        )

        return list(map(lambda t: Office(t[0], t[1], t[2], t[3]), cur.fetchall()))
