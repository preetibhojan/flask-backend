import logging

import psycopg2
from psycopg2.extras import execute_values

import config
import util.database
from packages.repository import get_active_packages
from util.address import AddressRepository

logger = logging.getLogger(__name__)


class SaveScheduleRepository:
    def has_home_address(self, phone_number):
        return AddressRepository().has_home_address(phone_number)

    def has_office_address(self, phone_number):
        return AddressRepository().has_office_address(phone_number)

    def packages_with_id_in(self, package_ids):
        return list(filter(lambda p: p.id in package_ids, get_active_packages()))

    def cuisine_id_for(self, phone_number):
        with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
            cur.execute('select cuisine_id from customer where phone_number = %(pho_no)s', {
                'pho_no': phone_number
            })

            logger.debug(cur.query)

            return cur.fetchone()[0]

    def fill_cannot_order_on(self, packages):
        with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
            cur.execute(
                'select p.id, coo.day '
                'from packages p '
                'inner join meals m '
                'on m.id = p.meal_id '
                'and m.active is true '
                'and p.id in %s '
                'left outer join cannot_order_on coo '
                'on m.id = coo.meal_id',
                (tuple(map(lambda p: p.id, packages)),)
            )

            logger.debug(cur.query)

            rows = cur.fetchall()

            for p_id, cannot_order_on_day in rows:
                package = next(filter(lambda p: p.id == p_id, packages))
                package.cannot_order_on.append(cannot_order_on_day)

            return packages

    def update_schedule(self, phone_number, packages):
        with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
            customer_id = util.database.get_customer_id(cur, phone_number)

            packages_as_tuple_to_insert = []

            for package in packages:
                for delivery_info in package.delivery_info:
                    packages_as_tuple_to_insert.append(
                        (
                            customer_id,
                            package.id,
                            delivery_info.day,
                            delivery_info.delivery_location,
                            package.quantity,
                        )
                    )

            cur.execute(
                'delete from temp_tiffininformation where customer_id = %s',
                (customer_id,)
            )
            logger.debug(cur.query)

            execute_values(
                cur,
                'insert into temp_tiffininformation(customer_id, package_id, day, delivery_location, quantity) '
                'values %s',
                packages_as_tuple_to_insert,
            )

            logger.debug(cur.query)
