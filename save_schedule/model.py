from payment_info.models import DeliveryInfo
from util.collection import is_distinct
from util.exceptions import InvalidInput


class PackageToSchedule:
    """
    Represents information about package.

    id: Each package has a unique id

    delivery_info:
    """

    def __init__(self, id, delivery_info, cannot_order_on=None, quantity=1):
        if id is None or delivery_info is None or quantity is None:
            raise InvalidInput('Package information contains null')

        if cannot_order_on is None:
            self.cannot_order_on = []
        else:
            self.cannot_order_on = cannot_order_on

        self.id = id
        self.delivery_info = delivery_info
        self.quantity = quantity

    @staticmethod
    def from_json(json):
        temp = []

        for t in json['delivery_info']:
            temp.append(DeliveryInfo.from_json(t))

        return PackageToSchedule(json['package_id'], temp, quantity=json.get('quantity', 1))

    def is_valid(self):
        """
        Package is valid if all of the following are true:
        1. id is positive. Note: The presence of id needs to be checked in the database
        2. length of delivery_info is at least 1 and less than 7
        3. all delivery info are valid
        4. all delivery info have distinct days
        5. quantity is positive
        :return: boolean that says is the package valid
        """
        if self.id < 0:
            return False

        if self.quantity < 1:
            return False

        if len(self.delivery_info) < 1 or len(self.delivery_info) > 7:
            return False

        for _delivery_info in self.delivery_info:
            if not _delivery_info.is_valid():
                return False

        if not is_distinct(list(map(lambda d: d.day, self.delivery_info))):
            return False

        return True

    def __eq__(self, o: object) -> bool:
        return isinstance(o, PackageToSchedule) \
               and self.id == o.id \
               and self.delivery_info == o.delivery_info \
               and self.cannot_order_on == o.cannot_order_on

    def __repr__(self):
        return f'id: {self.id}, delivery_info: {self.delivery_info}, ' \
               f'cannot order on: {self.cannot_order_on}'
