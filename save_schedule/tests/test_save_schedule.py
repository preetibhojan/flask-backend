import unittest
from unittest.mock import Mock

from packages.repository import Package
from save_schedule.exceptions import NoPackageBookedForMoreThan3Days
from save_schedule.model import PackageToSchedule
from payment_info.models import DeliveryInfo
from save_schedule.use_case import save_schedule_use_case
from util.constants import OFFICE, HOME
from util.exceptions import AuthenticationError, InvalidInput


class SaveScheduleTestCase(unittest.TestCase):
    def test_save_schedule_raises_authentication_error_when_auth_func_throws(self):
        auth_func = Mock()
        auth_func.side_effect = Exception
        
        with self.assertRaises(AuthenticationError):
            save_schedule_use_case('', '', auth_func, None, None)
    
    def test_save_schedule_raises_invalid_input_when_schedule_is_none(self):
        auth_func = Mock()
        auth_func.return_value = None

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, None, None)

        auth_func.assert_called_once_with('', '')

    def test_save_schedule_raises_invalid_input_when_packages_to_book_is_none(self):
        auth_func = Mock()
        auth_func.return_value = None

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, None, None)

        auth_func.assert_called_once_with('', '')

    def test_save_schedule_raises_invalid_input_when_packages_to_book_is_empty(self):
        auth_func = Mock()
        auth_func.return_value = None

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, [], None)

        auth_func.assert_called_once_with('', '')

    def test_save_schedule_raises_invalid_input_when_package_to_book_is_invalid(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(0, HOME), DeliveryInfo(1, OFFICE)]),
        ]

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, None)

        auth_func.assert_called_once_with('', '')

    def test_save_schedule_raises_invalid_input_when_no_package_is_scheduled_for_more_than_3_days(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(2, HOME), DeliveryInfo(1, OFFICE)]),
        ]

        with self.assertRaises(NoPackageBookedForMoreThan3Days):
            save_schedule_use_case('', '', auth_func, packages, None)

        auth_func.assert_called_once_with('', '')

    def test_save_schedule_raises_invalid_input_when_package_ids_are_not_unique(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(1, [DeliveryInfo(2, HOME), DeliveryInfo(1, OFFICE)]),
        ]

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, None)

        auth_func.assert_called_once_with('', '')

    def test_save_schedule_raises_invalid_input_when_delivery_location_does_not_not_exist(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)]),
        ]

        repository = Mock()

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = False

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        auth_func.assert_called_once_with('', '')

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

    def test_save_schedule_raises_invalid_input_when_package_does_not_exist(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)]),
        ]

        repository = Mock()
        # Package does not exist if length of repository.packages != packages to book
        repository.packages_with_id_in.return_value = []

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2])

    def test_save_schedule_raises_invalid_input_when_two_or_more_meal_ids_are_same(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)]),
            PackageToSchedule(3, [DeliveryInfo(5, HOME), DeliveryInfo(1, OFFICE)]),
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 1, 4, '', None, None, False),
            Package(3, 0, '', '', 0, 1, 2, '', None, None, False),
            #
            # # Add ons
            # Package(4, 0, '', '', 0, 1, 1, '', None, None, True),
            # Package(5, 0, '', '', 0, 1, 1, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 3])

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

    def test_save_schedule_raises_invalid_input_when_length_of_normal_packages_is_0(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            # Add ons
            Package(4, 0, '', '', 0, 1, 1, '', None, None, False),
            Package(5, 0, '', '', 0, 1, 2, '', None, None, False),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([4, 5])

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

    def test_add_on_packages_meal_id_is_not_a_subset_of_packages_meal_id(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)]),
            PackageToSchedule(3, [DeliveryInfo(5, HOME), DeliveryInfo(1, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 1, 4, '', None, None, False),
            Package(3, 0, '', '', 0, 1, 3, '', None, None, False),

            # Add ons
            Package(4, 0, '', '', 0, 1, 1, '', None, None, True),
            Package(5, 0, '', '', 0, 1, 2, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 3, 4, 5])

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

    def test_quantity_of_add_on_per_meal_is_less_than_sum_of_quantity_of_packages_for_that_meal(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)]),
            PackageToSchedule(3, [DeliveryInfo(5, HOME), DeliveryInfo(1, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)], quantity=2),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)], quantity=1)
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 1, 4, '', None, None, False),
            Package(3, 0, '', '', 0, 1, 3, '', None, None, False),

            # Add ons
            Package(4, 0, '', '', 0, 1, 2, '', None, None, True),
            Package(5, 0, '', '', 0, 1, 4, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 3, 4, 5])

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

    def test_save_schedule_raises_invalid_input_when_cuisine_ids_of_normal_packages_are_different(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 2, 4, '', None, None, False),

            # Add ons
            Package(4, 0, '', '', 0, 1, 2, '', None, None, True),
            Package(5, 0, '', '', 0, 2, 2, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        repository.cuisine_id_for.return_value = 1

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 4, 5])

    def test_save_schedule_raises_invalid_input_when_cuisine_ids_of_add_ons_are_different(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 1, 4, '', None, None, False),

            # Add ons
            Package(4, 0, '', '', 0, 1, 2, '', None, None, True),
            Package(5, 0, '', '', 0, 2, 2, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        repository.cuisine_id_for.return_value = 1

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 4, 5])

    def test_raises_invalid_input_when_cuisine_ids_of_normal_packages_is_different_from_cuisine_id_for_customer(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 1, 4, '', None, None, False),

            # Add ons
            Package(4, 0, '', '', 0, 1, 2, '', None, None, True),
            Package(5, 0, '', '', 0, 1, 2, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        repository.cuisine_id_for.return_value = 2

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 4, 5])
        repository.cuisine_id_for.assert_called_once_with('')

    def test_raises_invalid_input_when_cuisine_ids_of_add_ons_is_different_from_cuisine_id_for_customer(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 2, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 2, 4, '', None, None, False),

            # Add ons
            Package(4, 0, '', '', 0, 2, 2, '', None, None, True),
            Package(5, 0, '', '', 0, 2, 2, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        repository.cuisine_id_for.return_value = 1

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 4, 5])
        repository.cuisine_id_for.assert_called_once_with('')

    def test_save_schedule_raises_invalid_input_when_a_package_is_ordered_on_a_day_it_cannot_be_ordered_on(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 1, 4, '', None, None, False),

            # Add ons
            Package(4, 0, '', '', 0, 1, 2, '', None, None, True),
            Package(5, 0, '', '', 0, 1, 2, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        repository.cuisine_id_for.return_value = 1

        repository.fill_cannot_order_on.return_value = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)], []),
            # this package cannot be ordered on
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)], [1, 4]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)], []),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)], [])
        ]

        with self.assertRaises(InvalidInput):
            save_schedule_use_case('', '', auth_func, packages, repository)

        repository.fill_cannot_order_on.assert_called_once_with(packages)

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 4, 5])

    def test_success(self):
        auth_func = Mock()
        auth_func.return_value = None

        packages = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)]),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)]),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)])
        ]

        repository = Mock()
        repository.packages_with_id_in.return_value = [
            Package(1, 0, '', '', 0, 1, 2, '', None, None, False),
            Package(2, 0, '', '', 0, 1, 4, '', None, None, False),

            # Add ons - their cuisine id is none
            Package(4, 0, '', '', 0, 1, 2, '', None, None, True),
            Package(5, 0, '', '', 0, 1, 2, '', None, None, True),
        ]

        repository.has_home_address.return_value = True
        repository.has_office_address.return_value = True

        repository.cuisine_id_for.return_value = 1

        packages_with_price_and_cannot_order_on = [
            PackageToSchedule(1, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE)], []),
            PackageToSchedule(2, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE), DeliveryInfo(2, OFFICE)], [1, 5]),

            # Add ons
            PackageToSchedule(4, [DeliveryInfo(1, HOME), DeliveryInfo(2, OFFICE), DeliveryInfo(3, OFFICE)], []),
            PackageToSchedule(5, [DeliveryInfo(3, HOME), DeliveryInfo(4, OFFICE)], [])
        ]

        repository.fill_cannot_order_on.return_value = packages_with_price_and_cannot_order_on

        save_schedule_use_case('', '', auth_func, packages, repository)

        repository.fill_cannot_order_on.assert_called_once_with(packages)

        repository.has_office_address.assert_called_once_with('')
        repository.has_home_address.assert_called_once_with('')

        auth_func.assert_called_once_with('', '')
        repository.packages_with_id_in.assert_called_once_with([1, 2, 4, 5])

        repository.update_schedule.assert_called_once_with('', packages_with_price_and_cannot_order_on)


if __name__ == '__main__':
    unittest.main()
