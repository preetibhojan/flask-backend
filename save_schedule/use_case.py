import logging

from save_schedule.exceptions import NoPackageBookedForMoreThan3Days
from util.collection import is_distinct, is_subset, consists_of_same_element
from util.constants import OFFICE, HOME
from util.exceptions import InvalidInput, AuthenticationError

logger = logging.getLogger(__name__)

_minimum_days_at_least_once_package_needs_to_be_scheduled = 3


def save_schedule_use_case(phone_number, password, auth_func, packages_to_schedule, repository):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.critical('Authentication error')
        raise AuthenticationError

    if not packages_to_schedule:
        logger.info('packages to schedule null or empty')
        raise InvalidInput

    for package in packages_to_schedule:
        # Note: package.is_valid returns false when multiple
        # delivery info have same days
        if not package.is_valid():
            logger.info('Package is not valid')
            raise InvalidInput

    max_delivery_info_count = max(list(map(lambda p: len(p.delivery_info), packages_to_schedule)))

    if max_delivery_info_count < _minimum_days_at_least_once_package_needs_to_be_scheduled:
        logger.info('no package scheduled for more than 3 days')
        raise NoPackageBookedForMoreThan3Days

    package_ids = list(map(lambda p: p.id, packages_to_schedule))

    if not is_distinct(package_ids):
        logger.info('package id not unique')
        raise InvalidInput

    if _customer_has_not_set_delivery_locations_they_are_scheduling_for(phone_number, packages_to_schedule, repository):
        logger.info('delivery locations requested is not a subset of address in database')
        raise InvalidInput

    packages = repository.packages_with_id_in(package_ids)

    package_does_not_exist = len(packages) != len(packages_to_schedule)

    if package_does_not_exist:
        logger.info('one or more package does not exist')
        raise InvalidInput

    normal_packages = list(filter(lambda p: p.add_on is False, packages))
    add_ons = list(filter(lambda p: p.add_on is True, packages))

    if len(normal_packages) == 0:
        raise InvalidInput

    normal_package_meal_ids = set(map(lambda p: p.meal_id, normal_packages))
    add_on_meal_ids = set(map(lambda p: p.meal_id, add_ons))

    if not add_on_meal_ids.issubset(normal_package_meal_ids):
        logger.info('add on meal ids not a subset of normal packages')
        raise InvalidInput

    for add_on_meal_id in add_on_meal_ids:
        normal_packages_with_given_meal_id = list(filter(lambda p: p.meal_id == add_on_meal_id, normal_packages))
        normal_package_ids = list(map(lambda p: p.id, normal_packages_with_given_meal_id))
        normal_package_count = sum(map(lambda p: p.quantity,
                                       filter(lambda p: p.id in normal_package_ids, packages_to_schedule)))

        add_on_packages_with_given_meal_id = list(filter(lambda p: p.meal_id == add_on_meal_id, add_ons))
        add_on_package_ids = list(map(lambda p: p.id, add_on_packages_with_given_meal_id))
        add_on_which_cannot_be_ordered = next(filter(lambda p: p.quantity > normal_package_count,
                                                     filter(lambda p: p.id in add_on_package_ids,
                                                            packages_to_schedule)), None)
        if add_on_which_cannot_be_ordered is not None:
            logger.info(f'{add_on_which_cannot_be_ordered.id} has more quantity than normal packages of same meal')
            raise InvalidInput

    cuisine_ids = list(map(lambda p: p.cuisine_id, packages))

    if not consists_of_same_element(cuisine_ids):
        logger.info('packages are of different cuisine id')
        raise InvalidInput

    if cuisine_ids[0] != repository.cuisine_id_for(phone_number):
        logger.info('cuisine id does not match cuisine id for customer')
        raise InvalidInput

    packages_to_schedule = repository.fill_cannot_order_on(packages_to_schedule)

    for package in packages_to_schedule:
        days = list(map(lambda d: d.day, package.delivery_info))

        for day_package_cannot_order_on in package.cannot_order_on:
            if day_package_cannot_order_on in days:
                logger.info('package ordered on a day it cannot be ordered on')
                raise InvalidInput

    repository.update_schedule(phone_number, packages_to_schedule)


def _customer_has_not_set_delivery_locations_they_are_scheduling_for(phone_number, packages, repository):
    delivery_locations_requested = []

    if repository.has_home_address(phone_number):
        logger.debug('customer has home address')
        delivery_locations_requested.append(HOME)

    if repository.has_office_address(phone_number):
        logger.debug('customer has office address')
        delivery_locations_requested.append(OFFICE)

    locations_to_deliver = set()

    for package in packages:
        for delivery_info in package.delivery_info:
            locations_to_deliver.add(delivery_info.delivery_location)

    return not is_subset(delivery_locations_requested, locations_to_deliver)
