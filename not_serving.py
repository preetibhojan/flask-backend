import psycopg2

import config


def not_serving_repository(current_date):
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select till_date, message, message2, message3 from not_serving '
            'where from_date <= %s '
            'and till_date >= %s',
            (current_date, current_date)
        )

        print(cur.query)

        rows = cur.fetchall()

        if len(rows) == 0:
            return {}

        return {
            'till': rows[0][0],
            'message': rows[0][1],
            'message2': rows[0][2],
            'message3': rows[0][3],
        }
