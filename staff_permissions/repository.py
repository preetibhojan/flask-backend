import psycopg2
import config


def permissions_for(phone_number):
    with psycopg2.connect(config.DATABASE_URL) as conn:
        with conn.cursor() as cur:
            cur.execute(
                'select home, add_quantity, set_menu, delivery, '
                'notifications, convert, add_order, cancel_order, update_payment_detail, notification_not_sent '
                'from staff_permissions '
                'where staff_id = (select id from staff where phone_number = %s)',
                (phone_number,)
            )

            result = cur.fetchone()

            if result is None:
                return None

            return {
                'home': result[0],
                'add_quantity': result[1],
                'set_menu': result[2],
                'delivery': result[3],
                'notifications': result[4],
                'convert': result[5],
                'add_order': result[6],
                'cancel_order': result[7],
                'update_payment_detail': result[8],
                'notifications_not_sent': result[9]
            }
