import psycopg2

import config
from util.exceptions import AuthenticationError


def get_credit_use_case(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select temp_credit, deposit '
            'from paymentdetail '
            'where customer_id = ('
            'select id '
            'from customer '
            'where phone_number = %s'
            ')',
            (phone_number,)
        )

        row = cur.fetchone()
        return float(row[0]), float(row[1])
