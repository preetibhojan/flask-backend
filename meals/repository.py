import logging

import psycopg2

import config
from util.datetime_util import time_to_seconds
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


class Meal:
    def __init__(self, id, name, priority, order_before, delivery_time, active, deposit, cannot_order_on=None, image_url=None):
        self.id = id
        self.name = name
        self.priority = priority
        self.order_before = order_before
        self.delivery_time = delivery_time
        self.active = active
        self.deposit = deposit
        self.image_url = image_url

        if cannot_order_on is None:
            self.cannot_order_on = []

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'priority': self.priority,
            'order_before': self.order_before.seconds,
            'delivery_time': time_to_seconds(self.delivery_time),
            'active': self.active,
            'deposit': self.deposit,
            'cannot_order_on': self.cannot_order_on,
            'image_url': self.image_url,
        }


def get_meals(phone_number, password, auth_func):
    """
    Returns all active meals

    :return: a list of meals
    """

    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select cuisine_id from Customer where phone_number = %s',
            (phone_number,)
        )

        logger.debug(cur.query)

        cuisine_id = cur.fetchone()[0]

        cur.execute(
            'select id, name, priority, orderbefore, '
            'deliverytime, active, deposit, image_url '
            'from meals '
            'inner join mealscuisines m '
            'on meals.id = m.meal_id '
            'where cuisine_id = %s '
            'and meals.active is true',
            (cuisine_id,)
        )

        logger.debug(cur.query)

        rows = cur.fetchall()

        meals = {}
        for meal_id, name, priority, orderBefore, deliveryTime, active, deposit, image_url in rows:
            meals[meal_id] = Meal(
                meal_id,
                name,
                priority,
                orderBefore,
                deliveryTime,
                active,
                deposit,
                image_url=image_url,
            )

        cur.execute('select meal_id, day from cannot_order_on')

        for meal_id, day in cur.fetchall():
            if meals.get(meal_id) is not None:
                meals[meal_id].cannot_order_on.append(day)

    return meals.values()


def get_meals_for_menu(phone_number, password, auth_func):
    """
    Returns all meals with their image_urls to display on the home page.

    :return: a list of meals
    """

    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select cuisine_id from Customer where phone_number = %s',
            (phone_number,)
        )

        logger.debug(cur.query)

        cuisine_id = cur.fetchone()[0]

        cur.execute(
            'select id, name, priority, orderbefore, '
            'deliverytime, active, deposit, image_url '
            'from meals '
            'inner join mealscuisines m '
            'on meals.id = m.meal_id '
            'where cuisine_id = %s ',
            (cuisine_id,)
        )

        logger.debug(cur.query)

        rows = cur.fetchall()

        meals = {}
        for meal_id, name, priority, orderBefore, deliveryTime, active, deposit, image_url in rows:
            meals[meal_id] = Meal(
                meal_id,
                name,
                priority,
                orderBefore,
                deliveryTime,
                active,
                deposit,
                image_url=image_url,
            )

        cur.execute('select meal_id, day from cannot_order_on')
        logger.debug(cur.query)

        for meal_id, day in cur.fetchall():
            if meals.get(meal_id) is not None:
                meals[meal_id].cannot_order_on.append(day)

        return meals.values()


def get_all_meals():
    """
    Returns all meals without image url
    """
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select id, name, priority, orderbefore, deliverytime, '
            'active, deposit from meals'
        )

        logger.debug(cur.query)

        rows = cur.fetchall()

        meals = {}
        for meal_id, name, priority, orderBefore, deliveryTime, active, deposit in rows:
            meals[meal_id] = Meal(
                meal_id,
                name,
                priority,
                orderBefore,
                deliveryTime,
                active,
                deposit,
            )

        cur.execute('select meal_id, day from cannot_order_on')

        for meal_id, day in cur.fetchall():
            meals[meal_id].cannot_order_on.append(day)

        return meals.values()
