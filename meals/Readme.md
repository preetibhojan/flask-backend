# Meals

It returns a list of meal objects. Each meal object contains id, name,
priority, cancel_before and cannot_order_on. ID is an int that uniquely 
identifies each meal. Name is a string denoting the name of the meal. 
Priority is used by the client to maintain the order of meals. 

`order_before` is a time duration in seconds before delivery time which 
a given meal can be cancelled for customers.

`cannot_order_on` is the day (between 1 and 6 both inclusive 1 
representing monday, 2 representing tuesday and so on till 6 
representing saturday). It was added to represent the fact that dinner and 
evening snacks are not served on saturday. 

`delivery_time` time in seconds at which the meal will start to be 
delivered.

Image url is the url of the the image to display for the given meal.
It depends on the cuisine. Hence phone number and password are 
required for authentication.  

For example, if user adds breakfast and lunch and removed breakfast 
and then adds it again, breakfast should still be above lunch. 

# Sample output
```json
[
    {
        "cannot_order_on": null,
        "delivery_time": 28800,
        "id": 1,
        "name": "Breakfast",
        "order_before": 14400,
        "priority": 0
    },
    {
        "cannot_order_on": null,
        "delivery_time": 43200,
        "id": 2,
        "name": "Lunch",
        "order_before": 14400,
        "priority": 1
    },
    {
        "cannot_order_on": 6,
        "delivery_time": 72000,
        "id": 3,
        "name": "Dinner",
        "order_before": 14400,
        "priority": 2
    }
]
```