from flask import Blueprint, render_template, request, send_from_directory

website = Blueprint('website', __name__)


@website.route('/web/<path:path>')
def privacy_policy(path):
    if path.startswith('assets/'):
        return send_from_directory('templates', path)

    return render_template(path, header=request.args.get('header', True))


@website.route('/<path:path>')
def webpage(path):
    return send_from_directory('website', path)


@website.route('/')
def index():
    return send_from_directory('website', 'index.html')
