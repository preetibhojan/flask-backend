import logging

import firebase_admin
import psycopg2
from firebase_admin.messaging import send_multicast, Notification, MulticastMessage

import config
from util.exceptions import AuthenticationError, UnauthorizedAccess, InvalidInput

logger = logging.getLogger(__name__)

firebase_admin.initialize_app()


def send_notification(phone_number, password, auth_func, customers, title, body, high_priority=True):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select notifications from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        if cur.fetchone()[0] is not True:
            logger.critical('unauthorised access to send notifications')
            raise UnauthorizedAccess

        cur.execute(
            'select fcm_token from customer '
            'where phone_number in %s',
            (tuple(customers),)
        )

        tokens = list(map(lambda x: x[0], cur.fetchall()))

        if len(tokens) == 0:
            return

        message = MulticastMessage(
            notification=Notification(
                title=title,
                body=body,
            ),
            tokens=tokens,
        )

        response = send_multicast(message)
        logger.info(f'messages to be sent: {len(tokens)}, messages sent: {response.success_count}')


def send_notification2(phone_number, password, auth_func,
                       customers, meal_id, title, body, add_to_notification_log,
                       today):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select notifications from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        if cur.fetchone()[0] is not True:
            logger.critical('unauthorised access to send notifications')
            raise UnauthorizedAccess

        cur.execute(
            'select id from meals where id = %s',
            (meal_id,)
        )

        if len(cur.fetchall()) != 1:
            raise InvalidInput

        cur.execute(
            'select id, fcm_token from customer '
            'where id in %s and fcm_token is not null',
            (tuple(customers),)
        )

        rows = cur.fetchall()
        sent_customers = list(map(lambda r: r[0], rows))
        tokens = list(map(lambda r: r[1], rows))

        if len(tokens) != 0:
            message = MulticastMessage(
                notification=Notification(
                    title=title,
                    body=body,
                ),
                tokens=tokens,
            )

            response = send_multicast(message)
            logger.info(f'messages to be sent: {len(tokens)}, messages sent: {response.success_count}')

        if add_to_notification_log:
            for customer_id in customers:
                cur.execute(
                    'insert into notification_history(customer_id, meal_id, date, title, body, sent) '
                    'values (%s, %s, %s, %s, %s, %s)',
                    (customer_id, meal_id, today, title, body, customer_id in sent_customers)
                )
