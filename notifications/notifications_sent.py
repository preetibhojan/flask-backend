import logging

import psycopg2

import config
from util.exceptions import AuthenticationError, UnauthorizedAccess

logger = logging.getLogger(__name__)


def notifications_sent(phone_number, password, auth_func, meal_id, date):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select notifications from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        if cur.fetchone()[0] is not True:
            logger.critical('unauthorised access to notifications sent')
            raise UnauthorizedAccess

        cur.execute(
            'select customer_id from notification_history '
            'where meal_id = %s and date = %s',
            (meal_id, date)
        )

        return list(map(lambda row: row[0], cur.fetchall()))


def notifications_sent2(phone_number, password, auth_func, meal_id, date):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select notifications from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        if cur.fetchone()[0] is not True:
            logger.critical('unauthorised access to notifications sent')
            raise UnauthorizedAccess

        cur.execute(
            'select customer_id, sent, sent_timestamp from notification_history '
            'where meal_id = %s and date = %s',
            (meal_id, date)
        )

        rows = cur.fetchall()
        sent = list(map(lambda r: {'customer_id': r[0]}, filter(lambda r: r[1], rows)))
        not_sent = list(map(lambda r: {'customer_id': r[0]}, filter(lambda r: not r[1], rows)))
        return {
            'sent': sent,
            'not_sent': not_sent
        }
