import logging

import psycopg2

import config
from util.database import get_customer_id
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


def get_home_address(phone_number):
    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute(
            'select flat_number, building, tower_id, ha.door_step_delivery, '
            't.name, l.locality, a.area, l.delivery_charge from homeaddress ha '
            'inner join tower t '
            'on t.id = ha.tower_id '
            'and t.active is true '
            'inner join localities l '
            'on l.id = t.locality_id '
            'inner join areas a '
            'on a.id = l.area_id '
            'where customer_id = %s',
            (customer_id,)
        )

        logger.debug(cur.query)

        row = cur.fetchone()

        if cur.rowcount == 0:
            return {
                'enabled': False
            }

        return {
            'flat_number': row[0],
            'building': row[1],
            'society_id': row[2],
            'door_step_delivery': row[3],
            'society': row[4],
            'locality': row[5],
            'area': row[6],
        }

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def get_home_address_use_case(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication failed')
        raise AuthenticationError

    return get_home_address(phone_number)
