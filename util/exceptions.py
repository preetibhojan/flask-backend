import json
from typing import Dict, Union

from werkzeug.exceptions import HTTPException


class AuthenticationError(Exception):
    """
    Returned when -
    1. Phone number is invalid or a customer with the phone number does not exist
    2. Phone number, password combination is invalid
    """
    pass


class InvalidInput(Exception):
    """
    Returned when -
    1. Request is not json
    2. A required field is null or empty
    """
    pass


class UnauthorizedAccess(Exception):
    """
    The person trying to perform the action does not have the privilege
    to do so
    """


class GeneralError(HTTPException):
    def __init__(self, code: int, description: Union[str, Dict]):
        self.code = code
        if description is str:
            self.description = description
        else:
            self.description = json.dumps(description)


BAD_REQUEST = 400
