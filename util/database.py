import contextlib
import logging
from datetime import timedelta

import psycopg2
from psycopg2.extras import execute_values
from psycopg2.pool import SimpleConnectionPool

import config
from meals.repository import get_all_meals

logger = logging.getLogger(__name__)

connection_pool = SimpleConnectionPool(1, 20, config.DATABASE_URL)


@contextlib.contextmanager
def db_connection():
    conn = connection_pool.getconn()
    try:
        yield conn
    finally:
        connection_pool.putconn(conn)


def get_customer_id(cur, phone_number):
    """
    Gets id of customer from repository with the given phone number
    :param cur: cursor to use
    :param phone_number: phone number of customer
    :return: customer id of customer
    """
    cur.execute('select id from Customer where phone_number = %(pho_no)s',
                {
                    'pho_no': phone_number
                })
    logger.debug(cur.query)
    customer_id = cur.fetchone()[0]
    return customer_id


def temp_credit(conn, phone_number):
    """
    Get temp_credit available for customer

    **Note:** Locks row in payment detail for update. to prevent concurrent updates.

    **Note:** Does not close the connection. call commit to commit or unlock to
    close the connection rolling back the changes

    :param conn: connection to use
    :param phone_number: phone number of customer
    :return: a float representing temp_credit for customer. Mey
    be 0
    """
    cur = None
    try:
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute('select temp_credit from paymentdetail where customer_id = %(cust_id)s for update', {
            'cust_id': customer_id
        })

        logger.debug(cur.query)

        return float(cur.fetchone()[0])
    finally:
        if cur is not None:
            cur.close()


def set_credit(conn, phone_number, credit):
    """
    Set temp credit to credit

    **Note:** Does not close the connection or commit the changes.
    Call unlock to close the connection and replace_deliverables to
    commit changes

    :param conn: connection to use
    :param phone_number: phone number of customer
    :param credit: credit to add to temp credit
    :return:
    """
    cur = None
    try:
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)
        logger.debug(f'setting temp credit for {customer_id} to {credit}')

        cur.execute('update paymentdetail '
                    'set temp_credit = %(credit)s '
                    'where customer_id = %(cust_id)s ', {
                        'credit': credit,
                        'cust_id': customer_id
                    })

        logger.debug(cur.query)

    finally:
        if cur is not None:
            cur.close()


def add_to_cart(conn, phone_number, deliverables):
    """
    Adds deliverables to cart for customer with phone number. On conflict, does nothing
    :param conn: conn to use
    :param phone_number: phone number of customer
    :param deliverables: deliverables to add to cart
    :return:
    """
    cur = None
    try:
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        rows = list(map(lambda d: (customer_id, d.package_id, d.quantity, d.delivery_location), deliverables))
        execute_values(cur,
                       'insert into cart (customer_id, package_id, quantity, delivery_location) '
                       'VALUES %s on conflict(customer_id, package_id) do nothing',
                       rows)

        logger.debug(cur.query)
    finally:
        if cur is not None:
            cur.close()


def remove_redundant_reservations(now, customer_id=None):
    """
    Remove reservations more than 10 minutes ago and adds the quantity
    back to package_quantity
    :return:
    """

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        delta = timedelta(minutes=10)
        cur.execute(
            'delete from reservations '
            'where %(now)s - created > %(delta)s '
            'or customer_id = %(cust_id)s '
            'returning package_id, quantity, date_to_reserve as date, extra_quantity, tower_id ',
            {
                'delta': delta,
                'now': now,
                'cust_id': customer_id,
            }
        )
        logger.debug(cur.query)
        reservations = cur.fetchall()

        if len(reservations) == 0:
            return

        extra_reservations = list(filter(lambda r: r[3] > 0, reservations))
        normal_reservations = list(filter(lambda r: r[1] - r[3] > 0, reservations))

        if normal_reservations:
            cur.execute(
                'update package_quantity '
                'set quantity = package_quantity.quantity + data.quantity '
                'from (values %s) as data(package_id, quantity, date, extra_quantity, tower_id) '
                'where package_quantity.package_id = data.package_id '
                'and package_quantity.date = data.date '
                'and data.quantity > 0 ',
                normal_reservations,
            )
            logger.debug(cur.query)

        if extra_reservations:
            cur.execute(
                'update extra_quantity '
                'set quantity = extra_quantity.quantity + data.extra_quantity '
                'from (values %s) as data(package_id, quantity, date, extra_quantity, tower_id) '
                'where extra_quantity.package_id = data.package_id '
                'and extra_quantity.date = data.date '
                'and data.quantity > 0 '
                'and extra_quantity.staff_id = ( '
                'select staff_id from notification_permission '
                'where tower_id = data.tower_id '
                'and staff_id != 1 '
                ')',
                extra_reservations,
            )
            logger.debug(cur.query)
        conn.commit()


def booking_started_for_tomorrow(time):
    """
    Returns if booking started for tomorrow based on time
    :param time: current time
    :return: True if booking time started. false otherwise
    """
    max_delivery_time = max(map(lambda m: m.delivery_time, get_all_meals()))

    return time > max_delivery_time


def delivery_charge_for_v2(cur, phone_number):
    customer_id = get_customer_id(cur, phone_number)

    cur.execute(
        'select id, door_step_delivery_delta '
        'from meals'
    )

    door_step_delivery_delta = {}
    for meal_id, delta in cur.fetchall():
        door_step_delivery_delta[meal_id] = delta

    cur.execute(
        'select s.delivery_charge, ha.door_step_delivery '
        'from homeaddress ha '
        'inner join society s '
        'on ha.tower_id = s.tower_id '
        'and ha._flat_number = s.building '
        'and ha._building = s.flat_number '
        'and ha.customer_id = %s',
        (customer_id,)
    )

    home_address_charge = {}
    for meal_id, _ in door_step_delivery_delta.items():
        home_address_charge[meal_id] = 0

    if cur.rowcount == 0:
        cur.execute(
            'select l.delivery_charge, ha.door_step_delivery '
            'from homeaddress ha '
            'inner join tower t '
            'on t.id = ha.tower_id '
            'and home is true '
            'inner join localities l '
            'on l.id = t.locality_id '
            'and l.home is true '
            'where customer_id = %s ',
            (customer_id,)
        )

    row = cur.fetchone()
    if row:
        door_step_delivery = row[1]

        if door_step_delivery:
            for meal_id, delta in door_step_delivery_delta.items():
                home_address_charge[meal_id] += row[0] + delta
        else:
            for meal_id, delta in door_step_delivery_delta.items():
                home_address_charge[meal_id] += row[0]

    cur.execute(
        'select o.delivery_charge '
        'from officeaddress oa '
        'inner join office o '
        'on oa._company = o.company '
        'and oa.tower_id = o.tower_id '
        'and oa._office_number = o.office_number '
        'and oa.customer_id = %s',
        (customer_id,)
    )

    office_address_charge = {}
    for meal_id, _ in door_step_delivery_delta.items():
        office_address_charge[meal_id] = 0

    if cur.rowcount == 0:
        cur.execute(
            'select l.delivery_charge '
            'from officeaddress oa '
            'inner join tower t '
            'on oa.tower_id = t.id '
            'and t.home is false '
            'inner join localities l '
            'on l.id = t.locality_id '
            'and l.office is true '
            'where customer_id = %s ',
            (customer_id,)
        )

    row = cur.fetchone()
    if row:
        for meal_id, _ in door_step_delivery_delta.items():
            office_address_charge[meal_id] = row[0]

    return home_address_charge, office_address_charge


if __name__ == '__main__':
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        print(delivery_charge_for_v2(cur, '8095093358'))
