import logging
from enum import Enum

from flask import request, jsonify

import authenticate.repository
from authenticate.use_case import authenticate_use_case
from util.database import db_connection
from util.exceptions import AuthenticationError
from util.password import is_correct


def respond_with_error(error_code, status=400):
    return {'error': error_code.value}, status, {'Content-Type': 'application/json'}


class ErrorCode(Enum):
    INVALID_INPUT = 0
    AUTHENTICATION_ERROR = 1


def auth_func(pho_no, password):
    return authenticate_use_case(pho_no, password, password_for)


logger = logging.getLogger(__name__)


def authenticated(f):
    def decorator(*args, **kwargs):
        if not request.json:
            return jsonify({'error': 0}), 400

        with db_connection() as conn, conn.cursor() as cur:
            phone_number = request.json['phone_number']
            password = request.json['password']
            cur.execute('SELECT id, password from Customer where phone_number = %(pho_no)s',
                        {'pho_no': phone_number})
            logger.debug(cur.query)

            row = cur.fetchone()

            if row is None or not is_correct(password, row[1]):
                raise AuthenticationError

            return f(row[0], *args, **kwargs)
    return decorator


password_for = authenticate.repository.password_for_customer
