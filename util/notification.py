import logging

import psycopg2
from firebase_admin.messaging import Message, Notification, send

import config

logger = logging.getLogger(__name__)


def send_notification(phone_number, title, body):
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('select fcm_token from customer where phone_number = %s', (phone_number,))
        token = cur.fetchone()[0]
        if not token:
            logger.debug(f'customer with phone number: {phone_number} does not have a fcm token')
            return

        message = Message(
            notification=Notification(
                title=title,
                body=body,
            ),
            token=token,
        )

        message_id = send(message)
        logger.info(f'message id: {message_id}')
