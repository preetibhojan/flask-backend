# Am I regular customer

Returns whether customer with a given phone number and password is
regular or not.

# Sample input
```json
{
	"phone_number": "0987654321",
	"password": "abcdefghi"
}
```

# Sample output
```json
{
  "result": true
}
```

If customer is regular customer, result is true. Otherwise, result is
false. 

# Errors
Returns a 400 bad request in case of error with an error code.

Error codes:

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authorization error             |
