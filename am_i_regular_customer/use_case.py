"""
Use case for checking if a customer is regular or not
"""

import logging

import psycopg2

import config
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


"""
Could have put is_regular_customer in a separate repository file but 
the function is too small already
"""


def is_regular_customer(phone_number):
    """
    Is customer is a regular customer?
    If isRegularCustomer column of Customer table is true, the customer is regular customer
    :param phone_number: phone_number of the customer
    :return: True if customer is regular. False otherwise
    """
    conn = None
    cur = None
    try:
        # conn = psycopg2.connect(host=config.DATABASE_URL,
        #                         database=config.DATABASE,
        #                         user=config.AM_I_REGULAR_CUSTOMER_USER,
        #                         password=config.AM_I_REGULAR_CUSTOMER_PASSWORD
        #                         )
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('SELECT isRegularCustomer from Customer where phone_number = %(pho_no)s',
                    {'pho_no': phone_number})
        row = cur.fetchone()

        result = row[0]

        logger.info('is regular customer: %s', result)
        return result
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def am_i_regular_customer_use_case(phone_number, password, auth_func, is_regular_customer_func):
    """
    Checks if the customer with the given phone number is regular customer or not
    :param phone_number: Phone number of the customer
    :param password: Password of the customer
    :param auth_func: A function that throws an Exception when either phone number is
    invalid or doesn't exist or phone number, password combination is invalid
    :param is_regular_customer_func: A function which when given the phone number checks
    the repository if the customer is regular or not
    :return: True if customer is regular. False otherwise
    """
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication failed')
        raise AuthenticationError

    return is_regular_customer_func(phone_number)
