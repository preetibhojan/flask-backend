# Change email

Change email of customer. Takes phone number and password as input
for authentication and an email parameter which is the new email of
the customer. 

# Sample input
```json
{
    "phone_number": "7020582873",
	"password": "abcdefghi",
    "email": "def@abc.com"
}
```

# Sample output
```json
{
    "success": true
}
```

# Error codes:
It returns a 400 Bad request in case of an error with and error code.

| Error code | Reason                          |
| :----------| :------------------------------:|
|  0         | Invalid input                   |
|  1         | Authentication error            |
|  2         | Invalid email                   |

## Invalid input
1. Request is not json
2. A required parameter is missing or null

## Authentication Error
Phone number password combo does not match or customer does not exist

## Invalid email
Email is not valid. 

