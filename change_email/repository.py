import logging

import psycopg2
from email_validator import validate_email, EmailNotValidError

import config
from change_email.exceptions import InvalidEmail
from util.exceptions import AuthenticationError

logger = logging.getLogger()


def change_email_use_case(phone_number, password, email, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication failed')
        raise AuthenticationError

    try:
        email = validate_email(email.strip()).email
    except EmailNotValidError:
        raise InvalidEmail

    conn = None
    cur = None

    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('update customer set email = %(email)s where phone_number = %(pho_no)s',
                    {
                        'email': email,
                        'pho_no': phone_number
                    })

        logger.debug(cur.query)

        conn.commit()

    finally:
        if conn is not None:
            conn.close()

        if cur is not None:
            cur.close()
