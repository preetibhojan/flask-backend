# Delivery locations

It returns the areas and localities food is served. Area 
consists of a name (string) and an id (int). And locality consists 
of id (int), locality (string) and area_id (int) which specifies to 
which area a given locality is related to. 

# Return type 
Sample output: 
```json
{
  "areas": [
    {
      "area": "Magarpatta", 
      "id": 1
    }, 
    {
      "area": "Hadapsar", 
      "id": 2
    }
  ],
  "localities": [
    {
      "area_id": 1, 
      "id": 1, 
      "locality": "Magarpatta 1"
    }, 
    {
      "area_id": 2, 
      "id": 3, 
      "locality": "Hadapsar 1"
    }
  ]
```