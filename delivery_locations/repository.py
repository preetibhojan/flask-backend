import psycopg2

import config
from delivery_locations.models import Area, Locality, Tower
from util.exceptions import AuthenticationError


def delivery_locations():
    """
    Active areas, localities, and towers
    :return: a dictionary of 'areas', 'localities' and 'towers' as keys
    """
    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute('select id, area from areas where active is true')

        rows = cur.fetchall()

        area_ids = tuple(map(lambda x: x[0], rows))
        areas = []
        for id, area in rows:
            areas.append(Area(id, area))

        cur.execute(
            'select id, locality, area_id, home, office '
            'from localities '
            'where active is true '
            'and area_id in %s',
            (area_ids,)
        )

        rows = cur.fetchall()

        locality_ids = tuple(map(lambda x: x[0], rows))
        localities = []
        for id, locality, area_id, home, office in rows:
            localities.append(Locality(id, locality, area_id, home, office))

        cur.execute(
            'select id, name, locality_id, home '
            'from tower '
            'where active is true '
            'and locality_id in %s',
            (locality_ids,)
        )

        towers = []
        for id, name, locality_id, home in cur.fetchall():
            towers.append(Tower(id, name, locality_id, home))

        locality_ids_used = set(map(lambda t: t.locality_id, towers))
        locality_ids_not_used = set(map(lambda l: l.id, localities)).difference(locality_ids_used)

        print(f'locality ids not used: {locality_ids_not_used}')

        for locality_id in locality_ids_not_used:
            for locality in filter(lambda l: l.id == locality_id, localities):
                print(f'unused locality: {locality}')
                localities.remove(locality)

        area_ids_used = set(map(lambda l: l.area_id, localities))
        area_ids_not_used = set(map(lambda a: a.id, areas)).difference(area_ids_used)

        print(f'area ids not used: {locality_ids_not_used}')

        for area_id in area_ids_not_used:
            for area in filter(lambda a: a.id == area_id, areas):
                print(f'unused area: {area}')
                areas.remove(area)

        return {
            'areas': areas,
            'localities': localities,
            'towers': towers
        }

    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def localities_for(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select id, locality '
            'from localities '
            'where staff_id = ('
            'select id '
            'from staff '
            'where phone_number = %s'
            ')',
            (phone_number,)
        )

        localities = []
        for id, locality in cur.fetchall():
            localities.append(Locality(id, locality, None, None, None))

        return localities


if __name__ == '__main__':
    areas_ = delivery_locations()
    print([area.serialize() for area in areas_['areas']])
    print([locality.serialize() for locality in areas_['localities']])
    print([towers.serialize() for towers in areas_['towers']])
