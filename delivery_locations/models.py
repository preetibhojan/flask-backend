class Area:
    def __init__(self, id, area):
        self.id = id
        self.area = area

    def serialize(self):
        return {
            'id': self.id,
            'area': self.area
        }

    def __str__(self):
        return str(self.serialize())


class Locality:
    def __init__(self, id, locality, area_id, home, office):
        self.id = id
        self.locality = locality
        self.area_id = area_id
        self.home = home
        self.office = office

    def serialize(self):
        return {
            'id': self.id,
            'locality': self.locality,
            'area_id': self.area_id,
            'home': self.home,
            'office': self.office,
        }

    def __str__(self):
        return str(self.serialize())


class Tower:
    def __init__(self, id, name, locality_id, home):
        self.id = id
        self.name = name
        self.locality_id = locality_id
        self.home = home

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'locality_id': self.locality_id,
            'home': self.home,
        }

    def __str__(self):
        return str(self.serialize())
