import psycopg2
from psycopg2.extras import execute_values

import config


class SetMenuRepository:
    def cuisine_ids_exist(self, menu_items):
        with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
            cuisine_ids = set(map(lambda m: m.cuisine_id, menu_items))
            cur.execute('select id from cuisines where id in %s', (tuple(cuisine_ids),))

            cuisine_ids_in_db = cur.fetchall()

            if not cuisine_ids_in_db:
                return False

            return len(cuisine_ids_in_db) == len(cuisine_ids)

    def meal_ids_exist(self, menu_items):
        with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
            meal_ids = set(map(lambda m: m.meal_id, menu_items))
            cur.execute('select id from meals where id in %s', (tuple(meal_ids),))

            meal_ids_in_db = cur.fetchall()

            if not meal_ids_in_db:
                return False

            return len(meal_ids_in_db) == len(meal_ids)

    def replace_menu(self, menu_items):
        with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
            cur.execute('delete from menu where date in %s',
                        (tuple(map(lambda m: m.date, menu_items)),))

            execute_values(
                cur,
                'insert into menu(meal_id, sabji, date, cuisine_id, daal, '
                'rice, sweet) values %s',
                list(map(lambda m: (m.meal_id, m.sabji, m.date, m.cuisine_id,
                                    m.daal, m.rice, m.sweet),
                         menu_items)
                     )
            )
            conn.commit()

    def is_authorized(self, phone_number):
        with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
            cur.execute(
                'select set_menu from staff_permissions '
                'where staff_id = (select id from staff where phone_number = %s)',
                (phone_number,)
            )

            return cur.fetchone()[0]
