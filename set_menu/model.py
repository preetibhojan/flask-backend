from datetime import date


class MenuItem:
    def __init__(self, meal_id, cuisine_id, _date, sabji, daal, rice, sweet):
        self.meal_id = meal_id
        self.cuisine_id = cuisine_id
        self.date = _date
        self.sabji = sabji
        self.daal = daal
        self.rice = rice
        self.sweet = sweet

    @staticmethod
    def from_json(json):
        return MenuItem(
            json['meal_id'],
            json['cuisine_id'],
            date.fromisoformat(json['date']),
            json['sabji'],
            json.get('daal', None),
            json.get('rice', None),
            json.get('sweet', None)
        )

    def is_valid(self):
        """
        MenuITem is valid if:
        1. Meal ID, cuisine ID, and Date are not null and
        2. At least one of sabji, daal, rice and sweet is not None or ''
        :return: True if menu item is valid. False otherwise
        """
        _menu = [self.sabji, self.daal, self.rice, self.sweet]
        return self.meal_id is not None \
               and self.cuisine_id is not None \
               and self.date is not None \
               and _menu.count(None) == 0 \
               and _menu.count('') < 4