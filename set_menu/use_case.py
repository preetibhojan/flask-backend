import logging

import util.exceptions
from util.collection import is_distinct


def _any_date_is_before_today(menu_items, today):
    return next(filter(lambda m: m.date < today, menu_items), None) is not None


def _cuisine_id_meal_id_date_combo_is_unique(menu_items):
    cuisine_id_meal_id_tuples = list(map(lambda m: (m.cuisine_id, m.meal_id, m.date),
                                         menu_items))

    return is_distinct(cuisine_id_meal_id_tuples)


logger = logging.getLogger(__name__)


def set_menu_use_case(phone_number, password, auth_func, menu_items, today, repository):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('authentication failed')
        raise util.exceptions.AuthenticationError

    if not menu_items:
        logger.info('length of menu items is 0')
        raise util.exceptions.InvalidInput

    for item in menu_items:
        if not item.is_valid():
            logger.info('menu item not valid')
            raise util.exceptions.InvalidInput

    if _any_date_is_before_today(menu_items, today):
        logger.info('a date is before tomorrow')
        raise util.exceptions.InvalidInput

    if not _cuisine_id_meal_id_date_combo_is_unique(menu_items):
        logger.info('cuisine id, meal id, date combination is not unique')
        raise util.exceptions.InvalidInput

    if not repository.is_authorized(phone_number):
        logger.info('Not an admin')
        raise util.exceptions.UnauthorizedAccess

    if not repository.cuisine_ids_exist(menu_items):
        logger.info('meal id does not exist')
        raise util.exceptions.InvalidInput

    if not repository.meal_ids_exist(menu_items):
        logger.info('meal id does not exist')
        raise util.exceptions.InvalidInput

    repository.replace_menu(menu_items)
