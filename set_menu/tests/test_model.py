import unittest
from datetime import date

from set_menu.model import MenuItem


class MenuItemTestCase(unittest.TestCase):
    def test_is_valid_returns_false_when_meal_id_is_none(self):
        item = MenuItem(None, 1, date.today(), 'welify', 'weufty', 'wefut', 'wefut')
        self.assertFalse(item.is_valid())

    def test_is_valid_returns_false_when_cuisine_id_is_none(self):
        item = MenuItem(1, None, date.today(), 'welify', 'weufty', 'wefut', 'wefut')
        self.assertFalse(item.is_valid())

    def test_is_valid_returns_false_when_sabji_is_none(self):
        item = MenuItem(1, 1, date.today(), None, 'weufty', 'wefut', 'wefut')
        self.assertFalse(item.is_valid())

    def test_is_valid_returns_false_when_daal_is_none(self):
        item = MenuItem(1, 1, date.today(), 'weutf', None, 'wefut', 'wefut')
        self.assertFalse(item.is_valid())

    def test_is_valid_returns_false_when_rice_is_none(self):
        item = MenuItem(1, 1, date.today(), 'wsidf', 'weufty', None, 'wefut')
        self.assertFalse(item.is_valid())

    def test_is_valid_returns_false_when_sweet_is_none(self):
        item = MenuItem(1, 1, date.today(), 'None', 'weufty', 'wefut', None)
        self.assertFalse(item.is_valid())

    def test_is_valid_returns_false_when_all_of_sabji_daal_rice_and_sweet_are_empty(self):
        item = MenuItem(1, 1, date.today(), '', '', '', '')
        self.assertFalse(item.is_valid())

    def test_success_when_some_items_are_not_specified(self):
        item = MenuItem(1, 1, date.today(), '', '', 'welfih', 'eliwfh')
        self.assertTrue(item.is_valid())

    def test_success(self):
        item = MenuItem(1, 1, date.today(), 'None', 'weufty', 'wefut', 'wdify')
        self.assertTrue(item.is_valid())


if __name__ == '__main__':
    unittest.main()
