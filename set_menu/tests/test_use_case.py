import unittest
from datetime import datetime, timedelta
from unittest.mock import Mock

from set_menu.model import MenuItem
from set_menu.use_case import set_menu_use_case
from util.exceptions import AuthenticationError, InvalidInput, UnauthorizedAccess


class SetMenuTestCase(unittest.TestCase):
    def test_set_menu_raises_authentication_error_when_auth_func_raises_exception(self):
        auth_func = Mock()
        auth_func.side_effect = Exception

        with self.assertRaises(AuthenticationError):
            set_menu_use_case('', '', auth_func, [], datetime.now(), Mock())

        auth_func.assert_called_once_with('', '')

    def test_set_menu_raises_invalid_input_when_menu_items_is_empty(self):
        auth_func = Mock()
        auth_func.return_value = None

        with self.assertRaises(InvalidInput):
            set_menu_use_case('', '', auth_func, [], datetime.now(), Mock())

        auth_func.assert_called_once_with('', '')

    def test_set_menu_raises_invalid_input_when_menu_items_is_none(self):
        auth_func = Mock()
        auth_func.return_value = None

        with self.assertRaises(InvalidInput):
            set_menu_use_case('', '', auth_func, None, datetime.now(), Mock())

        auth_func.assert_called_once_with('', '')

    def test_set_menu_raises_invalid_input_when_menu_item_is_invalid(self):
        auth_func = Mock()
        auth_func.return_value = None

        menu_items = [
            MenuItem(1, 12, '', '', '', '', ''),
            MenuItem(None, 1, '', '', '', '', '')
        ]

        with self.assertRaises(InvalidInput):
            set_menu_use_case('', '', auth_func, menu_items, datetime.now(), Mock())

        auth_func.assert_called_once_with('', '')

    def test_set_menu_raises_invalid_input_when_any_date_is_before_tomorrow(self):
        auth_func = Mock()
        auth_func.return_value = None

        now = datetime.now()
        tomorrow = now.date() + timedelta(days=1)

        menu_items = [
            MenuItem(1, 12, tomorrow, 'eowfiy', '', '', ''),
            MenuItem(1, 1, now.date(), 'efoy8ner', '', '', '')
        ]

        with self.assertRaises(InvalidInput):
            set_menu_use_case('', '', auth_func, menu_items, tomorrow, Mock())

        auth_func.assert_called_once_with('', '')

    def test_set_menu_raises_invalid_input_when_cuisine_id_meal_id_date_combo_is_not_unique(self):
        auth_func = Mock()
        auth_func.return_value = None

        now = datetime.now()
        tomorrow = now.date() + timedelta(days=1)

        menu_items = [
            MenuItem(1, 12, tomorrow, 'eowfiy', '', '', ''),
            MenuItem(1, 12, tomorrow, 'weekujft', '', '', ''),
        ]

        with self.assertRaises(InvalidInput):
            set_menu_use_case('', '', auth_func, menu_items, tomorrow, Mock())

        auth_func.assert_called_once_with('', '')

    def test_set_menu_raises_unauthorized_access_if_staff_is_not_authorized(self):
        auth_func = Mock()
        auth_func.return_value = None

        now = datetime.now()
        tomorrow = now.date() + timedelta(days=1)

        repository = Mock()
        repository.is_authorized.return_value = False

        menu_items = [
            MenuItem(1, 12, tomorrow, 'eowfiy', '', '', ''),
            MenuItem(1, 1, tomorrow, 'efoy8ner', '', '', '')
        ]

        with self.assertRaises(UnauthorizedAccess):
            set_menu_use_case('', '', auth_func, menu_items, tomorrow, repository)

        auth_func.assert_called_once_with('', '')
        repository.is_authorized.assert_called_once_with('')

    def test_set_menu_raises_invalid_input_if_cuisine_ids_does_not_exist(self):
        auth_func = Mock()
        auth_func.return_value = None

        now = datetime.now()
        tomorrow = now.date() + timedelta(days=1)

        menu_items = [
            MenuItem(1, 12, tomorrow, 'eowfiy', '', '', ''),
            MenuItem(1, 1, tomorrow, 'efoy8ner', '', '', '')
        ]

        repository = Mock()
        repository.is_authorized.return_value = True
        repository.cuisine_ids_exist.return_value = False

        with self.assertRaises(InvalidInput):
            set_menu_use_case('', '', auth_func, menu_items, tomorrow, repository)

        auth_func.assert_called_once_with('', '')
        repository.is_authorized.assert_called_once_with('')
        repository.cuisine_ids_exist.assert_called_once_with(menu_items)

    def test_set_menu_raises_invalid_input_if_meal_ids_does_not_exist(self):
        auth_func = Mock()
        auth_func.return_value = None

        now = datetime.now()
        tomorrow = now.date() + timedelta(days=1)

        menu_items = [
            MenuItem(1, 12, tomorrow, 'eowfiy', '', '', ''),
            MenuItem(1, 1, tomorrow, 'efoy8ner', '', '', '')
        ]

        repository = Mock()
        repository.is_authorized.return_value = True
        repository.cuisine_ids_exist.return_value = True
        repository.meal_ids_exist.return_value = False

        with self.assertRaises(InvalidInput):
            set_menu_use_case('', '', auth_func, menu_items, tomorrow, repository)

        auth_func.assert_called_once_with('', '')
        repository.is_authorized.assert_called_once_with('')
        repository.cuisine_ids_exist.assert_called_once_with(menu_items)
        repository.meal_ids_exist.assert_called_once_with(menu_items)

    def test_success(self):
        auth_func = Mock()
        auth_func.return_value = None

        now = datetime.now()
        tomorrow = now.date() + timedelta(days=1)

        menu_items = [
            MenuItem(1, 12, tomorrow, 'eowfiy', '', '', ''),
            MenuItem(1, 1, tomorrow, 'efoy8ner', '', '', '')
        ]

        repository = Mock()
        repository.is_authorized.return_value = True
        repository.cuisine_ids_exist.return_value = True
        repository.meal_ids_exist.return_value = True

        set_menu_use_case('', '', auth_func, menu_items, tomorrow, repository)

        auth_func.assert_called_once_with('', '')
        repository.is_authorized.assert_called_once_with('')
        repository.cuisine_ids_exist.assert_called_once_with(menu_items)
        repository.meal_ids_exist.assert_called_once_with(menu_items)
        repository.replace_menu.assert_called_once_with(menu_items)


if __name__ == '__main__':
    unittest.main()
