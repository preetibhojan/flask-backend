import psycopg2

import config
from util.exceptions import AuthenticationError, InvalidInput


def cancel_order(auth_func, phone_number, password, customer_id, package_id, date, quantity, price, credit=True):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    if quantity < 1:
        raise InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select cancel_order from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise AuthenticationError

        cur.execute('select id from customer where id = %s', (customer_id,))
        if cur.rowcount == 0:
            raise InvalidInput

        cur.execute('select id from packages where id = %s', (package_id,))
        if cur.rowcount == 0:
            raise InvalidInput

        cur.execute(
            'update deliverables '
            'set quantity = quantity - %(quantity)s, '
            'price = price - %(price)s '
            'where customer_id = %(cust_id)s '
            'and package_id = %(p_id)s '
            'and date = %(date)s '
            'returning quantity',
            {
                'quantity': quantity,
                'price': price,
                'p_id': package_id,
                'date': date,
                'cust_id': customer_id,
            }
        )

        if cur.rowcount == 0:
            raise InvalidInput

        if cur.fetchone()[0] == 0:
            cur.execute(
                'delete from deliverables '
                'where customer_id = %s '
                'and package_id = %s '
                'and date = %s',
                (customer_id, package_id, date)
            )

        if not credit:
            conn.commit()
            return

        cur.execute(
            'update paymentdetail '
            'set temp_credit = temp_credit + %s '
            'where customer_id = %s',
            (price, customer_id)
        )

        conn.commit()
