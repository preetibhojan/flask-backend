import psycopg2

import config


def door_step_delivery():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute('select max(door_step_delivery_delta) from meals')
        return cur.fetchone()[0]
