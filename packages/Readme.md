# Packages

Not to be confused with package (or module) of any language you 
probably know. A package is the different combinations of item that can
be ordered. For example two packages may differ in the quantity of food
or maybe some packages have extra dishes. 

A Package has an id, name, description, price, cuisine_id, meal_id and
a group. Group is used to group packages when displaying them to user. 
**Each package is uniquely identified by its id**. Name and 
description are human readable text/string and for the UI to display. 
Price is an integer. Cuisine_id is the id of the cuisine this package 
is associated with and meal_id is the id of the meal this package is
associated with.  

Meals cannot share same packages (even if everything is identical, 
the id will be different). Every meal will have at least one package 
associated with it.

**Note:** 
1. This end point only returns regular packages i.e. packages that
are usually available. It is meant to be used with schedule and
the likes.

2. Quantity is always null  

# Sample output 
```json
[
    {
        "cuisine_id": 2,
        "description": "Gujarat dinner 3",
        "group": "Package With 3 Roti",
        "id": 18,
        "meal_id": 3,
        "name": "Package 3",
        "price": 70,
        "quantity": null
    },
    {
        "cuisine_id": 1,
        "description": "Gujarati breakfast description 2",
        "group": "Breakfast",
        "id": 26,
        "meal_id": 1,
        "name": "Gujarati Breakfast 2",
        "price": 25,
        "quantity": null
    },
    {
        "cuisine_id": 2,
        "description": "Maharashtrian breakfast description 2",
        "group": "Breakfast",
        "id": 27,
        "meal_id": 1,
        "name": "Maharashtrian Breakfast 2",
        "price": 25,
        "quantity": null
    }
]
```

# Packages for menu

Returns package details mentioned above for packages that can be ordered
right now i.e. packages with a non null quantity and packages that belong
with meals that can be ordered at the time of calling the API. These 
packages are not guaranteed to be a subset of packages returned by 
`/packages` because they include non regular packages. 

# Sample output
```json
[
    {
        "cuisine_id": 2,
        "description": "Gujarat dinner 3",
        "group": "Package With 3 Roti",
        "id": 18,
        "meal_id": 3,
        "name": "Package 3",
        "price": 70,
        "quantity": 1
    },
    {
        "cuisine_id": 1,
        "description": "Gujarati breakfast description 2",
        "group": "Breakfast",
        "id": 26,
        "meal_id": 1,
        "name": "Gujarati Breakfast 2",
        "price": 25,
        "quantity": 2
    },
    {
        "cuisine_id": 2,
        "description": "Maharashtrian breakfast description 2",
        "group": "Breakfast",
        "id": 27,
        "meal_id": 1,
        "name": "Maharashtrian Breakfast 2",
        "price": 25,
        "quantity": 3
    }
]
```