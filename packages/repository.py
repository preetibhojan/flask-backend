import logging

import psycopg2

import config
import util.database
import util.exceptions
from util.datetime_util import next_working_day

logger = logging.getLogger(__name__)


class Package:
    def __init__(self, id, quantity, name, description, price, cuisine_id, meal_id, group, date, complimentary, add_on,
                 group_id, delivery_time=None, display_name=None):
        self.id = id
        self.quantity = quantity
        self.name = name
        self.description = description
        self.price = price
        self.cuisine_id = cuisine_id
        self.meal_id = meal_id
        self.group = group
        self.group_id = group_id
        self.date = date
        self.complimentary = complimentary
        self.add_on = add_on
        self.delivery_time = delivery_time
        self.display_name = display_name

    def serialize(self):
        if self.id is not None:
            return {
                'id': self.id,
                'name': self.name,
                'description': self.description,
                'price': self.price,
                'cuisine_id': self.cuisine_id,
                'meal_id': self.meal_id,
                'group': self.group,
                'quantity': self.quantity,
                'date': self.date,
                'complimentary': self.complimentary,
                'add_on': self.add_on,
                'display_name': self.display_name,
                'group_id': self.group_id,
            }

    def __str__(self):
        return str(self.serialize())

    def __format__(self, format_spec):
        return self.__str__()


def get_packages_v2(phone_number, password, auth_func):
    """
    Gets regular packages for the cuisine id chosen by customer
    :param phone_number: phone number
    :param password: password
    :param auth_func: a function which throws on authentication failure
    :return: a list of Package
    """
    try:
        auth_func(phone_number, password)
    except Exception:
        raise util.exceptions.InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select p.id, p.name, description, price, cuisine_id, '
            'meal_id, p.group_id, pg.name, complimentary, add_on, display_name '
            'from packages p '
            'inner join package_group pg on p.group_id = pg.id '
            'where not conditional '
            'and (cuisine_id = ('
            'select cuisine_id '
            'from customer '
            'where phone_number = %s'
            ') or cuisine_id is null)',
            (phone_number,)
        )

        logger.debug(cur.query)

        return get_rows(cur)


def get_rows(cur):
    rows = cur.fetchall()
    packages = []
    for package_id, name, description, price, cuisine_id, meal_id, group_id, group, complimentary, add_on, display_name in rows:
        packages.append(Package(package_id, None, name, description, price,
                                cuisine_id, meal_id, group, None, complimentary, add_on, group_id,
                                display_name=display_name))
    return packages


def get_active_packages():
    """
    All packages that can be ordered regularly and the meals of which are active.
    Some packages can only be ordered on some days. Those will be
    returned by get_packages_for_menu
    :return: a list of package
    """
    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        cur.execute(
            'select p.id, p.name, description, price, cuisine_id, '
            'meal_id, p.group_id, pg.name, complimentary, add_on, display_name '
            'from packages p '
            'inner join package_group pg on p.group_id = pg.id '
            'inner join meals m '
            'on p.meal_id = m.id '
            'and m.active is true '
            'where not conditional'
        )

        logger.debug(cur.query)

        return get_rows(cur)
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def get_packages_for_menu(phone_number, password, auth_func, now):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise util.exceptions.AuthenticationError

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = util.database.get_customer_id(cur, phone_number)

        util.database.remove_redundant_reservations(now)

        cur.execute(
            'select p.id, pq.quantity + coalesce(d.quantity, 0), '
            'p.name, description, p.price, cuisine_id, meal_id, '
            'p.group_id, pg.name, pq.date, complimentary, add_on '
            'from packages p '
            'inner join package_group pg on p.group_id = pg.id '
            'inner join package_quantity pq '
            'on p.id = pq.package_id '
            'and (p.cuisine_id = ('
            'select cuisine_id '
            'from customer '
            'where id = %(cust_id)s'
            ') or p.cuisine_id is null)'
            'inner join meals m '
            'on p.meal_id = m.id '
            'left outer join deliverables d '
            'on p.id = d.package_id '
            'and customer_id = %(cust_id)s '
            'and d.date = pq.date '
            'and status = \'ordered\' '
            'where pq.quantity + coalesce(d.quantity, 0) > 0 and '
            '((pq.date = %(now)s and deliverytime - orderbefore > %(time)s) '
            'or pq.date = %(next_working_day)s)',
            {
                'now': now.date(),
                'next_working_day': next_working_day(now).date(),
                'time': now.time(),
                'cust_id': customer_id,
            })

        logger.debug(cur.query)

        rows = cur.fetchall()

        packages = []
        for package_id, quantity, name, description, price, cuisine_id, meal_id, group_id, group, date, complimentary, add_on \
                in rows:
            packages.append(
                Package(package_id, quantity, name, description, price,
                        cuisine_id, meal_id, group, date, complimentary, add_on, group_id))

        return packages
    finally:
        if conn is not None:
            conn.close()
        if cur is not None:
            cur.close()


def get_packages_for_menu2(customer_id, now, home):
    with util.database.db_connection() as conn, conn.cursor() as cur:
        util.database.remove_redundant_reservations(now)

        if home:
            cur.execute(
                'select tower_id from homeaddress where customer_id = %s',
                (customer_id,)
            )
        else:
            cur.execute(
                'select tower_id from officeaddress where customer_id = %s',
                (customer_id,)
            )

        if cur.rowcount == 0:
            raise util.exceptions.GeneralError(util.exceptions.BAD_REQUEST, {'error': 'address not found'})

        tower = cur.fetchone()[0]

        cur.execute(
            'select p.id, coalesce(pq.quantity, 0) + coalesce(eq.quantity, 0), '
            'p.name, description, p.price, cuisine_id, meal_id, p.group_id, pg.name, ' 
            'coalesce(pq.date, eq.date), complimentary, add_on '
            'from package_quantity pq '
            'full outer join ( '
            'select package_id, quantity, date '
            'from extra_quantity '
            'where date = %(now)s '
            'and quantity > 0 '
            'and staff_id = ( '
            'select id '
            'from staff '
            'inner join notification_permission np '
            'on staff.id = np.staff_id '
            'and np.tower_id = %(tower)s '
            'and staff.id != 1) '
            ') eq '
            'on pq.package_id = eq.package_id '
            'and pq.date = eq.date '
            'inner join packages p '
            'on pq.package_id = p.id '
            'or eq.package_id = p.id '
            'inner join meals m on p.meal_id = m.id '
            'inner join package_group pg on p.group_id = pg.id '
            'where coalesce(pq.quantity, 0) + coalesce(eq.quantity, 0) > 0 '
            'and ( '
            '(coalesce(pq.date, eq.date) = %(next_working_day)s) '
            'or (coalesce(pq.date, eq.date) = %(now)s '
            'and deliverytime - orderbefore > %(time)s) '
            ')',
            {
                'now': now.date(),
                'next_working_day': next_working_day(now).date(),
                'time': now.time(),
                'tower': tower,
            })

        logger.debug(cur.query)

        rows = cur.fetchall()

        packages = []
        for package_id, quantity, name, description, price, cuisine_id, meal_id, group_id, group, date, complimentary, add_on \
                in rows:
            packages.append(
                Package(package_id, quantity, name, description, price,
                        cuisine_id, meal_id, group, date, complimentary, add_on, group_id))

        return packages


def get_packages_for_menu_for_admin(phone_number, password, auth_func, now):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise util.exceptions.AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select add_quantity from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        row = cur.fetchone()
        if row is None or row[0] is False:
            raise util.exceptions.UnauthorizedAccess

        cur.execute('select p.id, pq.quantity, p.name, description, p.price, '
                    'cuisine_id, meal_id, p.group_id, pg.name, pq.date, add_on '
                    'from packages p '
                    'inner join package_group pg on p.group_id = pg.id '
                    'inner join package_quantity pq '
                    'on p.id = pq.package_id '
                    'and pq.date >= %s',
                    (now.date(),)
                    )

        packages = []

        for package_id, quantity, name, description, price, cuisine_id, meal_id, group_id, group, date, add_on in cur.fetchall():
            packages.append(
                Package(package_id, quantity, name, description, price, cuisine_id, meal_id, group, date, None, add_on, group_id))

        return packages


def get_all_packages():
    """
    Returns all packages
    :return: a list of package
    """

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select p.id, p.name, description, price, cuisine_id, '
            'meal_id, p.group_id, pg.name, complimentary, add_on, display_name '
            'from packages p '
            'inner join package_group pg on p.group_id = pg.id '
        )

        logger.debug(cur.query)

        return get_rows(cur)


def get_groups():
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select id, name, image_url from package_group'
        )
        logger.debug(cur.query)

        result = []
        for id, name, image_url in cur.fetchall():
            result.append({
                'id': id,
                'name': name,
                'image_url': image_url
            })

        return result
