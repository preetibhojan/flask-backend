import logging

import psycopg2

import config
from deliverables.model import Deliverable
from util.database import get_customer_id
from util.exceptions import AuthenticationError

logger = logging.getLogger(__name__)


def fetched_rows(cur):
    deliverables = []
    rows = cur.fetchall()
    for package_id, quantity, delivery_location, price, date, status, meal_id, add_on in rows:
        deliverables.append(
            Deliverable(
                package_id,
                quantity,
                delivery_location,
                price,
                date,
                status,
                meal_id=meal_id,
                add_on=add_on
            )
        )

    return deliverables


def get_all_deliverables(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute(
            'select package_id, quantity, delivery_location, d.price, date, status, meal_id, add_on '
            'from deliverables d '
            'inner join packages p on d.package_id = p.id '
            'where customer_id = %(cust_id)s '
            'and status != \'cancelled\'',
            {
                'cust_id': customer_id,
            })

        logger.debug(cur.query)

        return fetched_rows(cur)
    finally:
        if conn is not None:
            conn.close()

        if cur is not None:
            cur.close()


def deliverables_from_today(phone_number, password, auth_func, now):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute(
            'select package_id, quantity, delivery_location, d.price, date, status, meal_id, add_on '
            'from deliverables d '
            'inner join packages p on d.package_id = p.id '
            'where customer_id = %(cust_id)s '
            'and date >= %(today)s '
            'and status = \'ordered\'',
            {
                'cust_id': customer_id,
                'today': now.date()
            })

        logger.debug(cur.query)

        return fetched_rows(cur)
    finally:
        if conn is not None:
            conn.close()

        if cur is not None:
            cur.close()


def cancelled_deliverables_from_tomorrow(phone_number, password, auth_func, now):
    try:
        auth_func(phone_number, password)
    except Exception:
        raise AuthenticationError

    conn = None
    cur = None
    try:
        conn = psycopg2.connect(config.DATABASE_URL)
        cur = conn.cursor()

        customer_id = get_customer_id(cur, phone_number)

        cur.execute(
            'select package_id, quantity, delivery_location, d.price, date, status, meal_id, add_on '
            'from deliverables d '
            'inner join packages p on d.package_id = p.id '
            'where customer_id = %(cust_id)s '
            'and date > %(today)s '
            'and status = \'cancelled\'',
            {
                'cust_id': customer_id,
                'today': now.date()
            })

        return fetched_rows(cur)
    finally:
        if conn is not None:
            conn.close()

        if cur is not None:
            cur.close()
