from enum import IntEnum


class DeliverableStatus(IntEnum):
    ORDERED = 0
    DELIVERED = 1
    CANCELLED = 2

    @staticmethod
    def from_string(string):
        """
        Accepts a string and returns the corresponding DeliverableStatus.

        :raises ValueError if no match found
        :param string: the string to convert
        :return: corresponding deliverable status
        """
        if string == 'ordered':
            return DeliverableStatus.ORDERED

        if string == 'delivered':
            return DeliverableStatus.DELIVERED

        if string == 'cancelled':
            return DeliverableStatus.CANCELLED

        raise ValueError


class Deliverable:
    def __init__(self, package_id, quantity, delivery_location, price, date, status,
                 door_step_delivery=False, meal_id=None, add_on=None):
        self.package_id = package_id
        self.quantity = quantity
        self.delivery_location = delivery_location
        self.price = price
        self.date = date
        self.door_step_delivery = door_step_delivery
        self.meal_id = meal_id
        self.add_on = add_on

        if isinstance(status, str):
            self.status = DeliverableStatus.from_string(status)
        elif isinstance(status, DeliverableStatus):
            self.status = status
        else:
            raise TypeError('status should be either a string or DeliverableStatus')

    def serialize(self):
        return {
            'package_id': self.package_id,
            'quantity': self.quantity,
            'delivery_location': self.delivery_location,
            'price': str(self.price),
            'date': self.date,
            'status': self.status,
            'meal_id': self.meal_id,
            'add_on': self.add_on
        }

    def __eq__(self, o: object) -> bool:
        return isinstance(o, Deliverable) \
               and self.package_id == o.package_id \
               and self.quantity == o.quantity \
               and self.delivery_location == o.delivery_location \
               and self.price == o.price \
               and self.date == o.date \
               and self.status == o.status \
               and self.door_step_delivery == o.door_step_delivery \
               and self.meal_id == o.meal_id \
               and self.add_on == o.add_on

    def __hash__(self) -> int:
        return hash((self.package_id,
                     self.quantity,
                     self.delivery_location,
                     self.price,
                     self.date,
                     self.status,
                     self.door_step_delivery,
                     self.meal_id,
                     self.add_on,
                     ))

    def __str__(self):
        return f'Package ID: {self.package_id} ' \
               f'Quantity {self.quantity} ' \
               f'Delivery location: {self.delivery_location} ' \
               f'Price: {self.price} ' \
               f'Date: {self.date} ' \
               f'status: {self.status} ' \
               f'door step delivery: {self.door_step_delivery} ' \
               f'meal id: {self.meal_id} ' \
               f'add on: {self.add_on}'

    def __repr__(self):
        return self.__str__()
