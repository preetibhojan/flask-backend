# Deliverables

Deliverables are packages which have been paid for. It takes phone 
number and password for authentication and returns a list of 
package id, quantity, delivery location, price and date.

There are three end points:
1. `/deliverables` - which returns all deliverables till date and
2. `/deliverables_from_tomorrow_to_till_date` - returns all 
deliverables from tomorrow to till date or empty list 
3. `/cancelled_deliverables_from_tomorrow` - returns cancelled
deliverables from tomorrow. 

**Note: All end points have same input and output format**

## Sample input
```json
{
    "phone_number": "1234567890",
    "password": "abcdefghi"
}
```

### Sample output
```json
[
  {
    "date": "2020-08-23",
    "delivery_location": 0,
    "package_id": 27,
    "price": 10,
    "quantity": 1,
    "status": 1
  },
  {
    "date": "2020-08-22",
    "delivery_location": 0,
    "package_id": 27,
    "price": 10,
    "quantity": 1,
    "status": 0
  }
]
```

Status is the integer version of an enum. It can be one of:
| Status     | Meaning                  |
| :----------| :-----------------------:|
|  0         | Ordered                  |
|  1         | Delivered                |
|  2         | Cancelled                |