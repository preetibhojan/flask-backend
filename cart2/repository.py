import psycopg2

import config
from util.database import get_customer_id
from util.exceptions import AuthenticationError


def set_in_cart(phone_number, password, auth_func, package_id, quantity):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)
        cur.execute(
            'insert into cart2(customer_id, package_id, quantity) '
            'values (%s, %s, %s) '
            'on conflict (customer_id, package_id) do update '
            'set quantity = %s',
            (customer_id, package_id, quantity, quantity)
        )


def delete_from_cart(phone_number, password, auth_func, package_id):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)
        cur.execute(
            'delete from cart2 '
            'where customer_id = %s '
            'and package_id = %s',
            (customer_id, package_id)
        )

        cur.execute('select meal_id from packages where id = %s', (package_id,))
        meal_id = cur.fetchone()[0]

        cur.execute(
            'select package_id from cart2 '
            'where customer_id = %s '
            'and package_id in (select id from packages where meal_id = %s)',
            (customer_id, meal_id)
        )

        if cur.rowcount == 0:
            cur.execute(
                'delete from cart_delivery_location '
                'where customer_id = %s '
                'and meal_id = %s',
                (customer_id, meal_id)
            )


def set_delivery_location(phone_number, password, auth_func, meal_id, delivery_location):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)
        cur.execute(
            'insert into cart_delivery_location (customer_id, meal_id, delivery_location) '
            'values (%s, %s, %s) on conflict (customer_id, meal_id) do update '
            'set delivery_location = %s',
            (customer_id, meal_id, delivery_location, delivery_location)
        )


def cartV2(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        customer_id = get_customer_id(cur, phone_number)
        cur.execute(
            'select package_id, quantity, meal_id from cart2 '
            'inner join packages p on cart2.package_id = p.id '
            'where customer_id = %s',
            (customer_id,)
        )

        packages = []
        for package_id, quantity, meal_id in cur.fetchall():
            packages.append({
                'quantity': quantity,
                'package_id': package_id,
                'meal_id': meal_id,
            })

        cur.execute(
            'select meal_id, delivery_location '
            'from cart_delivery_location '
            'where customer_id = %s',
            (customer_id,)
        )

        delivery_locations = []
        for meal_id, delivery_location in cur.fetchall():
            delivery_locations.append({
                'meal_id': meal_id,
                'delivery_location': delivery_location
            })

        return {
            'packages': packages,
            'delivery_locations': delivery_locations
        }


def clear_cart2(phone_number, password, auth_func):
    try:
        auth_func(phone_number, password)
    except:
        raise AuthenticationError

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'delete from cart2 '
            'where customer_id = ('
            'select id from customer '
            'where phone_number = %s'
            ')',
            (phone_number,)
        )

        cur.execute(
            'delete from cart_delivery_location '
            'where customer_id = ('
            'select id from customer '
            'where phone_number = %s'
            ')',
            (phone_number,)
        )
