import logging
from datetime import datetime, date

import psycopg2
from firebase_admin.messaging import Message, Notification, send_all
from psycopg2.extras import execute_values

import config
import util.exceptions
from add_quantity.exceptions import NotAllMealsCanBeOrderedNow, DuplicatesFound
from meals.repository import Meal

logger = logging.getLogger(__name__)


def _meals_for(cur, packages):
    cur.execute('select id, name, priority, orderbefore, deliverytime, deposit '
                'from meals where id in ('
                'select distinct meal_id '
                'from packages p '
                'where p.id in %s'
                ')',
                (tuple(map(lambda p: p.id, packages)),)
                )

    meals = []

    for meal_id, name, priority, order_before, delivery_time, deposit in cur.fetchall():
        meals.append(Meal(meal_id, name, priority, order_before, delivery_time, None, deposit))

    return meals


def _packages_exist(cur, package_ids):
    cur.execute('select id from packages where id in %s',
                (tuple(package_ids),)
                )

    return cur.rowcount == len(package_ids)


def _all_meals_can_be_ordered_now(meals, now):
    for meal in meals:
        if datetime.combine(now.date(), meal.delivery_time) - meal.order_before < now:
            return False

    return True


def add_quantity_use_case(phone_number, password, auth_func, packages, date, gst, now):
    try:
        auth_func(phone_number, password)
    except Exception:
        logger.info('Authentication error')
        raise util.exceptions.AuthenticationError

    if not packages:
        logger.info('packages to set is none or empty')
        raise util.exceptions.InvalidInput

    if date < now.date():
        logger.info('date to set quantity before today')
        raise util.exceptions.InvalidInput

    distinct_package_ids = set(map(lambda p: p.id, packages))
    package_ids_are_unique = len(packages) == len(distinct_package_ids)

    if not package_ids_are_unique:
        logger.info('packages are not unique')
        raise util.exceptions.InvalidInput

    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        cur.execute(
            'select add_quantity from staff_permissions '
            'where staff_id = (select id from staff where phone_number = %s)',
            (phone_number,)
        )

        if cur.fetchone()[0] is not True:
            logger.critical('unauthorised access to set quantity')
            raise util.exceptions.UnauthorizedAccess

        if not _packages_exist(cur, distinct_package_ids):
            logger.info('package does not exist')
            raise util.exceptions.InvalidInput

        if date == now.date():
            meals = _meals_for(cur, packages)

            if not _all_meals_can_be_ordered_now(meals, now):
                logger.info('not all packages can be set right now')
                raise NotAllMealsCanBeOrderedNow

        _insert_packages(cur, packages, date)
        _add_complimentary_packages_for_regular_customer(cur, date)
        updated_deliverables = _convert(cur, packages, date, gst)
        conn.commit()

        if len(updated_deliverables) == 0:
            logger.debug('No deliverables updated')
            return

        # row[2] = old package_id, row[3] = new package id
        package_ids = set(map(lambda r: r[2], updated_deliverables))
        package_ids = package_ids.union(set(map(lambda r: r[3], updated_deliverables)))

        package_ids = tuple(package_ids)

        if len(package_ids) == 0:
            logger.debug('No package found')
            return

        cur.execute(
            'select id, name, description from packages where id in %s',
            (package_ids,)
        )
        logger.debug(cur.query)

        package_names = {}
        package_descriptions = {}
        for id, name, description in cur.fetchall():
            package_names[id] = name
            package_descriptions[id] = description

        # row[0] = customer id
        customer_ids = set(map(lambda r: r[0], updated_deliverables))
        cur.execute(
            'select id, fcm_token from customer where id in %s and fcm_token is not null',
            (tuple(customer_ids),)
        )
        logger.debug(cur.query)

        fcm_tokens = {}
        for id, fcm_token in cur.fetchall():
            fcm_tokens[id] = fcm_token

        if len(fcm_tokens) == 0:
            logger.debug('No customer with token found')
            return

        messages = []
        for customer_id, _, old_package_id, new_package_id in updated_deliverables:
            if fcm_tokens.get(customer_id) is None:
                continue
            messages.append(Message(
                notification=Notification(
                    title='Your package has been converted',
                    body=f'You package: {package_names[old_package_id]} - {package_descriptions[old_package_id]} '
                         f'has been converted to {package_names[new_package_id]} - {package_descriptions[new_package_id]}'
                ),
                token=fcm_tokens[customer_id]
            ))

        response = send_all(messages)
        logger.info(f'messages to be sent: {len(fcm_tokens)}, messages sent: {response.success_count}')


def _add_complimentary_packages_for_regular_customer(cur, date):
    complementary_packages = _complimentary_packages(cur, date)
    customers = _customers_who_can_book_complimentary_packages(cur, date, complementary_packages)

    values = []

    for meal_id, package_ids in complementary_packages.items():
        for customer_id, delivery_location, quantity in customers.get(meal_id, []):
            for package_id in package_ids:
                values.append((customer_id, package_id, quantity, delivery_location, date, 0))

    execute_values(
        cur,
        'insert into deliverables values %s '
        'on conflict(customer_id, package_id, date) '
        'do nothing',
        values,
    )


def _customers_who_can_book_complimentary_packages(cur, date, complementary_packages):
    if len(complementary_packages) == 0:
        return []

    scheduled_quantity = {}
    cur.execute(
        'with package_quantity as ('
        'select distinct customer_id, package_id, quantity '
        'from tiffininformation '
        'inner join packages p '
        'on p.id = tiffininformation.package_id '
        'and p.add_on is false '
        ') '
        'select customer_id, meal_id, sum(quantity) as sum '
        'from package_quantity '
        'inner join packages p on p.id = package_quantity.package_id '
        'group by customer_id, meal_id'
    )
    logger.debug(cur.query)

    for customer_id, meal_id, quantity in cur.fetchall():
        scheduled_quantity[(customer_id, meal_id)] = quantity

    meal_ids = tuple(complementary_packages.keys())
    cur.execute(
        'select meal_id, customer_id, delivery_location, sum(quantity) '
        'from deliverables d '
        'inner join packages p on p.id = d.package_id '
        'where customer_id in (select id from customer where isregularcustomer) '
        'and meal_id in %s '
        'and date= %s '
        'and p.add_on is false '
        'and status = \'ordered\' '
        'group by meal_id, customer_id, delivery_location ',
        (meal_ids, date)
    )
    logger.debug(cur.query)

    customers = {}

    for meal_id, customer_id, delivery_location, quantity in cur.fetchall():
        if customers.get(meal_id) is None:
            customers[meal_id] = []

        final_quantity = min(quantity, scheduled_quantity.get((customer_id, meal_id), 0))

        customers[meal_id].append((customer_id, delivery_location, final_quantity))

    return customers


def _complimentary_packages(cur, date_to_set_quantity):
    cur.execute(
        'select id, meal_id '
        'from packages p '
        'inner join package_quantity pq '
        'on p.id = pq.package_id '
        'and pq.date = %s '
        'and p.complimentary '
        'and quantity > 0 ',
        (date_to_set_quantity,)
    )
    logger.debug(cur.query)

    complimentary_packages = {}
    for package_id, meal_id in cur.fetchall():
        if complimentary_packages.get(meal_id) is None:
            complimentary_packages[meal_id] = []

        complimentary_packages[meal_id].append(package_id)

    return complimentary_packages


def _insert_packages(cur, packages, date):
    execute_values(
        cur,
        'insert into package_quantity'
        '(package_id, quantity, date) values %s '
        'on conflict(package_id, date) do update '
        'set quantity = package_quantity.quantity '
        '+ EXCLUDED.quantity',
        list(map(lambda p: (p.id, p.quantity, date),
                 packages))
    )
    logger.debug(cur.query)


def _duplicates(_list):
    duplicates = set()
    for i in _list:
        if _list.count(i) > 1:
            duplicates.add(i)

    return list(duplicates)


def _convert(cur, packages, date, gst):
    possible_trigger_packages = list(filter(lambda p: p.quantity > 0, packages))

    if len(possible_trigger_packages) == 0:
        return []

    # Ensure deliverables are not updated by someone booking
    # or cancelling an order
    # cur.execute('LOCK TABLE deliverables IN ACCESS EXCLUSIVE MODE')
    # logger.debug(cur.query)

    cur.execute(
        'select from_package, to_package, p1.price - p2.price as price '
        'from new_conversions '
        'inner join packages p1 on p1.id = from_package '
        'inner join packages p2 on p2.id = to_package '
        'where trigger_package in %s ',
        (tuple(map(lambda p: p.id, possible_trigger_packages)),)
    )
    logger.debug(cur.query)

    conversions = cur.fetchall()
    from_packages = list(map(lambda x: x[0], conversions))

    duplicates = _duplicates(from_packages)
    duplicates_found = len(conversions) > 0 and len(duplicates) > 0

    if duplicates_found:
        raise DuplicatesFound(duplicates)

    update_deliverables = cur.mogrify(
        'update deliverables d '
        'set package_id = pkgs.new_id, '
        'price = d.price - ((pkgs.price * quantity) + (pkgs.price * quantity) * %(gst)s / 100::real) '
        'from (values %%s) as pkgs(old_id, new_id, price) '
        'where package_id = pkgs.old_id '
        'and date = %(date)s '
        'and (status = \'ordered\' or status = \'delivered\') '
        'returning customer_id::text, '
        '((pkgs.price * quantity) + (pkgs.price * quantity) * %(gst)s / 100::real) as price, '
        'pkgs.old_id, pkgs.new_id',
        {
            'date': date,
            'gst': gst,
        }
    )

    updated_deliverables = execute_values(cur, update_deliverables, conversions, fetch=True)
    logger.debug(update_deliverables)

    execute_values(
        cur,
        'update paymentdetail pd '
        'set temp_credit = temp_credit + update_deliverables.price '
        'from (values %s) as update_deliverables (customer_id, price, old_package_id, new_package_id)'
        'where pd.customer_id = update_deliverables.customer_id::uuid '
        'returning pd.customer_id, temp_credit',
        updated_deliverables,
    )
    logger.debug(cur.query)
    return updated_deliverables


if __name__ == '__main__':
    with psycopg2.connect(config.DATABASE_URL) as conn, conn.cursor() as cur:
        d = date(2020, 11, 25)
        _add_complimentary_packages_for_regular_customer(cur, d)
