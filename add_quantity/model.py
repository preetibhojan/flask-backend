from datetime import date


class PackageToSet:
    def __init__(self, id, quantity):
        self.id = id
        self.quantity = quantity

    @staticmethod
    def from_json(json):
        return PackageToSet(
            json['package_id'],
            json['quantity'],
        )
