class NotAllMealsCanBeOrderedNow(Exception):
    pass


class DuplicatesFound(Exception):
    def __init__(self, duplicates):
        self.duplicates = duplicates
